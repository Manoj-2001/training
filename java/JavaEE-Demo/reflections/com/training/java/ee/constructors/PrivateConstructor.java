/*
1) Create a private constructor with main function
----------WBS----------

Requirement:
```````````
Create a private constructor with main function

Entity:
``````
-Test
-PrivateConstructor

Method Signature:
````````````````
-public static void instanceMethod()
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a class Test with private constructor and a static method instanceMethod()
2) Invoke the static method instanceMethod
    2.1) The static method can create object of Test class.

Pseudo Code:
```````````
class Test {

    private Test() {
        System.out.println("This is a private constructor.");
    }

    public static void instanceMethod() {
        Test obj = new Test();
    }

}

public class PrivateConstructor {

    public static void main(String[] args) {
        Test.instanceMethod();
    }
}

*/

package com.training.java.ee.constructors;

class Test {

    // create private constructor
    private Test() {
        System.out.println("This is a private constructor.");
    }

    // create a public static method
    public static void instanceMethod() {
        // create an instance of Test class
        Test obj = new Test();
    }

}

public class PrivateConstructor {
    public static void main(String[] args) {
        // call the instanceMethod()
        Test.instanceMethod();
    }
}
