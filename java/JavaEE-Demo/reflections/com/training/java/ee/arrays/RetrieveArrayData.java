/*
2.Retrieve the added elements using the appropriate method.

----------WBS----------

Requirement:
```````````
Retrieve the added elements using the appropriate method.

Entity:
``````
-RetrieveArrayData

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a integer array using newInstance method with type and specified size
2) Add elements to array at the specified index using setInt method.
3) Get the elements from array using getInt method and display it.

Pseudo Code:
```````````
public class RetrieveArrayData {

    public static void main(String[] args) {
        int[] array = (int[]) Array.newInstance(int.class, 5);

        int range = 5;
        Array.setInt(array, 0, range);
        Array.setInt(array, 1, 10);
        Array.setInt(array, 2, 15);
        Array.setInt(array, 3, 20);
        Array.setInt(array, 4, 25);

        int index = -1;
        while( ++index < range) {
            System.out.println(Array.getInt(array, index));
        }

    }

}
*/

package com.training.java.ee.arrays;

import java.lang.reflect.Array;

public class RetrieveArrayData {

    public static void main(String[] args) {
        int[] array = (int[]) Array.newInstance(int.class, 5);

        int range = 5;
        // setting integers to array
        Array.setInt(array, 0, range);
        Array.setInt(array, 1, 10);
        Array.setInt(array, 2, 15);
        Array.setInt(array, 3, 20);
        Array.setInt(array, 4, 25);

        int index = -1;
        // getting integers from array
        while( ++index < range) {
            System.out.println(Array.getInt(array, index));
        }

    }

}
