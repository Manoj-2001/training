/*
1.Create a java array using reflect.array class and add elements
using the appropriate method.

----------WBS----------

Requirement:
```````````
Create a java array using reflect.array class and add elements
using the appropriate method.

Entity:
``````
-ReflectArrayDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a character array using newInstance method with type and specified size
2) Add elements to array at the specified index using setChar method.

Pseudo Code:
```````````
public class ReflectArrayDemo {
    public static void main(String[] args) {

        char[] array = (char[]) Array.newInstance(char.class, 5);

        // Add elements into the array
        Array.setChar(array, 0, 'M');
        Array.setChar(array, 1, 'A');
        Array.setChar(array, 2, 'N');
        Array.setChar(array, 3, 'O');
        Array.setChar(array, 4, 'J');

    }
}
*/

package com.training.java.ee.arrays;

import java.lang.reflect.Array;

public class ReflectArrayDemo {
    public static void main(String[] args) {

        // Create a String array using reflect.Array class with the newInstance()
        char[] array = (char[]) Array.newInstance(char.class, 5);

        // Add elements into the array
        Array.setChar(array, 0, 'M');
        Array.setChar(array, 1, 'A');
        Array.setChar(array, 2, 'N');
        Array.setChar(array, 3, 'O');
        Array.setChar(array, 4, 'J');

        System.out.println(new String(array));
    }
}