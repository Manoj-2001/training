/*
3)create a Program to demonstrate @Target annotation (@Target) 
using interface annotation (@interface)

----------WBS----------

Requirement:
```````````
3)create a Program to demonstrate @Target annotation (@Target) 
using interface annotation (@interface)

Entity:
``````
-TargetAnnotationDemo

Interface:
`````````
-CustomAnnotation

Method Signature:
````````````````
-int studentAge()
-String studentName()
-String studentAddress()
-String studentDept()
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a annotation for interface
2) Make the target type as METHOD
3) Use the customAnnotation to methods.

Pseudo Code:
```````````
@Target(ElementType.METHOD)
@interface CustomAnnotation {

    // declare methods

}

public class TargetAnnotationDemo {

    @CustomAnnotation()
    public static void main(String[] args) {
        System.out.println("The main method is annotated");
    }
}
*/

package com.training.java.ee.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@interface CustomAnnotation {

    int studentAge() default 18;
    String studentName();
    String studentAddress();
    String studentDept() default "ECE";

}

public class TargetAnnotationDemo {

    @CustomAnnotation(studentName = "Manoj", studentAddress = "Thiruvarur")
    public static void main(String[] args) {
        System.out.println("The main method is annotated");
    }
}
