/*
2)complete:
i)Deprecate the display method
ii)how to suppress the deprecate message

class DeprecatedTest {
    //method to be deprecated
    public void Display()
    {
        System.out.println("Deprecatedtest display()");
    }
}

public class Test
{
    public static void main(String args[]) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.Display();
    }
}

----------WBS----------

Requirement:
```````````
Deprecate the display method.
Suppress the deprecate message

Entity:
``````
-DeprecatedTest
-DeprecateDemo

Method Signature:
````````````````
-public void display()
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a class DeprecatedTest with a method display
2) Create an object of the class DeprecatedTest
3) Invoke the method display using the object
4) Use Deprecate annotation to the method display
5) Use SuppressWarnings annotation to the main method

Pseudo Code:
```````````
class Base {

    public void display() {
    
    }

}
class Derived extends Base {

    @Override
    public void display(int x)
    {
    
    }
  
    public static void main(String args[])
    {
        Derived obj = new Derived();
        obj.display();
    }
}

*/

package com.training.java.ee.annotations;

class DeprecatedTest {
    @Deprecated
    public void display() {
        System.out.println("Deprecatedtest display()");
    }
}

public class DeprecateDemo {

    @SuppressWarnings({"checked", "deprecation"})
    public static void main(String args[]) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.display();
    }

}
