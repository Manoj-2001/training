/*
1)USE OVERRIDE ANNOTATION and complete the program,
SHOW THE OUTPUT OF THIS PROGRAM: 

class Base 
{ 
     public void display() 
     { 
         
     } 
} 
class Derived extends Base 
{ 
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}

----------WBS----------

Requirement:
```````````
USE OVERRIDE ANNOTATION and complete the program

Entity:
``````
-Base
-Derived

Method Signature:
````````````````
-public void display()
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a base class with a method display.
2) Create a derived class with a method display, the class extends base class.
3) Use Override annotation to denote the method overriding base class method.

Pseudo Code:
```````````
class Base {

    public void display() {
    
    }

}
class Derived extends Base {

    @Override
    public void display(int x)
    {
    
    }
  
    public static void main(String args[])
    {
        Derived obj = new Derived();
        obj.display();
    }
}

*/

package com.training.java.ee.annotations;

class Base {

    public void display() {
        System.out.println("Hello I'm Base class");
    }

}

class Derived extends Base {

    @Override
    public void display() {
        System.out.println("Hello I'm from Derived class");
    }

    public static void main(String args[]) {
        Derived obj = new Derived();
        obj.display();
    }

}
