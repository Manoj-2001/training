package com.training.java.ee.genrics;

/*

1. Consider this class:
    class Node<T> implements Comparable<T> {
        public int compareTo(T obj) {  ...  }
        
    }

Will the following code compile? If not, why?

*/

/*

Answer:
``````
No, because the greater than (>) operator applied only to primitive numeric types.

*/