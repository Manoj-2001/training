/*
37. Create a list
        => Add 10 values in the list
        => Create another list and perform addAll() method with it
        => Find the index of some value with indexOf() and lastIndexOf()
        => Print the values in the list using
            - For loop
            - For Each
            - Iterator
            - Stream API
        => Convert the list to a set
        => Convert the list to a array
    Explain about contains(), subList(), retainAll() and give example

----------WBS----------

Requirement:
```````````
Add 10 values in the list
Create another list and perform addAll() method with it
Find the index of some value with indexOf() and lastIndexOf()
Print the values in the list using For, Enhanced For, Iterator, Stream API
Convert the list to a set
Convert the list to a array
Explain about contains(), subList(), retainAll() and give example.

Entity:
``````
-ListDemo

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create and add 10 students in students list
2) Create a another list newStudents with some students and add all the students 
from students list.
3) Find the position of a student in list using indexOf and find the position 
of a student present at last who's name repeated using lastIndexOf method
4) Display all the students in the list by accessing the index in list.
5) Display all the students in students list using enhanced for.
6) Create an object of Iterator by invoking the iterator method using list
go through the elements in Iterator object to display the elements
7) Display all the students using forEach method in Stream API
8) Adding all elements of list to set by passing list as parameter to HashSet
Constructor
9) Check whether a student present in students list
10) Get a part of the list using subList method in Collections
11) Keep the students in students list who are all present in
newlyJoinedStudents list.

Pseudo Code:
```````````
public class ListDemo {
    public static void main(String[] args) {
        List<String> students = new ArrayList<>();

        students.add("Chris");
        students.add("James");
        students.add("Robert");

        List<String> newStudents = new ArrayList<>();

        newStudents.add("Jacob");
        newStudents.add("James");

        students.addAll(newStudents);

        students.indexOf("Jacob");
        students.lastIndexOf("James");

        //For loop
        System.out.println("For loop:");
        for (int index = 0; index < students.size(); index++) {
            System.out.println(students.get(index));
        }

        //Enhanced For loop
        System.out.println("Enhanced for loop:");
        for (String student : students) {
            System.out.println(student);
        }

        //While loop
        System.out.println("Iterator interface with while loop:");
        Iterator<String> studIter = students.iterator();
        while(studIter.hasNext()) {
            System.out.println(studIter.next());
        }

        //Stream API's forEach
        System.out.println("Iterating values using Stream API:");
        students.forEach(System.out::println);

        Set<String> studentSet = new HashSet<>(students);
        String[] studentArr = students.toArray(new String[students.size()]);

        students.contains("Smith");
        List<String> newlyJoinedStudents = students.subList(2, students.size());
        students.retainAll(newlyJoinedStudents);
    }
}

*/
package com.training.java.ee.listset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListDemo {

    public static void main(String[] args) {
        //Creating a list and adding 10 elements to it.
        List<String> students = new ArrayList<>();

        students.add("Chris");
        students.add("James");
        students.add("Robert");
        students.add("Smith");
        students.add("Peter");
        students.add("Noah");
        students.add("Henry");
        students.add("Tim");
        students.add("William");
        students.add("Mark");

        System.out.println("Student list:\n" + students);

        //Creating another list with some elements.
        List<String> newStudents = new ArrayList<>();

        newStudents.add("Jacob");
        newStudents.add("Noah");
        newStudents.add("April");

        //And perform addAll() method
        students.addAll(newStudents);

        System.out.println("New student list:\n" + students);

        //Find the index of a value with indexOf() and lastIndexOf()
        System.out.println("Index of student 'Tim' " + students.indexOf("Tim"));
        System.out.println("Last index of student 'Noah' " +
        students.lastIndexOf("Noah"));

        //Print the values in the list using
        //For loop
        System.out.println("For loop:");
        for (int index = 0; index < students.size(); index++) {
            System.out.println(students.get(index));
        }

        //Enhanced For loop
        System.out.println("Enhanced for loop:");
        for (String student : students) {
            System.out.println(student);
        }

        //While loop
        System.out.println("Iterator interface with while loop:");
        Iterator<String> studIter = students.iterator();
        while(studIter.hasNext()) {
            System.out.println(studIter.next());
        }

        //Stream API's forEach
        System.out.println("Iterating values using Stream API:");
        students.forEach(System.out::println);

        //Convert the list to a set
        Set<String> studentSet = new HashSet<>(students);

        //Convert the list to a array
        String[] studentArr = students.toArray(new String[students.size()]);
        System.out.println(studentArr);

        //contains()
        System.out.println(students.contains("Smith"));

        //subList(startIndex, lastIndex)
        List<String> newlyJoinedStudents = students.subList(10, students.size());
        System.out.println(newlyJoinedStudents);

        //retainAll()
        System.out.println("After using retainAll method in students list: " + 
        students.retainAll(newlyJoinedStudents) + "\n" + students);
    }
}
