/*
37. Create a set
        => Add 10 values
        => Perform addAll() and removeAll() to the set.
        => Iterate the set using 
            - Iterator
            - for-each
        Explain the working of contains(), isEmpty() and give example.

----------WBS----------

Requirement:
```````````
Add 10 values to a set
Perform addAll() and removeAll() to the set
Iterate the set using Iterator and for-each.
working of contains(), isEmpty()

Entity:
``````
-SetDemo

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a bag set and add 10 things to bag.
2) create an another set called geometry and
add some things to geometry set
3) Add all things of geometry set to bag set.
4) Remove all things of geometry set from bag set.
5) Create an object of Iterator by invoking the iterator method using set
go through the elements in Iterator object to display the elements
6) Display all elements in set using Stream API
7) Check bag contains box.
8) Check geometry set has any elements.

Pseudo Code:
```````````
public class SetDemo {
    public static void main(String[] args) {
        Set<String> bag = new HashSet<>();

        bag.add("Notebook");
        bag.add("Paper");
        bag.add("Pencil");
        bag.add("Erazer");
        bag.add("Pen");
        bag.add("Ruler");

        Set<String> geometry = new HashSet<>();
        geometry.add("Compass");
        geometry.add("Protractor");
        geometry.add("Divider");
        geometry.add("Set-square");

        bag.addAll(geometry);
        bag.removeAll(geometry);

        Iterator<String> items = bag.iterator();
        while (items.hasNext()) {
            System.out.println(items.next());
        }

        bag.forEach(System.out::println);

        System.out.println(bag.contains("Protractor"));
        System.out.println(geometry.isEmpty());
    }
}

*/
package com.training.java.ee.listset;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

    public static void main(String[] args) {
        //Creating a set and adding 6 elements to it.
        Set<String> bag = new HashSet<>();

        bag.add("Notebook");
        bag.add("Paper");
        bag.add("Pencil");
        bag.add("Erazer");
        bag.add("Pen");
        bag.add("Ruler");

        //Creating another set with some elements.
        Set<String> geometry = new HashSet<>();
        geometry.add("Compass");
        geometry.add("Protractor");
        geometry.add("Divider");
        geometry.add("Set-square");

        //Performing addAll() method
        bag.addAll(geometry);
        System.out.println(bag);
        //Performing removeAll() method
        bag.removeAll(geometry);
        System.out.println(bag);
        geometry.clear();

        //Displaying all the set elements using Iterator interface
        Iterator<String> items = bag.iterator();
        while (items.hasNext()) {
            System.out.println("Iterator : " + items.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("Using ForEach:");
        bag.forEach(System.out::println);

        //contains()
        System.out.println("Bag contains protractor? " + bag.contains("Protractor"));

        //isEmpty()
        System.out.println("is geometry empty? " + geometry.isEmpty());
    }
}
