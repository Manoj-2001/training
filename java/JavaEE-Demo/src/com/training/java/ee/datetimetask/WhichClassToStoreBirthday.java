/*
2. Which class would you use to store your birthday in years, months,
days, seconds, and nanoseconds?
*/

package com.training.java.ee.datetimetask;

/*

Answer:
``````
 -> LocalDateTime class
 -> ZonedDateTime class (to take a particular time zone)

Both classes track date and time to nanosecond precision and both classes,
when used in conjunction with Period, give a result using a combination of
human-based units, such as years, months, and days.

*/
