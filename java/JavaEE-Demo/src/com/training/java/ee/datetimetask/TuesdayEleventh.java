/*
4. Write an example that tests whether a given date occurs on Tuesday the 11th.

----------WBS----------

Requirement:
```````````
Whether a given date occurs on Tuesday the 11th

Entity:
``````
-TuesdayEleventh

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the date from user using scanner
2) parse the string date into LocalDate
3) check the date is tuesday 11th using query method
    3.1) return true
    3.2) Otherwise return false

Pseudo Code:
```````````
public class TuesdayEleventh implements TemporalQuery<Boolean> {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String dateInput = scanner.nextLine();
        LocalDate date = LocalDate.parse(dateInput);
        System.out.println(date.query(new TuesdayEleventh()));
    }

    public Boolean queryFrom(TemporalAccessor temporal) {
        return ((temporal.get(ChronoField.DAY_OF_MONTH) == 11) &&
                (temporal.get(ChronoField.DAY_OF_WEEK) == 2));
    }

}
*/

package com.training.java.ee.datetimetask;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.util.Scanner;

public class TuesdayEleventh implements TemporalQuery<Boolean> {

    public static void main(String[] args) {
        System.out.println("Enter the date in the format (YYYY-MM-DD)");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String dateInput = scanner.nextLine();
        LocalDate date = LocalDate.parse(dateInput);
        System.out.println(date.query(new TuesdayEleventh()));
    }

    @Override
    public Boolean queryFrom(TemporalAccessor temporal) {
        return ((temporal.get(ChronoField.DAY_OF_MONTH) == 11) &&
                (temporal.get(ChronoField.DAY_OF_WEEK) == 2));
    }

}
