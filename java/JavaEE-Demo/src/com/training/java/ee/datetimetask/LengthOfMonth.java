/*
5. Write an example that, for a given year, reports the length of each
month within that particular year.

----------WBS----------

Requirement:
```````````
for a given year, reports the length of each month within that particular year.

Entity:
``````
-LengthOfMonth

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the year from user using scanner
2) Go through 1 to 12 using while loop
    2.1) Create object of YearMonth class with year and month 
    in integer as parameters
    2.2) Get the length of month using lengthOfMonth method

Pseudo Code:
```````````
public class LengthOfMonth {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        int month = 1;
        while (month <= 12) {
            YearMonth yearMonth = YearMonth.of(year, month);
            System.out.println(yearMonth.lengthOfMonth());
            month++;
        }
    }

}
*/

package com.training.java.ee.datetimetask;

import java.time.YearMonth;
import java.util.Scanner;

public class LengthOfMonth {

    public static void main(String[] args) {
        System.out.println("Enter the year:");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        int month = 1;
        YearMonth yearMonth = null;
        while (month <= 12) {
            yearMonth = YearMonth.of(year, month);
            System.out.println(month + " : " + yearMonth.lengthOfMonth());
            month++;
        }
    }

}
