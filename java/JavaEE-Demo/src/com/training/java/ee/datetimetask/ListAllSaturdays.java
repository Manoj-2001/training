/*
3. Write an example that, for a given month of the current year,
lists all of the Saturdays in that month.

----------WBS----------

Requirement:
```````````
For a given month of the current year, lists all of the Saturdays in that month.

Entity:
``````
-ListAllSaturdays

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the month from user using Scanner
2) Store the month to an object
3) Get the first saturday in the month using TemporalAdjusters and store it in
the object date of LocalDate
4) Get the month of the date and store it in dateOfMonth
5) checking date of month is matches with month user input
    5.1) Get the date of the 'saturday'
    5.2) display the date

Pseudo Code:
```````````
public class ListAllSaturdays {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine().toUpperCase();
        Month month = Month.valueOf(input);

        LocalDate date = Year.now().atMonth(month).atDay(1).with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month dateOfMonth = date.getMonth();

        while (month == dateOfMonth) {
            System.out.println(date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            dateOfMonth = date.getMonth();
        }
    }

}
*/

package com.training.java.ee.datetimetask;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListAllSaturdays {

    public static void main(String[] args) {
        System.out.println("Enter the month to find all saturdays:");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine().toUpperCase();
        Month month = Month.valueOf(input);

        LocalDate date = Year.now().atMonth(month).atDay(1)
                .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month dateOfMonth = date.getMonth();

        while (month == dateOfMonth) {
            System.out.println(date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            dateOfMonth = date.getMonth();
        }
    }

}
