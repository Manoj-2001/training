/*
1.Given a random date, how would you find the date of the previous Friday?

----------WBS----------

Requirement:
```````````
Given a random date, to find the date of the previous Friday

Entity:
``````
-PreviousFriday

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the random date in LocalDate
2) Get the previous friday using previous method of TemporalAdjusters

Pseudo Code:
```````````
public class PreviousFriday {

    public static void main(String[] args) {
        // getting a random date
        LocalDate date = #random_date

        // getting the previous friday of the specified date
        System.out.format(date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }

}

*/

package com.training.java.ee.datetimetask;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class PreviousFriday {

    public static void main(String[] args) {
        // getting a random date
        LocalDate date = LocalDate.of(2020, 8, 21);

        // getting the previous friday of the specified date
        System.out.format("The previous Friday is: %s",
                  date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }

}
