/*
5. Write some String content using Writer

----------WBS----------

Requirement:
```````````
Write some String content using Writer

Entity:
``````
-WriteStringUsingWriter

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source of file
    1.1) If file not exists throw exception
2) Write the file using FileWriter
3) invoke the write of content to write content to file

Pseudo Code:
```````````
public class WriteStringUsingWriter {

    public static void main(String[] args) {

        // FileWriter for writing data to file
        Writer writer = new FileWriter(source);
        String data = "This statement written by Writer";
        writer.write(data);
        writer.close();
        System.out.println("File writed successfully");
    }

}

*/

package com.training.java.ee.io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriteStringUsingWriter {

    public static void main(String[] args) {

        // source as file path
        String source = "src/com/training/java/ee/io/content.txt";
        try {
            // FileWriter for writing data to file
            Writer writer = new FileWriter(source);
            String data = "This statement written by Writer";
            writer.write(data);
            writer.close();
            System.out.println("File writed successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
