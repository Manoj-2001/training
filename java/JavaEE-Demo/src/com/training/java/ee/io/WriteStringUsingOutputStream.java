/*
4. Write some String content using OutputStream

----------WBS----------

Requirement:
```````````
Write some String content using OutputStream

Entity:
``````
-WriteStringUsingOutputStream

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source of file
    1.1) If file not exists throw exception
2) Write the file using OutputStream
3) invoke the write of byte array to write content to file
4) OutputStream decodes the array and store as String content

Pseudo Code:
```````````
public class WriteStringUsingOutputStream {

    public static void main(String[] args) {

        OutputStream outStream = new FileOutputStream(source);
        String data = "This statement written by Output Stream";

        // encodes the string to byte array
        byte[] byteArray = data.getBytes();

        // decodes and store as string
        outStream.write(byteArray);
        outStream.close();
        System.out.println("File writed successfully");
    }

}

*/

package com.training.java.ee.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteStringUsingOutputStream {

    public static void main(String[] args) {
        // file path
        String source = "src/com/training/java/ee/io/content.txt";
        try {
            OutputStream outStream = new FileOutputStream(source);
            String data = "This statement written by Output Stream";

            // encodes the string to byte array
            byte[] byteArray = data.getBytes();

            // decodes and store as string
            outStream.write(byteArray);
            outStream.close();
            System.out.println("File writed successfully");
        } catch (FileNotFoundException fileNotFound) {
            fileNotFound.getMessage();
        } catch (IOException ioe) {
            ioe.getMessage();
        }
    }

}
