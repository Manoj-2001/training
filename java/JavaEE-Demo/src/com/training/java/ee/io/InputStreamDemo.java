/*
1. Reading a file using InputStream

----------WBS----------

Requirement:
```````````
To read a file using InputStream

Entity:
``````
-InputStreamDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file in InputStream to read data byte by byte
    1.1) If no file exists throw exception
2) read method to read data byte by byte
    2.1) If the data less than 0, It represents there is no more data on that file
    2.2) If data present convert the integer to character

Pseudo Code:
```````````
public class InputStreamDemo {

    public static void main(String[] args) throws IOException {

        // get the file through input stream
        // input stream get data byte by byte
        InputStream input = new FileInputStream(source); // source is the file path
        int data;

        // read to get a single byte
        while ((data = input.read()) != -1) {
            System.out.println((char) data);
        }
    }
}

*/

package com.training.java.ee.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamDemo {

    public static void main(String[] args) throws IOException {
        String source = "src/com/training/java/ee/io/content.txt";
        try {
            // get the file through input stream
            // input stream get data byte by byte
            InputStream input = new FileInputStream(source);
            int data;

            // read to get a single byte
            while ((data = input.read()) != -1) {
                System.out.println((char) data);
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

}
