/*
2. Reading a file using Reader

----------WBS----------

Requirement:
```````````
To read a file using Reader

Entity:
``````
-ReaderDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file
    1.1) If no file exists throw exception
2) Read the file with reader
3) Create a character array of file length
4) Read the data of size of char array and store to char array
5) Display the readed content

Pseudo Code:
```````````
public class ReaderDemo {

    public static void main(String[] args) throws IOException {

        // getting the file
        File file = new File(source);

        // Reader to read the data present in file
        Reader reader = new FileReader(file);

        // creating a char array of size of file
        char[] chars = new char[file.length()];

        // read() gets the byes of data upto the size of char array
        // and stores the readed data to char array
        reader.read(chars);
        System.out.println(chars);
    }

}

*/

package com.training.java.ee.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class ReaderDemo {

    public static void main(String[] args) throws IOException {

        // source file location
        String source = "src/com/training/java/ee/io/content.txt";

        // getting the file
        File file = new File(source);

        // Reader to read the data present in file
        Reader reader = new FileReader(file);

        // creating a char array of size of file
        char[] chars = new char[(int) file.length()];

        // read() gets the byes of data upto the size of char array
        // and stores the readed data to char array
        reader.read(chars);
        System.out.println(new String(chars));
        reader.close();
    }

}
