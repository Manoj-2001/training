/*
3. Read a any text file using BufferedReader and print the content of the file

----------WBS----------

Requirement:
```````````
Read a any text file using BufferedReader and print the content of the file

Entity:
``````
-ReadFileContent

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a file to write some content
2) Write contents to the file created
3) close the file after written
4) FileReader to read the content of file
5) Buffered reader takes the Reader file
6) Read each line
    6.1) If a line exists on file show the line of data
    6.2) If no more line exists, stop reading the file
7) close the BufferedReader properly.

Pseudo Code:
```````````
public class ReadFileContent {

    public static void main(String[] args) throws IOException {

        // Creates a file on specified location
        FileWriter file = new FileWriter(source); // source is the file path

        // Writing data to created file and closing the file
        file.write("Hello world I'm Manoj");
        file.write("\nFirst time I'm writing a file");
        file.close();

        // reader to read the data in file
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
        String line;
        // readLine reads data line by line
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        // closing the reader
        bufferedReader.close();
    }

}

*/

package com.training.java.ee.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadFileContent {

    public static void main(String[] args) throws IOException {
        // Creates a file on specified location
        String source = "src/com/training/java/ee/io/content.txt";
        FileWriter file = new FileWriter(source);

        // Writing data to created file and closing the file
        file.write("Hello world I'm Manoj");
        file.write("\nFirst time I'm writing a file");
        file.close();

        // reader to read the data in file
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
        String line;
        // readLine reads data line by line
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        // closing the reader
        bufferedReader.close();
    }

}
