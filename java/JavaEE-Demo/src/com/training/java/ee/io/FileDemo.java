/*
6. Read a file using java.io.File

----------WBS----------

Requirement:
```````````
Read a file using java.io.File

Entity:
``````
-FileDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file
    1.1) If no file exists on the specified path throw exception

Pseudo Code:
```````````
public class ReaderDemo {

    public static void main(String[] args) throws IOException {

        // getting the file
        File file = new File(source);

        // shows the path of file
        System.out.println(file);
    }

}

*/

package com.training.java.ee.io;

import java.io.File;

public class FileDemo {

    public static void main(String[] args) {

        // source as file path
        String source = "src/com/training/java/ee/io/content.txt";

        // opening the file
        File file = new File(source);

        // getting the length of file
        System.out.println(file.length());

        // file or file.getPath() returns the path of the file
        System.out.println(file.getPath());
        System.out.println(file);
    }

}
