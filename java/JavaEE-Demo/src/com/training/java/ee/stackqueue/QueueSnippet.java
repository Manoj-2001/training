/*
4) Consider a following code snippet
        Queue bike = new PriorityQueue();
        bike.poll();
        System.out.println(bike.peek());

   what will be output and complete the code.

----------WBS----------

Requirement:
```````````
Output of the code and to complete the code.

Entity:
``````
-QueueSnippet

Function Declaration:
````````````````````
-public static void main(String[] args)

*/

package com.training.java.ee.stackqueue;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueSnippet {

    public static void main(String[] args) {
        Queue<String> bike = new PriorityQueue<>();
        bike.add("Duke");
        bike.add("Pulsar");
        bike.add("Royal Enfield");
        System.out.println(bike.poll());
        System.out.println(bike.peek());

    }

}
