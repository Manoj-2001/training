/*
3) Create a queue using generic type and in both implementation Priority Queue,
Linked list and complete following
  -> add at least 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value
  -> print the size of stack
  -> print the elements using Stream

----------WBS----------

Requirement:
```````````
Write a java program to create a queue using generic type and in both 
implementation Priority Queue, Linked list and complete following
  add at least 5 elements
  remove the front element
  search a element in stack using contains key word and print boolean value
  print the size of stack
  print the elements using Stream

Entity:
``````
-QueueDemo

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) First to create objects of LinkedList and PriorityQueue.
2) Add at least 5 elements to both the objects of LinkedList and 
PriorityQueue.
3) Remove the specific element from queue.
4) Check the element is present or not in queue.
5) Get the size of queue.
6) Finally printing all the elements of queue using stream's forEach.

Pseudo Code:
```````````
public class QueueDemo {

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        queue.add(100);
        queue.add(101);
        queue.add(102);
        queue.add(103);
        queue.add(104);

        // removing the peek element from the queue
        queue.remove();

        // checking whether queue has the element or not
        System.out.println(queue.contains(100));

        // getting the size of the queue
        System.out.println(queue.size());

        // printing all elements using forEach in stream
        Stream<Integer> stream = queue.stream();
        stream.forEach(System.out::println);

        Queue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(2001);
        priorityQueue.add(2004);
        priorityQueue.add(2012);
        priorityQueue.add(2017);
        priorityQueue.offer(2020);
        priorityQueue.offer(2021);

        priorityQueue.poll();

        System.out.println(priorityQueue.contains(2020));

        System.out.println(priorityQueue.size());

        Stream<Integer> priorityQueueStream = priorityQueue.stream();
        priorityQueueStream.forEach(System.out::println);
    }

}

*/
package com.training.java.ee.stackqueue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueDemo {

    public static void main(String[] args) {
        System.out.println("LinkedList implementation:");
        Queue<Integer> queue = new LinkedList<>();

        // adding elements to queue
        queue.add(100);
        queue.add(101);
        queue.add(102);
        queue.add(103);
        queue.add(104);

        // removing the peek element from the queue
        queue.remove();

        // checking whether queue has the element or not
        System.out.println(queue.contains(100));

        // getting the size of the queue
        System.out.println(queue.size());

        // printing all elements using forEach in stream
        System.out.println("Iterating elements using stream:");
        Stream<Integer> stream = queue.stream();
        stream.forEach(System.out::println);

        System.out.println("PriorityQueue implementation:");
        Queue<Integer> priorityQueue = new PriorityQueue<>();

        // adding elements to the queue
        priorityQueue.add(2001);
        priorityQueue.add(2004);
        priorityQueue.add(2012);
        priorityQueue.add(2017);
        priorityQueue.offer(2020);
        priorityQueue.offer(2021);

        // removing the peek element from the queue
        priorityQueue.poll();

        //checking whether queue has the element or not
        System.out.println(priorityQueue.contains(2020));

        // getting the size of the queue
        System.out.println(priorityQueue.size());

        // printing all elements using forEach in stream
        System.out.println("Iterating elements using stream:");
        Stream<Integer> priorityQueueStream = priorityQueue.stream();
        priorityQueueStream.forEach(System.out::println);
    }

}
