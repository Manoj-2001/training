/*
2) Reverse List Using Stack with minimum 7 elements in list.

----------WBS----------

Requirement:
```````````
To reverse the list using stack.

Entity:
``````
-ReverseListUsingStack

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a list of meaningful characters.
2) Remove each element in list and add to a object of Stack
3) Go through then remove each element from stack and add to list

Pseudo Code:
```````````
public class ReverseListUsingStack {

    public static void main(String[] args) {
        List<Character> list = new ArrayList<>();

        list.add('H');
        list.add('E');
        list.add('L');
        list.add('L');
        list.add('O');

        Stack<Character> stack = new Stack<>();

        while (list.size() > 0) {
            stack.push(list.remove(0));
        }

        // stack follows "First In Last Out"
        while (stack.size() > 0) {
            list.add(stack.pop());
        }

        System.out.print(list)
    }

}

*/
package com.training.java.ee.stackqueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ReverseListUsingStack {

    public static void main(String[] args) {
        List<Character> list = new ArrayList<>();
        Stack<Character> stack = new Stack<>();

        list.add('H');
        list.add('E');
        list.add('L');
        list.add('L');
        list.add('O');
        list.add(' ');
        list.add('W');
        list.add('O');
        list.add('R');
        list.add('L');
        list.add('D');

        System.out.print(list); // printing the list before reversing.

        while (list.size() > 0) {
            stack.push(list.remove(0)); // pushing all elements of list to stack.
        }

        System.out.println(); // at this statement list becomes empty.

        // stack follows "First In Last Out"
        while (stack.size() > 0) {
            list.add(stack.pop()); // picking the last element and putting in list.
        }

        System.out.print(list); // at this statement stack becomes empty.
    }

}
