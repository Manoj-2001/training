/*
1) Create a stack using generic type and implement
      -> Push at least 5 elements
      -> Pop the peek element
      -> search a element in stack and print index value
      -> print the size of stack
      -> print the elements using Stream

----------WBS----------

Requirement:
```````````
Write a java program to create a stack using generic type.
Push at least 5 elements
Pop the peek element
Search a element in stack and print index value
Print the size of stack
Print the elements using Stream

Entity:
``````
-StackDemo

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a object of Stack and push some characters to it.
2) Get the element at top using pop() method
3) Search for an element in characters stack
4) Get the size of the characters stack
5) Display the characters in characters stack using Stream's forEach

Pseudo Code:
```````````
public class StackDemo {

    public static void main(String[] args) {
        Stack<Character> characters = new Stack<>();
        characters.push('M');    // index 1
        characters.push('A');    // index 2
        characters.push('N');    // index 3
        characters.push('O');    // index 4
        characters.push('J');    // index 5

        System.out.println(characters.pop());

        System.out.println(characters.search('N'));

        System.out.println(characters.size());

        Stream<Character> stream = characters.stream();
        stream.forEach(System.out::print);
    }

}
*/
package com.training.java.ee.stackqueue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {

    public static void main(String[] args) {
        Stack<Character> characters = new Stack<>();
        characters.push('M');    // index 1
        characters.push('A');    // index 2
        characters.push('N');    // index 3
        characters.push('O');    // index 4
        characters.push('J');    // index 5

        //This method picks the element from stack now, the element not in stack.
        System.out.println(characters.pop());
        
        /*
        search() is the method of stack which will return the index of the
        passed element. The method uses the equals() method to check the values
        in the stack.
        */
        System.out.println(characters.search('N'));
        
        //Prints the size of the stack, already a element is popped now size is 4.
        System.out.println(characters.size());
        
        //Print the elements using Stream API's forEach
        Stream<Character> stream = characters.stream();
        stream.forEach(System.out::print);
    }

}
