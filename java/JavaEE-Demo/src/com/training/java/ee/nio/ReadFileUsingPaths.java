/*
7. Read a file using java.nio.Files using Paths

----------WBS----------

Requirement:
```````````
Read a file using java.nio.Files using Paths

Entity:
``````
-ReadFileUsingPaths

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of some files
    1.1) If no file exists on the specified path throw exception
2) Read all lines of file using readAllLines method of Files and store it into list
3) Read the file as bytes using readAllBytes method of Files and store in into byte
array.
4) Display the list

Pseudo Code:
```````````
public class ReadFileUsingPaths {

    public static void main(String[] args) {
        try {
            Path path = Paths.get(#file path);
            List<String> lines = Files.readAllLines(path);
            byte[] bytes = Files.readAllBytes(path);
            System.out.println(lines);
            System.out.println(new String(bytes));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}

*/

package com.training.java.ee.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadFileUsingPaths {

    public static void main(String[] args) {
        try {
            // get the path of the file using get of Paths
            Path path = Paths.get("src/com/training/java/ee/nio/demo.txt");

            // read all lines in file and store it into list
            List<String> lines = Files.readAllLines(path);

            // read all bytes of file and store it into byte array
            byte[] bytes = Files.readAllBytes(path);

            System.out.println(lines);
            System.out.println(new String(bytes));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
