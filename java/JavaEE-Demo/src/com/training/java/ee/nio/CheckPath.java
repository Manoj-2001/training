/*
9. Given a path, check if path is file or directory

----------WBS----------

Requirement:
```````````
Given a path, check if path is file or directory

Entity:
``````
-CheckPath

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of a file in File class
2) Check the file path
    2.1) If the path is a file, display as file
    2.2) If the path is a directory, display as directory.

Pseudo Code:
```````````
public class CheckPath {

    public static void main(String[] args) {
        File filePath = new File(#file);
        if (filePath.isFile()) {
            System.out.println("The given path is file");
        } else {
            System.out.println("The given path is directory");
        }
    }

}

*/

package com.training.java.ee.nio;

import java.io.File;

public class CheckPath {

    public static void main(String[] args) {
        File filePath = new File("src/com/training/java/ee/nio");
        if (filePath.isFile()) {
            System.out.println("The given path is file");
        } else {
            System.out.println("The given path is directory");
        }
    }

}