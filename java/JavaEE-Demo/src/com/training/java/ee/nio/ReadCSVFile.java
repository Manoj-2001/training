/*
8. Reading a CSV file using java.nio.Files API as List string with each 
row in CSV as a String

----------WBS----------

Requirement:
```````````
Reading a CSV file using java.nio.Files API as List string with each 
row in CSV as a String

Entity:
``````
-ReadCSVFile

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of some files
    1.1) If no file exists on the specified path throw exception
2) Read all lines of file using readAllLines method of Files and store it into list
3) Display line by line using forEach of Stream.

Pseudo Code:
```````````
public class ReadCSVFile {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get(#file location);
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            System.out.println(line);
        }
    }
}

*/

package com.training.java.ee.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadCSVFile {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("src/com/training/java/ee/nio/sample.csv");
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            System.out.println(line);
        }
    }
}