/*
15. Create two paths and test whether they represent same path

----------WBS----------

Requirement:
```````````
Create two paths and test whether they represent same path

Entity:
``````
-SamePathOrNot

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of some files
    1.1) If no file exists on the specified path throw exception
2) Check the paths using isSameFile method of Files.
    2.1) If paths are same, display the paths are same.
    2.1) Otherwise display as paths are not same.

Pseudo Code:
```````````
public class SamePathOrNot {

    public static void main(String[] args) throws NoSuchFileException {
        try {
            // getting three paths from system
            Path path1 = Paths.get(#path);
            Path path2 = Paths.get(#path);
            Path path3 = Paths.get(#path);

            // checking the paths are same
            checkPath(path1, path2);
            checkPath(path1, path3);
        } catch (IOException ioe) {
            System.out.println(ioe.getClass());
            System.out.println(ioe.getMessage());
        }
    }

}

*/

package com.training.java.ee.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SamePathOrNot {

    static void checkPath(Path p1, Path p2) throws IOException {
        if (Files.isSameFile(p1, p2)) {
            System.out.println("Same paths");
        } else System.out.println("Not same paths");
    }

    public static void main(String[] args) throws NoSuchFileException {
        try {
            // getting three paths from system
            Path path1 = Paths.get("src/com/training/java/ee/nio/");
            Path path2 = Paths.get("src/com/training/java/ee/nio/");
            Path path3 = Paths.get("src/com/training/java/ee/io/");

            // checking the paths are same
            checkPath(path1, path2);
            checkPath(path1, path3);
        } catch (IOException ioe) {
            System.out.println(ioe.getClass());
            System.out.println(ioe.getMessage());
        }
    }
}