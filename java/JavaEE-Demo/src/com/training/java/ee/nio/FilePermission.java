/*
13. Get the permission allowed for a file

----------WBS----------

Requirement:
```````````
Get the permission allowed for a file

Entity:
``````
-FilePermission

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of a file in File class
2) If file not exists
    2.1) Display as file not exists
3) check the file can be readable, writable and executable.

Pseudo Code:
```````````
public class FilePermission {

    public static void main(String[] args) {
        File file = new File(#path);
        boolean exist = file.exists();

        if (exist) {
            System.out.println("Executable: " + file.canExecute());
            System.out.println("Writable: " + file.canWrite());
            System.out.println("Readable: " + file.canRead());
        }
        else System.out.println("File not exists");
    }

}

*/

package com.training.java.ee.nio;

import java.io.File;

public class FilePermission {

    public static void main(String[] args) {
        File file = new File("src/com/training/java/ee/nio/demo.txt");
        boolean exist = file.exists();

        if (exist) {
            // checking the file can be readable, writable and executable
            System.out.println("Executable: " + file.canExecute());
            System.out.println("Writable: " + file.canWrite());
            System.out.println("Readable: " + file.canRead());
        } else System.out.println("File not exists");

    }

}
