/*
12. Number of files in a directory and number of directories in a directory

----------WBS----------

Requirement:
```````````
Number of files in a directory and number of directories in a directory

Entity:
``````
-DirectoryLength

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of a file in File class
2) Use list method to make list of directories
3) Get the count of list using length method.

Pseudo Code:
```````````
public class DirectoryLength {

    public static void main(String[] args) {
        // opening the file path
        File path = new File("src/com/training/java/ee/nio/");
        // converting the path to list of packages
        int directoriesCount = path.list().length;
        System.out.println("Number of directories: " + directoriesCount);
    }

}


*/

package com.training.java.ee.nio;

import java.io.File;

public class DirectoryLength {

    public static void main(String[] args) {
        File path = new File("src/com/training/java/ee/nio/");
        int directoriesCount = path.list().length;
        System.out.println("Number of directories: " + directoriesCount);
    }

}
