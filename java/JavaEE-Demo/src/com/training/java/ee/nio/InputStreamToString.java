/*
10. InputStream to String and vice versa

----------WBS----------

Requirement:
```````````
InputStream to String and vice versa

Entity:
``````
-InputStreamToString

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Use input stream to access the file
2) Use the input stream reader to read the file
3) Use buffered reader to get the buffered data
4) Read lines in buffered reader and append to string buffer
5) Finally print it as String.

Pseudo Code:
```````````
public class InputStreamToString {

    public static void main(String args[]) throws IOException {

        InputStreamReader reader = new InputStreamReader(new FileInputStream(path));
        BufferedReader bufferReader = new BufferedReader(reader);
        StringBuffer stringBuffer = new StringBuffer();

        String line;
        while ((line = bufferReader.readLine()) != null) {
            stringBuffer.append(line);
            stringBuffer.append("\n");
        }
        System.out.println(stringBuffer.toString());
    }

}
*/

package com.training.java.ee.nio;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InputStreamToString {

    public static void main(String args[]) throws IOException {

        String path = "src/com/training/java/ee/nio/demo.txt";
        // creating an InputStreamReader object
        InputStreamReader reader = new InputStreamReader(new FileInputStream(path));
        // Creating a BufferedReader object
        @SuppressWarnings("resource")
        BufferedReader bufferReader = new BufferedReader(reader);

        StringBuffer stringBuffer = new StringBuffer();

        String line;
        while ((line = bufferReader.readLine()) != null) {
            stringBuffer.append(line);
            stringBuffer.append("\n");
        }
        String data = stringBuffer.toString();
        System.out.println(data);

        String string = "Now String to Input Stream";
        // data is stored as byte array stream
        InputStream inputStream = new ByteArrayInputStream(string.getBytes());
        System.out.println(inputStream);

    }

}