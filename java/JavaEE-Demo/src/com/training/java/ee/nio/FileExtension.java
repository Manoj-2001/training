/*
14. Get the file names of all file with specific extension in a directory

----------WBS----------

Requirement:
```````````
Get the file names of all file with specific extension in a directory

Entity:
``````
-FileExtension

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of a file in File class
2) Filter the file based on the file name using FilenameFilter interface
3) Store to list
4) Display the filenames in list

Pseudo Code:
```````````
public class FileExtension {

    public static void main(String[] args) {
        File path = new File(#path);
        String[] files = path.list(new FilenameFilter() {

            public boolean accept(File dir, String name) {
                if (name.endsWith(".java")) {
                    return true;
                } else return false;
            }

        });
        for (String file : files) {
            System.out.println(file);
        }
    }

}

*/

package com.training.java.ee.nio;

import java.io.File;
import java.io.FilenameFilter;

public class FileExtension {

    public static void main(String[] args) {
        File path = new File("src/com/training/java/ee/nio");

        // filter based on file name ends with .java and store to list
        String[] files = path.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if (name.endsWith(".java")) {
                    return true;
                } else return false;
            }

        });
        for (String file : files) {
            System.out.println(file);
        }
    }

}
