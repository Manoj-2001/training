/*
4. Write a Java program to calculate your age

----------WBS----------

Requirement:
```````````
Write a Java program to calculate your age 

Entity:
``````
-CalculateAge

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the date of birth using local date
2) Get the present date using local date
3) Get age by find difference between birth date and present date using between method of Period

Pseudo Code:
```````````

public class CalculateAge {

    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.parse("2001-08-21");
        LocalDate now = LocalDate.now();

        Period ageDifference = Period.between(dateOfBirth, now);
    }
}

*/

package com.training.java.ee.dateandtime;

import java.time.LocalDate;
import java.time.Period;

public class CalculateAge {

    public static void main(String[] args) {
        // date of birth
        LocalDate dateOfBirth = LocalDate.parse("2001-08-21");
        // present date
        LocalDate now = LocalDate.now();
        // difference between present date and date of birth
        Period ageDifference = Period.between(dateOfBirth, now);

        System.out.format("%d years, %d months and %d days old.", 
                ageDifference.getYears(),
                ageDifference.getMonths(),
                ageDifference.getDays());
    }
}
