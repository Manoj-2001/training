/*
7. Write a Java program to get and display information
(year, month, day, hour, minute) of a default calendar

----------WBS----------

Requirement:
```````````
Write a Java program to get and display information
(year, month, day, hour, minute) of a default calendar

Entity:
``````
-CalendarDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an instance of Calendar
2) Get the year, month, date, hours, minutes from
calendar and display it

Pseudo Code:
```````````

public class CalendarDemo {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();

        // Get and display information of current year, month, date, hours,
        // minutes, seconds from the calendar
        calendar.get(Calendar.YEAR);
	calendar.get(Calendar.MONTH)
    }

}

*/

package com.training.java.ee.dateandtime;

import java.util.Calendar;

public class CalendarDemo {

    public static void main(String[] args) {
        // Create a default calendar
        Calendar calendar = Calendar.getInstance();

        // Get and display information of current year, month, date, hours,
        // minutes, seconds from the calendar
        System.out.println(
            "Year: " + calendar.get(Calendar.YEAR) + "\n" +
            "Month: " + calendar.get(Calendar.MONTH) + "\n" +
            "Date: " + calendar.get(Calendar.DATE) + "\n" +
            "Hours: " + calendar.get(Calendar.HOUR) + "\n" +
            "Minutes: " + calendar.get(Calendar.MINUTE) + "\n" +
            "Seconds: " + calendar.get(Calendar.SECOND)
        );
    }

}