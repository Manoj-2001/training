/*
1. How do you convert a Calendar to Date and vice-versa with example?

----------WBS----------

Requirement:
```````````
convert a Calendar to Date and vice-versa with example

Entity:
``````
-CalendarToDateConvertion 

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an instance of Calendar
2) Get time and store it into date and display it.
3) Create new date with milliseconds using currentTimeMillis
4) Set the time to calendar

Pseudo Code:
```````````

public class CalendarToDateConvertion {

    public static void main(String[] args) throws InterruptedException {

        Calendar calendar = new GregorianCalendar();
        Date date = calendar.getTime();
        System.out.println(date);

        Date newDate = new Date(System.currentTimeMillis());
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTime(newDate);
        System.out.println(newCalendar.getTime());
    }

}

*/

package com.training.java.ee.dateandtime;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarToDateConvertion {

    public static void main(String[] args) throws InterruptedException {

        // convert calendar to date
        Calendar calendar = new GregorianCalendar();
        Date date = calendar.getTime();
        System.out.println(date);

        // make the thread to sleep for some time
        Thread.sleep(3000);

        // convert date to calendar
        Date newDate = new Date(System.currentTimeMillis());
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTime(newDate);
        System.out.println(newCalendar.getTime());
    }

}
