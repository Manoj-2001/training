/*
2. Find the time difference using timer class ?

----------WBS----------

Requirement:
```````````
Find the time difference using timer class

Entity:
``````
-TimeDifferenceUsingTimer
-MyTimer

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the start time using start method in MyTimer timer object of MyTimer class
2) Get the end time using end method in MyTimer timer object of MyTimer class
3) Subtract the two times to get the delayed time by timer

Pseudo Code:
```````````
class MyTimer {
    private long startTime;
    private long endTime;

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public void end() {
        endTime = System.currentTimeMillis();
        System.out.println((endTime - startTime)/1000 + " secs");
    }
}

public class TimeDifferenceUsingTimer {

    public static void main(String[] args) throws InterruptedException {
        MyTimer timer = new MyTimer();

        timer.start();
        timer.end();
    }

}
*/

package com.training.java.ee.dateandtime;

class MyTimer {
    private long startTime;
    private long endTime;

    public void start() {
        startTime = System.currentTimeMillis();
        System.out.println("Timer started..");
    }

    public void end() {
        endTime = System.currentTimeMillis();
        System.out.println((endTime - startTime)/1000 + " secs");
        System.out.println("Timer stopped");
    }
}

public class TimeDifferenceUsingTimer {

    public static void main(String[] args) throws InterruptedException {
        MyTimer timer = new MyTimer();

        // this method gets the current time in milliseconds
        timer.start();

        // make the thread to sleep for some time
        Thread.sleep(3000);

        // this method gets the current time in milliseconds
        // and displays the difference of start and end time.
        timer.end();
    }

}
