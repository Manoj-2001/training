/*
11.Write a Java program to get a date before and after 1 year compares 
to the current date.

----------WBS----------

Requirement:
```````````
Write a Java program to get a date before and after 1 year compares 
to the current date.

Entity:
``````
-BeforeAndAfterCurrentYear 

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an instance of calendar
2) Get the current date in calendar
3) Get date after one year on calendar
4) Get date before one year on calendar
5) Compare the current date with one year before and after date

Pseudo Code:
```````````
public class BeforeAndAfterCurrentYear {

    public static void main(String[] args)
    {
      Calendar calendar = Calendar.getInstance();
      Date currentDate = calendar.getTime();

      calendar.add(Calendar.YEAR, 1); 
      Date dateAfterOneYear = calendar.getTime();

      calendar.add(Calendar.YEAR, -2); 
      Date dateBeforeOneYear = calendar.getTime();

      System.out.println(currentDate);
      System.out.println(dateBeforeOneYear);
      System.out.println(dateAfterOneYear);

      System.out.println(currentDate.compareTo(dateBeforeOneYear));
      System.out.println(currentDate.compareTo(dateAfterOneYear));
    }

}

*/

package com.training.java.ee.dateandtime;

import java.util.Calendar;
import java.util.Date;
public class BeforeAndAfterCurrentYear {

    public static void main(String[] args)
    {
      Calendar calendar = Calendar.getInstance();

      // current date
      Date currentDate = calendar.getTime();

      // next year
      calendar.add(Calendar.YEAR, 1); 
      Date dateAfterOneYear = calendar.getTime();

      // previous year
      calendar.add(Calendar.YEAR, -2); 
      Date dateBeforeOneYear = calendar.getTime();

      System.out.println("Current Date : " + currentDate);
      System.out.println("Date before 1 year : " + dateBeforeOneYear);
      System.out.println("Date after 1 year  : " + dateAfterOneYear);

      // comparing the current date with one year before and after the same date
      System.out.println(currentDate.compareTo(dateBeforeOneYear));
      System.out.println(currentDate.compareTo(dateAfterOneYear));
    }

}