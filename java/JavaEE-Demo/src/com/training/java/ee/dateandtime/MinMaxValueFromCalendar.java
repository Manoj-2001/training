/*
8. Write a Java program to get the maximum and minimum value
   of the year, month, week, date from the current date of a default calendar.

----------WBS----------

Requirement:
```````````
Java program to get the maximum and minimum value
of the year, month, week, date from the current date of a default calendar.

Entity:
``````
-MinMaxValueFromCalendar

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an instance of Calendar
2) Get the minimum year, month, week of month and date using getActualMinimum method
3) Get the maximum year, month, week of month and date using getActualMaximum method

Pseudo Code:
```````````
public class MinMaxValueFromCalendar {
    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();

        calendar.getActualMinimum(Calendar.YEAR);
        calendar.getActualMinimum(Calendar.MONTH);
        calendar.getActualMinimum(Calendar.WEEK_OF_MONTH);
        calendar.getActualMinimum(Calendar.DATE);

        calendar.getActualMaximum(Calendar.YEAR);
        calendar.getActualMaximum(Calendar.MONTH);
        calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        calendar.getActualMaximum(Calendar.DATE);
    }
}
*/

package com.training.java.ee.dateandtime;

import java.util.Calendar;

public class MinMaxValueFromCalendar {


    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();

        System.out.println("Minimum values from calendar:");

        int minYear = calendar.getActualMinimum(Calendar.YEAR);
        int minMonth = calendar.getActualMinimum(Calendar.MONTH);
        int minWeek = calendar.getActualMinimum(Calendar.WEEK_OF_MONTH);
        int minDate = calendar.getActualMinimum(Calendar.DATE);
        
        System.out.println("Year: " + minYear);
        System.out.println("Month: " + minMonth);
        System.out.println("Week: " + minWeek);
        System.out.println("Date: " + minDate);
 
        System.out.println("\n" + "Maximum values from calendar:");

        int maxYear = calendar.getActualMaximum(Calendar.YEAR);
        int maxMonth = calendar.getActualMaximum(Calendar.MONTH);
        int maxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        int maxDate = calendar.getActualMaximum(Calendar.DATE);

        System.out.println("Year: " + maxYear);
        System.out.println("Month: " + maxMonth);
        System.out.println("Week: " + maxWeek);
        System.out.println("Date: " + maxDate);
    }
}
