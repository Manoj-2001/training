/*
5. Write a Java program to get the information of current/given year
   5.1)check the given year is leap year or not? 
   5.2)Find the Length of the give year?

----------WBS----------

Requirement:
```````````
Check the given year is leap year or not
Find the Length of the give year

Entity:
``````
-CurrentYear

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the present year using Year.now() and store it into year variable
2) Check the year is leap year, then display it as a leap year
    2.1) Otherwise display it as not a leap year
3) Get the length of the year using length method

Pseudo Code:
```````````
public class CurrentYear {

    public static void main(String[] args) {
        Year year = Year.now();

        if (year.isLeap()) {
            System.out.println("leap year");
        } else System.out.println("not a leap year");

        System.out.format(year.length());
    }

}
*/

package com.training.java.ee.dateandtime;

import java.time.Year;

public class CurrentYear {

    public static void main(String[] args) {
        Year year = Year.now();
        System.out.println("Current year: " + year);

        if (year.isLeap()) {
            System.out.println(year + " is a leap year");
        } else System.out.println(year + " is not a leap year");

        System.out.format("Length of this year is %d days", year.length());
    }

}
