/*
9 Display the default time zone and Any three particular time zone as mentioned below.
     Sample output: 
        Displaying current of the particular TimeZones
        GMT-27-09-2020 Sunday 08:36:28 AM
        Europe/Copenhagen-27-09-2020 Sunday 10:36:28 AM
        Australia/Perth-27-09-2020 Sunday 04:36:28 PM
        America/Los_Angeles-27-09-2020 Sunday 01:36:28 AM 

----------WBS----------

Requirement:
```````````
Display the default time zone and Any three particular time zone

Entity:
``````
-TimeZoneDemo 

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an instance of SimpleDateFormat class with format specified
2) Get default, GMT, Asia/Kolkata, Australia/Perth time zones
3) Set the time zone to the date formater
4) Format the date using date formatter

Pseudo Code:
```````````
public class TimeZoneDemo {
    public static void main(String[] args) {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");

        Date date = new Date();

        // creating time zones of different places
        TimeZone defaultTimeZone = TimeZone.getDefault();
        TimeZone gmtTimeZone = TimeZone.getTimeZone("GMT");
        TimeZone asiaTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        TimeZone ausTimeZone = TimeZone.getTimeZone("Australia/Perth");

        // setting time zone and formatting into date format
        dateFormatter.setTimeZone(defaultTimeZone);
        dateFormatter.format(date);

        dateFormatter.setTimeZone(gmtTimeZone);
        dateFormatter.format(date);
    }

}
*/

package com.training.java.ee.dateandtime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneDemo {
    public static void main(String[] args) {
        // instantiate a date formatter
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");

        // creating a date object
        Date date = new Date();

        // creating time zones of different places
        TimeZone defaultTimeZone = TimeZone.getDefault();
        TimeZone gmtTimeZone = TimeZone.getTimeZone("GMT");
        TimeZone asiaTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        TimeZone ausTimeZone = TimeZone.getTimeZone("Australia/Perth");

        // setting time zone and formatting into date format
        dateFormatter.setTimeZone(defaultTimeZone);
        System.out.println("Default: " + dateFormatter.format(date));

        dateFormatter.setTimeZone(gmtTimeZone);
        System.out.println("\n" + "GMT: " + dateFormatter.format(date));

        dateFormatter.setTimeZone(asiaTimeZone);
        System.out.println("\n" + "Asia/Kolkata: " + dateFormatter.format(date));

        dateFormatter.setTimeZone(ausTimeZone);
        System.out.println("\n" + "Australia/Perth: " + dateFormatter.format(date));
    }

}
