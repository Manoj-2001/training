/*
12. write a Java program to convert ZonedDateTime to Date.

----------WBS----------

Requirement:
```````````
To convert ZonedDateTime to Date

Entity:
``````
-ZonedDateTimeToDate

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create current time with zone using now method of ZonedDateTime
2) Create an Instant using toInstant method
3) Get date from instant object of Instant
4) Display the date

Pseudo Code:
```````````
public class ZonedDateTimeToDate {

    public static void main(String args[]) {
        ZonedDateTime zdt = ZonedDateTime.now();
        Instant instant = zdt.toInstant();
        Date date = Date.from(instant);
        System.out.println(date);
    }

}
*/

package com.training.java.ee.dateandtime;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

public class ZonedDateTimeToDate {

    public static void main(String args[]) {
        // Create ZonedDateTime object
        ZonedDateTime zdt = ZonedDateTime.now();

        // Convert ZonedDateTime to Instant instance
        Instant instant = zdt.toInstant();

        // Create Date instance out of Instant
        Date date = Date.from(instant);

        // Print Date value
        System.out.println(date);
    }

}