/*
3. write a Java program to demonstrate schedule method calls of Timer class ?

----------WBS----------

Requirement:
```````````
To demonstrate schedule method calls of Timer class

Entity:
``````
-TimerClassSchedule 

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an object of the Timer class
2) Shedule the task using schedule method Timer class

Pseudo Code:
```````````
public class TimerClassSchedule {

    public static Timer timer;

    public static void main(String[] args) {

        TimerTask task = new TimerTask() {
	    int count = 1;
            public void run() {
                test();
            }
            private void test() {
                if (count == 5) {
                    timer.cancel();
                }
                System.out.println(count++);
            }
        };

        timer = new Timer();
        timer.schedule(task, 10000, 2000);
    }

}
*/

package com.training.java.ee.dateandtime;

import java.util.Timer;
import java.util.TimerTask;

public class TimerClassSchedule {

    public static Timer timer;

    public static void main(String[] args) {

        TimerTask task = new TimerTask() {

            int count = 1;

            @Override
            public void run() {
                test();
            }

            private void test() {
                if (count == 5) {
                    timer.cancel();
                }
                System.out.println(count++);
            }

        };

        timer = new Timer();
        timer.schedule(task, 10000, 2000); // schedule(task, delay, period)
    }

}
