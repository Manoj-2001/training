/*
6. Write a Java program to get the dates 10 days before and after today.
   6.1)using local date 
   6.2)using calendar

----------WBS----------

Requirement:
```````````
To get the dates 10 days before and after today using local date
and using calendar

Entity:
``````
-TenDaysBeforeAndAfter

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the local date using now method
2) Get the local date after and before ten days using plusDays method
3) Create a calendar instance
4) Get ten days after and before in calendar using add method in Calendar.DAY_OFMONTH

Pseudo Code:
```````````
public class TenDaysBeforeAndAfter {

    public static void main(String[] args) {

        LocalDate today = LocalDate.now();

        System.out.println(today.plusDays(-10));
        System.out.println(today.plusDays(10));

        // Printing the present calendar
        System.out.println(calendar.getTime());

        // Printing 10 days before calendar
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        System.out.println(calendar.getTime());

        // Printing 10 days after calendar
        calendar.add(Calendar.DAY_OF_MONTH, 10);
        System.out.println(calendar.getTime());
    }

}
*/

package com.training.java.ee.dateandtime;

import java.time.LocalDate;
import java.util.Calendar;

public class TenDaysBeforeAndAfter {

    public static void main(String[] args) {

        System.out.println("Local Date:");

        // getting the current date using local date
        LocalDate today = LocalDate.now();
        System.out.println("Current Date: " + today);

        // getting the date before and after 10 days
        System.out.println("10 days before: " + today.plusDays(-10));
        System.out.println("10 days after: " + today.plusDays(10) + "\n");

        System.out.println("Calendar:");

        // creating two calendars
        Calendar calendar = Calendar.getInstance();
        Calendar newCalendar = Calendar.getInstance();

        // Printing the present calendar
        System.out.println(calendar.getTime());

        // Printing 10 days before calendar
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        System.out.println(calendar.getTime());

        // Printing 10 days after calendar
        newCalendar.add(Calendar.DAY_OF_MONTH, 10);
        System.out.println(newCalendar.getTime());
    }

}
