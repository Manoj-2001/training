/*
4) Find out the IP Address and host name of your local computer.

----------WBS----------

Requirement:
```````````
Find out the IP Address and host name of your local computer.

Entity:
``````
-LocalComputer

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the local host address and host name using InetAddress.
2) Try to get the address of website or web browser using getByName
method of InetAddress.
    2.1) Otherwise throw an exception.

Pseudo Code:
```````````
class Client {
    protected Socket socket = null;
    protected DataInputStream dataInputStream;
    protected DataOutputStream dataOutputStream;

    Client (String address, int port) {
        // establish connection
        try {
            socket = new Socket(address, port);
            dataInputStream = new DataInputStream(System.in);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            String message = "";
            while (message != "out") {
                message = dataInputStream.readLine();
                dataOutputStream.writeUTF(message);
            }

        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
}

*/

package com.training.java.ee.networking;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class LocalComputer {
    public static void main(String[] args) {
        try {

            // getting address of local host
            InetAddress address = InetAddress.getLocalHost();
            System.out.println("IP address: " + address.getHostAddress());
            System.out.println("Host name: " + address.getHostName());

            // getting address of a web browser
            InetAddress googleAddress = InetAddress.getByName("google.co.in");
            System.out.println("Google address: " + googleAddress);
        } catch (UnknownHostException e) {
            System.out.println("Couldn't able to find the address :(");
        }
    }
}