/*

1) Build a server side program using Socket class for Networking.

----------WBS----------

Requirement:
```````````
Build a server side program using Socket class for Networking.

Entity:
``````
-Server
-ServerSide

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a server with port.
2) Wait for client.
3) If client established the connection.
    3.1) Process the request
    3.2) Send the response.
    3.3) Close the connection properly.

Pseudo Code:
```````````
class Server {

    protected ServerSocket serverSocket = null;
    protected Socket socket = null;

    public Server(int port) {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server Ready!");

            System.out.println("Waiting for client...");
            socket = serverSocket.accept();
            System.out.println("Client accepted");

            boolean out = false;
            while(!out) {
                // main loop
                // processing the request and response happens here
            }

            // close connection
            socket.close();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
}

*/

package com.training.java.ee.networking;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class Server {

    protected ServerSocket serverSocket = null;
    protected Socket socket = null;
    protected DataInputStream dataInputStream = null;

    public Server(int port) {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server Ready!");

            System.out.println("Waiting for client...");
            // client socket address stored on this socket
            // from which data can be retrieved.
            socket = serverSocket.accept();
            System.out.println("Client accepted");

            // getting the data as stream from client's socket
            dataInputStream = new DataInputStream(
                    new BufferedInputStream(socket.getInputStream()));

            String message = "";
            while(message != "out") {
                // reading the message from data stream
                message = dataInputStream.readUTF();
                System.out.println(message);
            }

            // close connection
            socket.close();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
}

public class ServerSide {

    public static void main(String[] args) {
        new Server(5060);
    }

}
