/*

2) Build a client side program using ServerSocket class for Networking.

----------WBS----------

Requirement:
```````````
Build a client side program using ServerSocket class for Networking.

Entity:
``````
-Client
-ClientSide

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a client socket with server address and server port.
2) If connection not established.
    2.1) Throw an exception.
3) Read the input through from the user.
4) Send the request to the Server.
5) Wait for server response.
6) Close the connection properly.

Pseudo Code:
```````````
class Client {
    protected Socket socket = null;
    protected DataInputStream dataInputStream;
    protected DataOutputStream dataOutputStream;

    Client (String address, int port) {
        // establish connection
        try {
            socket = new Socket(address, port);
            dataInputStream = new DataInputStream(System.in);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            String message = "";
            while (message != "out") {
                message = dataInputStream.readLine();
                dataOutputStream.writeUTF(message);
            }

        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
}

*/

package com.training.java.ee.networking;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

class Client {
    protected Socket socket = null;
    protected DataInputStream dataInputStream;
    protected DataOutputStream dataOutputStream;

    Client (String address, int port) {
        // establish connection
        try {
            socket = new Socket(address, port);
            System.out.println("Server Connected");

            // getting input as stream from terminal
            dataInputStream = new DataInputStream(System.in);

            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            String message = "";
            while (message != "out") {
                message = dataInputStream.readLine();
                // writing message to client socket
                dataOutputStream.writeUTF(message);
            }

        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
}

public class ClientSide {

    public static void main(String[] args) {
        new Client("127.0.0.1", 5060);
    }

}
