/*
5) Create a program  for url class and url connection class in networking.

----------WBS----------

Requirement:
```````````
Create a program  for url class and url connection class in networking.

Entity:
``````
-UrlClassAndUrlConnection

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a url object of URL class and pass the url as parameter
2) Get the protocol, host, port, and file in url
3) Open the url connection using HttpURLConnection
4) Get the header fields from connection.

Pseudo Code:
```````````
public class UrlClassAndUrlConnection {

    public static void main(String[] args) {
        // using url class
        try {
            URL url = new URL(#url);

            System.out.println("Protocol: " + url.getProtocol());
            System.out.println("Host: " + url.getHost());
            System.out.println("Port: " + url.getPort());
            System.out.println("File Name: " + url.getFile());

            // using url connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            for (int id = 1; id <= 8; id++) {
                System.out.println(connection.getHeaderFieldKey(id) +
                        "  -->  " + connection.getHeaderField(id));
            } connection.disconnect();
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

}
*/

package com.training.java.ee.networking;

import java.net.HttpURLConnection;
import java.net.URL;

public class UrlClassAndUrlConnection {

    public static void main(String[] args) {
        // using url class
        try {
            URL url = new URL("https://www.geeksforgeeks.org/java-tutorial/");

            System.out.println("Protocol: " + url.getProtocol());
            System.out.println("Host: " + url.getHost());
            System.out.println("Port: " + url.getPort());
            System.out.println("File Name: " + url.getFile());

            System.out.println();

            // using url connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            for (int id = 1; id <= 8; id++) {
                System.out.println(connection.getHeaderFieldKey(id) +
                        "  -->  " + connection.getHeaderField(id));
            } connection.disconnect();
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

}
