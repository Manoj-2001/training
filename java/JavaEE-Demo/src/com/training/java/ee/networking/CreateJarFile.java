package com.training.java.ee.networking;

/*
3) Create an jar file inside eclipse.

    1) In the Package Explorer select the items that you want to export. If you
    want to export all the classes and resources in the project just select the 
    project.

    2) Click on the File menu and select Export.

    3) In the filter text box of the first page of the export wizard type in "JAR".

    4) Under the Java category select JAR file.

    5) Enter the JAR file name and folder.

    6) The default is to export only the classes. To export the source code 
    also, click on the "Export Java source files and resources" check box.

    7) Click on Next to change the JAR packaging options.

    8) Click on Next to change the JAR Manifest specification.

    9) Click on Finish.

    10) Now, navigate to the location you specified for the jar.
    The icon you see and the behavior you get if you double click it will vary 
    depending on how your computer is set up.
*/