/*
6) Create a program using HttpUrlconnection in networking.
*/

/*
5) Create a program  for url class and url connection class in networking.

----------WBS----------

Requirement:
```````````
Create a program  for url class and url connection class in networking.

Entity:
``````
-HttpUrlConnectionDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Open the url connection using HttpURLConnection
2) Get the connection expiration using getExpiration method
3) Get the length of the content using getContentLength method

Pseudo Code:
```````````
public class HttpUrlConnectionDemo {

    public static void main(String[] args) {
        URL url = null;
        try {
            url = new URL(#url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Connection Expiration: " + connection.getExpiration());
            System.out.println("Content length: " + connection.getContentLength());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
*/

package com.training.java.ee.networking;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpUrlConnectionDemo {

    public static void main(String[] args) {
        URL url = null;
        try {
            url = new URL("https://www.guvi.in/code-kata");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Connection Expiration: " + connection.getExpiration());
            System.out.println("Content length: " + connection.getContentLength());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
