/*
5) Write java programs for stream tokenizer methods :-
5.1) commentChar() method
5.2) eollsSignificant() method
5.3) lineno() method
5.4) lowerCaseMode() method

----------WBS----------

Requirement:
```````````
5) Write java programs for stream tokenizer methods :-
    commentChar() method
    eollsSignificant() method
    lineno() method
    lowerCaseMode() method

Entity:
``````
-StreamTokenizerMethods

Method Signature:
````````````````
-public static void commentCharMethod(StreamTokenizer tokenizer)
-public static void eolIsSignificantMethod(StreamTokenizer tokenizer)
-public static void linenoMethod(StreamTokenizer tokenizer)
-public static void lowerCaseModeMethod(StreamTokenizer tokenizer)
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file using Reader.
2) BufferedReader to read the date in file obtained by reader.
3) StreamTokenizer to parse the data into tokens.
3) Invoke commentChar method in streamTokenizer.
4) invoke the eolIsSignificant method of StreamTokenizer to determine
whether or not ends of line are treated as tokens
5) Invoke the lineno method to get the line number.
6) Invoke the lowerCaseMode of StreamTokenizer to determine
whether or not word token are automatically lowercased.

Pseudo Code:
```````````
public class StreamTokenizerMethods {

    public static void commentCharMethod(StreamTokenizer tokenizer) throws IOException {

        // Use of commentChar() method
        tokenizer.commentChar('a');
        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;
            }
        }
    }

    public static void eolIsSignificantMethod(StreamTokenizer tokenizer) throws IOException {

        boolean arg = true;
        // Use of eolIsSignificant() method
        tokenizer.eolIsSignificant(arg);
        // Here the 'arg' is set true, so EOL is treated as a token

        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_EOL:
                System.out.println("End of Line encountered.");
                break;
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;

            }
        }

    }

    public static void linenoMethod(StreamTokenizer tokenizer) throws IOException {

        tokenizer.eolIsSignificant(true);
        // Use of lineno() method
        // to get current line no.
        System.out.println("Line Number:" + tokenizer.lineno());

        tokenizer.commentChar('a');
        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_EOL:
                System.out.println("");
                System.out.println("Line No. : " + tokenizer.lineno());
                break;
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;

            }
        }
    }

    public static void lowerCaseModeMethod(StreamTokenizer tokenizer) throws IOException {

        // Use of lowerCaseMode() method to Here, the we have set the Lower Case Mode ON
        boolean arg = true;
        tokenizer.lowerCaseMode(arg);

        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;

            }
        }

    }

    public static void main(String[] args) throws IOException {

        FileReader reader = new FileReader(#path");
        BufferedReader bufferReader = new BufferedReader(reader);
        StreamTokenizer tokenizer = new StreamTokenizer(bufferReader);

        commentCharMethod(tokenizer);
        eolIsSignificantMethod(tokenizer);
        linenoMethod(tokenizer);
        lowerCaseModeMethod(tokenizer);
    }
}
*/

package com.training.java.ee.serialization;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

public class StreamTokenizerMethods {

    public static void commentCharMethod(StreamTokenizer tokenizer) throws IOException {

        // Use of commentChar() method
        tokenizer.commentChar('a');
        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;
            }
        }
    }

    public static void eolIsSignificantMethod(StreamTokenizer tokenizer) throws IOException {

        boolean arg = true;
        // Use of eolIsSignificant() method
        tokenizer.eolIsSignificant(arg);
        // Here the 'arg' is set true, so EOL is treated as a token

        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_EOL:
                System.out.println("End of Line encountered.");
                break;
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;

            }
        }

    }

    public static void linenoMethod(StreamTokenizer tokenizer) throws IOException {

        tokenizer.eolIsSignificant(true);
        // Use of lineno() method
        // to get current line no.
        System.out.println("Line Number:" + tokenizer.lineno());

        tokenizer.commentChar('a');
        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_EOL:
                System.out.println("");
                System.out.println("Line No. : " + tokenizer.lineno());
                break;
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;

            }
        }
    }

    public static void lowerCaseModeMethod(StreamTokenizer tokenizer) throws IOException {

        // Use of lowerCaseMode() method to Here, the we have set the Lower Case Mode ON
        boolean arg = true;
        tokenizer.lowerCaseMode(arg);

        int token;
        while ((token = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
            switch (token) {
            case StreamTokenizer.TT_NUMBER:
                System.out.println("Number : " + tokenizer.nval);
                break;
            case StreamTokenizer.TT_WORD:
                System.out.println("Word : " + tokenizer.sval);
                break;

            }
        }

    }

    public static void main(String[] args) throws IOException {

        FileReader reader = new FileReader("src/com/training/java/ee/serialization/sample.txt");
        BufferedReader bufferReader = new BufferedReader(reader);
        StreamTokenizer tokenizer = new StreamTokenizer(bufferReader);

        System.out.println("Using commentCharMethod");
        commentCharMethod(tokenizer);
        System.out.println("Using eollsSignificantMethod");
        eolIsSignificantMethod(tokenizer);
        System.out.println("Using linenoMethod");
        linenoMethod(tokenizer);
        System.out.println("Using lowerCaseModeMethod");
        lowerCaseModeMethod(tokenizer);
    }
}