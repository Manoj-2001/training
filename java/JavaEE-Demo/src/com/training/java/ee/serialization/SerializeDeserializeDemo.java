/*
4) Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File
and print the content.

----------WBS----------

Requirement:
```````````
Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File
and print the content.

Entity:
``````
-SerializeDeserializeDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a output stream file for specified path
2) Create a ObjectOutputStream to store stream of object
3) Write the student object of Student class to ObjectOutputStream, now the object
serialized.
4) Close the files properly.
5) Create a input stream file for specified path
6) Create a ObjectInputStream to import object byte stream
7) Read student object of Student class from ObjectOutputStream, now the object
de-serialized.
8) Close the files properly.

Pseudo Code:
```````````
public class SerializeDeserializeDemo {

    public static void main(String[] args) throws ClassNotFoundException {

        Student student = new Student();
        // get name, id, phone number and address from user

        // serialization
        try {
            FileOutputStream outputFile = new FileOutputStream(#path);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputFile);
            objectOutputStream.writeObject(student);
            objectOutputStream.close();
            outputFile.close();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }

        // de-serialization
        try {
            FileInputStream inputFile = new FileInputStream(#path);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputFile);
            Student studObject = (Student) objectInputStream.readObject();
            objectInputStream.close();
            inputFile.close();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

}
*/

package com.training.java.ee.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeDeserializeDemo {

    public static void main(String[] args) throws ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);

        Student student = new Student();
        System.out.println("Enter the student name:");
        student.name = scanner.nextLine();
        System.out.println("Enter the student id:");
        student.studentId = scanner.nextInt();
        System.out.println("Enter the student's Address:");
        student.address = scanner.next();
        System.out.println("Enter the student phone number:");
        student.phoneNumber = scanner.nextLong();

        // serializing the object of a Student class
        try {
            FileOutputStream outputFile = new FileOutputStream("src/com/training/java/ee/serialization/studentInfo.ser");
            // creating a object output stream file and passing serializing file
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputFile);
            // writing the student object to object stream file
            objectOutputStream.writeObject(student);
            // after work done closing the files
            objectOutputStream.close();
            outputFile.close();
            System.out.println("Object serialized!");
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }

        // deserializing the object of a Student class
        try {
            FileInputStream inputFile = new FileInputStream("src/com/training/java/ee/serialization/studentInfo.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(inputFile);

            // reading the student object from object stream file
            Student studObject = (Student) objectInputStream.readObject();
            System.out.println("\n" + "Object de-serialized!" + "\n");
            // closing the files
            objectInputStream.close();
            inputFile.close();

            // invoking the method of deserialized object
            studObject.contactStudent();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

}
