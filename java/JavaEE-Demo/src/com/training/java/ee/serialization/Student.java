package com.training.java.ee.serialization;

import java.io.Serializable;

public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    public String name;
    public int studentId;
    public String address;
    public long phoneNumber;

    public void contactStudent() {
        System.out.println("Contact details: ");
        System.out.println(name + ", Contact number: " + phoneNumber);
        System.out.println(address);
    }

}
