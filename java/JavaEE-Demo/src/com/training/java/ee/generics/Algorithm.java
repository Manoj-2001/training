/*
6. Will the following class compile? If not, why?

public final class Algorithm {
    public static <T> T max(T x, T y) {
        return x > y ? x : y;
    }
}

*/
package com.training.java.ee.generics;

class Max<T> {
    public static <T> Integer max(Integer x, Integer y) {
        return x == y ? x : y;
    }
}

public class Algorithm {

    public static void main(String[] args) {
        System.out.println(Max.max(1, 2));
    }

}
/*
 * Answer
 * ``````
 * If the type in the static method marked as T then, compile time error occurs
 * because of the > operator. It is used to check only Numerical values.
 * So, making return type and data type is necessary to avoid error.
 */
