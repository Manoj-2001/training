/*

2. Why except iterator() method in iterable interface, are not necessary
to define in the implemented class?

*/

package com.training.java.ee.generics;

/*
 * Answer
 * ``````
 * The Iterable interface has 3 methods.
 * -> iterator()
 * -> forEach(Consumer<? super T> action)
 * -> spliterator()
 * 
 * The iterator method is a abstract method, it only declared in the interface. the
 * definition should be given to the implementing class.
 * 
 * The other 2 methods have default declaration and its definition itself in the
 * interface.
 * 
 * */
