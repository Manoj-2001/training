/*
8. Write a generic method to find the maximal element in 
the range [begin, end) of a list.

----------WBS----------

Requirement:
```````````
To write a generic method to find the maximal element in the list.

Entity:
``````
-MaximalElement

Function Declaration:
````````````````````
-public static <T> T largest(List<T> list, int startIndex, int endIndex)
-public static void main(String args[])

Jobs to be done:
```````````````
1) Create a list and store some numbers to list
2) Create a largest method that can find the largest number in between the
range of starting and ending index

Pseudo Code:
```````````
public class MaximalElement {

    public static <T> T largest(List<T> list, int startIndex, int endIndex) {
        T max = list.get(startIndex);
        for (int start = startIndex; start <= endIndex; start++) {
            if (max < list.get(start)) {
                max = list.get(start);
            }
        }
        return max;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(21, 321, 3, 40, 7, 16, 97, 28, 900, 10);
        System.out.println(largest(list, 1, 7));
    }
}

*/
package com.training.java.ee.generics;

import java.util.Arrays;
import java.util.List;

public class MaximalElement {

    public static <T> T largest(List<T> list, int startIndex, int endIndex) {
        T max = list.get(startIndex);
        for (int start = startIndex; start <= endIndex; start++) {
            if ((Integer)max < (Integer)list.get(start)) {
                max = list.get(start);
            }
        }
        return max;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(21, 321, 3, 40, 7, 16, 97, 28, 900, 10);
        System.out.println(largest(list, 1, 7));
    }
}

