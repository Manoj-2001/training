/*
1. Write a program to print employees name list by implementing iterable interface.

----------WBS----------

Requirement:
```````````
To write a java program to print employees name list by implementing
Iterable interface.

Entity:
``````
-IterableInterface
-MyList

Function Declaration:
````````````````````
-public Iterator<Type> iterator()
-public void addElements(Type ... varArgs)
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a class which implements Iterable interface and
definition is given in the implemented methods.
2) Creating a list of employees called employeeNameList.
3) For each employee in employeeNameList 
    3.1) display the name of employee.

Pseudo Code:
```````````
class MyList implements Iterable {

    public List myList;

    public void addElements(Type ... varArgs) {
        myList = Arrays.asList(varArgs);
    }

    @Override
    public Iterator iterator() {
        return myList.iterator();
    }

}

public class IterableInterface {

    public static void main(String[] args) {

        MyList employeeNameList = new MyList();
        employeeNameList.addElements("Navin", "Saran", "Hari", "Pranav", "Naveen");
        Iterator items = employeeNameList.iterator();
        while(items.hasNext()) {
            System.out.println(items.next());
        }
    }

}

*/
package com.training.java.ee.generics;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyList<Type> implements Iterable<Type> {

    public List<Type> myList;

    public void addElements(Type ... varArgs) {
        myList = Arrays.asList(varArgs);
    }

    @Override
    public Iterator<Type> iterator() {
        return myList.iterator();
    }

}

public class IterableInterface {

    public static void main(String[] args) {

        MyList<String> employeeNameList = new MyList<>();
        employeeNameList.addElements("Navin", "Saran", "Hari", "Pranav", "Naveen");
        Iterator<?> items = employeeNameList.iterator();
        while(items.hasNext()) {
            System.out.println(items.next());
        }
    }

}
