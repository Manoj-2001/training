/*
Problem statement:
`````````````````
4. Write a program to demonstrate generics - for loop,
for list, set and map.

----------WBS----------

Requirement:
```````````
Write a program to demonstrate generics - for loop, for list, set and map.

Entity:
``````
-GenericsCollection

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a list of students the students list can contain only String
2) Create a uniqueNumbers set that can contains only Integer.
3) Create a colleges map that can contains only college name and college code.
4) Display all elements of specific type.

Pseudo Code:
```````````
public class GenericsCollection {

    public static void main(String[] args) {

        List<String> students = Arrays.asList("Hem", "Barath", "Prabu", "Dinesh", "Sudan");

        Set<Integer> uniqueNumbers = new HashSet<>();
        uniqueNumbers.add(10);
        uniqueNumbers.add(22);
        uniqueNumbers.add(21);
        uniqueNumbers.add(3);
        uniqueNumbers.add(22);

        Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2702, "BAIT");
        colleges.put(2764, "KPRIET");
        colleges.put(2706, "MCET");
        colleges.put(2006, "PSG");
        colleges.put(2726, "SNS");

        for (Map.Entry<Integer, String> college : colleges.entrySet()) {
            System.out.println(college.getKey() +" - " + colleges.get(college.getKey()));
        }

    }

}
*/

package com.training.java.ee.generics;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GenericsCollection {

    public static void main(String[] args) {

        @SuppressWarnings("unused")
        List<String> students = Arrays.asList("Hem", "Barath", "Prabu", "Dinesh", "Sudan");

        Set<Integer> uniqueNumbers = new HashSet<>();
        uniqueNumbers.add(10);
        uniqueNumbers.add(22);
        uniqueNumbers.add(21);
        uniqueNumbers.add(3);
        uniqueNumbers.add(22);

        Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2702, "BAIT");
        colleges.put(2764, "KPRIET");
        colleges.put(2706, "MCET");
        colleges.put(2006, "PSG");
        colleges.put(2726, "SNS");

        for (Map.Entry<Integer, String> college : colleges.entrySet()) {
            System.out.println(college.getKey() +" - " +
                    colleges.get(college.getKey()));
        }

    }

}
