/*
5. Write a generic method to count the number of elements in a collection
that have a specific property 
(for example, odd integers, prime numbers, palindromes).

----------WBS----------

Requirement:
```````````
To count the number of elements in a collection
that have a specific property.

Entity:
``````
-NumberOfElements

Function Declaration:
````````````````````
-public static int countEven(List<Integer> list)
-public static int countOdd(List<Integer> list)
-public static void main(String args[])

Jobs to be done:
```````````````
1) Create a elements list and store some numbers in elements list
2) Counts the number of even numbers occurred in the list passed.
3) Counts the number of odd numbers occurred in the passed list.

Pseudo Code:
```````````
public class NumberOfElements {

    public static int countEven(List<Integer> list) {
        int count = 0;
        for (int element : list) {
            if (element % 2 == 0) {
                count ++;
            }
        } return count;
    }

    public static int countOdd(List<Integer> list) {
        int count = 0;
        for (int element : list) {
            if (element % 2 != 0) {
                count ++;
            }
        } return count;
    }

    public static void main(String[] args) {
        List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        System.out.println(countEven(elements));
    }

}

*/
package com.training.java.ee.generics;

import java.util.Arrays;
import java.util.List;

public class NumberOfElements {

    public static int countEven(List<Integer> list) {
        int count = 0;
        for (int element : list) {
            if (element % 2 == 0) {
                count ++;
            }
        } return count;
    }

    public static int countOdd(List<Integer> list) {
        int count = 0;
        for (int element : list) {
            if (element % 2 != 0) {
                count ++;
            }
        } return count;
    }

    public static void main(String[] args) {
        List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        System.out.println("Even numbers in list: " + countEven(elements));
        System.out.println("Odd numbers in list: " + countOdd(elements));
    }

}
