/*
3. Write a program to demonstrate generics - class objects as type literals.

----------WBS----------

Requirement:
```````````
To write a program for class objects as type literals.

Entity:
``````
-ObjectAsClassLiteral

Function Declaration:
````````````````````
-public static <T> boolean CheckIsPrimitive(Class<?> classObject)
-public static void main(String args[])

Jobs to be done:
```````````````
1) Create a user defined static method CheckIsPrimitive which accepts 
object of class the method returns boolean.
2) Get the class name using the getClass method.
3) Get the name using getName method.

Pseudo Code:
```````````
public class ObjectAsClassLiteral {

    public static <T> boolean CheckIsPrimitive(Class classObject) {
        return classObject.isPrimitive();
    }

    public static void main(String[] args) {
        Class strObject = String.class;
        System.out.println("Is primitive? " + CheckIsPrimitive(strObject));
        System.out.println(strObject.getClass().getName());
        System.out.println(strObject.getName());
        System.out.println();
        Class intObject = int.class;
        System.out.println("Is primitive? " + CheckIsPrimitive(intObject));
        System.out.println(intObject.getClass().getName());
        System.out.println(intObject.getName());
    }

}

*/
package com.training.java.ee.generics;

public class ObjectAsClassLiteral {

    public static <T> boolean CheckIsPrimitive(Class<?> classObject) {
        return classObject.isPrimitive();
    }

    public static void main(String[] args) {
        Class<String> strObject = String.class;
        System.out.println("Is primitive? " + CheckIsPrimitive(strObject));
        System.out.println(strObject.getClass().getName());
        System.out.println(strObject.getName());
        System.out.println();
        Class<Integer> intObject = int.class;
        System.out.println("Is primitive? " + CheckIsPrimitive(intObject));
        System.out.println(intObject.getClass().getName());
        System.out.println(intObject.getName());
    }

}
