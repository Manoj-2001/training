/*
7. Write a generic method to exchange the positions of two different 
elements in an array.

----------WBS----------

Requirement:
```````````
To write a generic method to exchange the positions of two different 
elements in an array.

Entity:
``````
-ExchangePositions

Function Declaration:
````````````````````
-public static <T> List<T> changePosition(List<T> list, int first, int second)
-public static void main(String args[])

Jobs to be done:
```````````````
1) Create a list and store some numbers to the list
2) Create a ExchangePositions method which can change the positions of 
two numbers in list
3) Change the positions of two numbers in list by index.

Pseudo Code:
```````````
public class ExchangePositions {

    public static <T> List<T> changePosition(List<T> list, int first, int second) {
        T temp = list.get(first);
        list.set(first, list.get(second));
        list.set(second, temp);
        return list;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(21, 321, 3, 40, 7, 16, 97);
        System.out.println("Before Exchange: \n" + list);
        list = changePosition(list, 2, 6);
        System.out.println("After Exchange: \n" + list);
    }

}

*/
package com.training.java.ee.generics;

import java.util.Arrays;
import java.util.List;

public class ExchangePositions {

    public static <T> List<T> changePosition(List<T> list, int first, int second) {
        T temp = list.get(first);
        list.set(first, list.get(second));
        list.set(second, temp);
        return list;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(21, 321, 3, 40, 7, 16, 97);
        System.out.println("Before Exchange: \n" + list);
        list = changePosition(list, 2, 6);
        System.out.println("After Exchange: \n" + list);
    }

}
