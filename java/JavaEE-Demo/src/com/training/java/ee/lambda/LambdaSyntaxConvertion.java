package com.training.java.ee.lambda;
/*

6) Convert this following code into a simple lambda expression
    int getValue(){
        return 5;
    }

Answer:
``````
-----------------------------------------------------------------
() -> 5;
-----------------------------------------------------------------
*/