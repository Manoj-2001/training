/*
3) Write a program to print the volume of a Rectangular box using lambda expression

----------WBS----------

Requirement:
```````````
To print the volume of a Rectangular box.

Entity:
``````
-RectangularBox
-VolumeOfBox

Interface:
`````````
-Box

Function Declaration:
````````````````````
-public static void main(String[] args)
-public double volume()

Jobs to be done:
```````````````
1) Create a functional interface Box with a abstract method volume()
2) Create a constructor of RectangularBox class which accepts length,
height and width of rectangular box.
3) The definition of Interface volume method given to a boxVolume variable.
4) Invoking the volume method by boxVolume variable
5) the value returned is multiplication of width, height and length.

Pseudo Code:
```````````
interface Box {
    public double volume();
}

class RectangularBox {
    private double length, width, height;
    
    Box boxVolume = () -> this.length * this.width * this.height;
    
    RectangularBox(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }
}

public class VolumeOfBox {

    public static void main(String[] args) {
        RectangularBox box = new RectangularBox(50.5, 35.2, 20);
        double volume = box.boxVolume.volume();
        System.out.print(volume);
    }

}

*/
package com.training.java.ee.lambda;

interface Box {
    public double volume();
}

class RectangularBox {
    private double length, width, height;
    
    Box boxVolume = () -> this.length * this.width * this.height;
    
    RectangularBox(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
        System.out.println(String.format("Box created with %.2f cm length, %.2f cm width, %.2f cm height", this.length, this.width, this.height));
    }
}

public class VolumeOfBox {

    public static void main(String[] args) {
        RectangularBox box = new RectangularBox(50.5, 35.2, 20);
        double volume = box.boxVolume.volume();
        System.out.println("Volume of the box is " + volume +" cm�");
    }

}
