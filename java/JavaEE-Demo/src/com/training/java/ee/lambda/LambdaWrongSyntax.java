package com.training.java.ee.lambda;
/*

5) What is wrong with the following interface? and fix it.
        (int x, y) -> x + y;

-----------------------------------------------------------------
Answer
``````
(int x, y) -> x + y;

This syntax throws error while compiling, because in lambda expression we either
use access specifiers or we don't use it, in both the cases there won't be any
error. but, if the access specifier is specified for particular variable then it
will results in error.

Preference:
    (x, y) -> x + y;
    (int x, int y) -> x + y;
-----------------------------------------------------------------
*/
