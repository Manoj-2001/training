/*
1) Write a Lambda expression program with a single method interface
To concatenate two strings

----------WBS----------

Requirement:
```````````
To concatenate two strings using Lambda expression.

Entity:
``````
-StringConcat

Interface:
`````````
-Concater

Function Declaration:
````````````````````
-public static void main(String[] args)
-public String concat(String firstname, String lastname)

Jobs to be done:
```````````````
1) Create a Concater interface with a abstract method concat
2) The definition of Interface concat method given to a concater variable.
3) Invoking the concat method using concater and passing first name and 
last name as arguments.
4) Display the merged string returned from method invoke.

Pseudo Code:
```````````
public interface Concater {
    public String concat(String firstName, String lastName);
}
public class StringConcat {
    public static void main(String[] args) {
        Concater concater = (firstName, lastName) -> firstName + " " + lastName;
        String myName = concater.concat("Manoj" + "Saravanan");
        System.out.print(myName);
    }
}
*/
package com.training.java.ee.lambda;

interface Concater {
    public String concat(String firstname, String lastname);
}

public class StringConcat {

    public static void main(String[] args) {
        Concater concater = (firstname, lastname) -> firstname + " " + lastname;
        String myName = concater.concat("Manoj", "Saravanan");
        System.out.println(myName);
    }

}
