/*
8) Code a functional program that can return the sum of elements of varArgs, passed
into the method of functional interface.

----------WBS----------

Requirement:
```````````
Functional code to print the sum of varArgs

Entity:
``````
-Adding
-VarArgsInLambda

Interface:
`````````
-Addable

Function Declaration:
````````````````````
-public int add(int ... varArgs)
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a Addable Interface with abstract method add
2) Definition of Interface add method given to a addable variable.
3) Invoking the add method of addable variable in adding object.

Pseudo Code:
```````````
interface Addable {
    public int add(int ... varArgs);
}

public class VarArgsInLambda {
    public static void main(String[] args) {
        private int sum = 0;
        Addable addable = (int ... varArgs) -> {
            for (int varArg : varArgs) {
                sum += varArg;
            }
            return sum;
        };
        System.out.println(adding.addable.add(1,2,3,4,5));
    }
}

*/

package com.training.java.ee.lambda;

interface Addable {
    public int add(int ... varArgs);
}

class Adding {
    private int sum;
    Addable addable = (int ... varArgs) -> {
        for (int varArg : varArgs) {
            sum += varArg;
        }
        return sum;
    };
}

public class VarArgsInLambda {
    public static void main(String[] args) {
        Adding adding = new Adding();
        System.out.println(adding.addable.add(1,2,3,4,5));
    }
}
