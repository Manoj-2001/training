/*
What is Method Reference and its types.Write a program for each types 
with suitable comments.

----------WBS----------

Requirement:
```````````
Method Reference and its types with examples

Entity:
``````
-StaticMethodReference
-InstanceMethodReference
-ConstructorReference
-MethodReferenceDemo

Interface:
`````````
-Printer
-Items
-Average
-Adder

Function Declaration:
````````````````````
-public void greet(String yourName)
-public int compareElements(int element1, int element2)
-public int averageOfList()
-public void sum(double ... varArgs)
-public static void show(String name)
-public int calculateAverage()
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create functional interfaces with abstract methods.
2) Using the class referencing the method and assigning into the
object of interface.
3) In static method reference, the static method referred in the class and
is initialized to the object of interface.
4) In parameter method reference, the method is referred by class and no 
need of specifying parameters. It can refer the interface.
5) Similarly the instance method reference, the method is referred by the object
of the class.
6) The constructor method reference, it refers the constructor of a class and it
can compared with interface.

Pseudo Code:
```````````
interface Printer {
    public void greet(String yourName);
}

interface Items {
    public int compareElements(int element1, int element2);
}

interface Average {
    public int averageOfList();
}

interface Adder {
    public void sum(double ... varArgs);
}

class StaticMethodReference {
    public static void show(String name) {
        System.out.println(String.format("Hello %s!", name));
    }
}

class InstanceMethodReference {
    public int sum;
    public int value1;
    public int value2;

    public int calculateAverage() {
        sum = value1 + value2;
        int avg = (sum / 2);
        return avg;
    }
}

class ConstructorReference {
    ConstructorReference(double ... args) {
        int sum = 0;
        for (double arg : args) {
            sum += arg;
        }
        System.out.println(sum);
    }
}

public class MethodReferenceDemo {

    public static void main(String[] args) {
        Printer print = StaticMethodReference::show; //Static Method Reference
        print.greet("Manoj");

        Items item = Integer::compare; //Parameter Method Reference
        System.out.println(item.compareElements(2, 5));

        InstanceMethodReference classObject = new InstanceMethodReference();
        classObject.value1 = 3;
        classObject.value2 = 6;
        Average average = classObject::calculateAverage; //Instance Method Reference
        int result = average.averageOfList();
        System.out.println(result);

        Adder adder = ConstructorReference::new; //Constructor Reference
        adder.sum(1,2,3,4,5);
    }

}
*/

/*
Method reference is further simplified form of lambda expression.It has four types,
        1.Static Method Reference
        2.Parameter Method Reference
        3.Instance Method Reference
        4.Constructor Reference
*/
package com.training.java.ee.lambda;

interface Printer {
    public void greet(String yourName);
}

interface Items {
    public int compareElements(int element1, int element2);
}

interface Average {
    public int averageOfList();
}

interface Adder {
    public void sum(double ... varArgs);
}

class StaticMethodReference {
    public static void show(String name) {
        System.out.println(String.format("Hello %s!", name));
    }
}

class InstanceMethodReference {
    public int sum;
    public int value1;
    public int value2;

    public int calculateAverage() {
        sum = value1 + value2;
        int avg = (sum / 2);
        return avg;
    }
}

class ConstructorReference {
    ConstructorReference(double ... args) {
        int sum = 0;
        for (double arg : args) {
            sum += arg;
        }
        System.out.println("Sum of elements: " + sum);
    }
}

public class MethodReferenceDemo {

    public static void main(String[] args) {
        Printer print = StaticMethodReference::show; //Static Method Reference
        print.greet("Manoj");

        Items item = Integer::compare; //Parameter Method Reference
        System.out.println("Comparing elements 2 and 5: " + item.compareElements(2, 5));

        InstanceMethodReference classObject = new InstanceMethodReference();
        classObject.value1 = 3;
        classObject.value2 = 6;
        Average average = classObject::calculateAverage; //Instance Method Reference
        int result = average.averageOfList();
        System.out.println("Average of two numbers: " + result);

        Adder adder = ConstructorReference::new; //Constructor Reference
        adder.sum(1,2,3,4,5);
    }

}
