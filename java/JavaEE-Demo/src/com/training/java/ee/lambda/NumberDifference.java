/*
2) Write a program to print difference of two numbers using lambda expression
and the single method interface

----------WBS----------

Requirement:
```````````
To print the difference of two numbers.

Entity:
``````
-NumberDifference

Interface:
`````````
-Subractor

Function Declaration:
````````````````````
-public static void main(String[] args)
-public int Difference(int number1, int number2)

Jobs to be done:
```````````````
1) Create a Subtractor interface with a abstract method difference
2) The definition of Interface method is given to a variable subtract
3) Invoking the method by the variable subtract and giving two numbers
4) Display the difference of two numbers.

Pseudo Code:
```````````
interface Subtractor {
    public int difference(int number1, int number2);
}

public class NumberDifference {
    public static void main(String[] args) {
        Subtractor subtract = (number1, number2) -> number1 - number2;
        int value = subtract.difference(10, 5);
        System.out.print(value);
    }
}

*/
package com.training.java.ee.lambda;

interface Subractor {
    public int difference(int number1, int number2);
}

public class NumberDifference {

    public static void main(String[] args) {
        Subractor subract = (num1, num2) -> num1 - num2;
        int number1 = 5, number2 = 10;
        int value = subract.difference(number1, number2);
         System.out.println(
             String.format(
                 "Difference of %d and %d is %d",
                 number1,
                 number2,
                 value)
         );
    }

}
