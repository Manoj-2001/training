/*
9)Difference between throws and throw , Explain with simple example.
*/
package com.training.java.ee.exception;

public class ThrowAndThrows {

    //throws can be used to tell the method may throw the exception and it is
    //used to suppress the compile time error occurred by the checked exceptions.
    public static void delay(int secs) throws InterruptedException {
        Thread.sleep(1000 * secs);
        System.out.println(secs + " second(s) delayed");
    }

    public static void main(String[] args) throws NullPointerException, InterruptedException {
        String string = "Manoj";
        int size = 0;
        try {
            size = string.length();
            System.out.println("String size: " + size);
            //throw used to explicitly throw an exception, new keyword is used 
            //because creating a memory for object of exception.
            throw new NullPointerException("Explicitly thrown");
        }
        catch (NullPointerException nullPointerException){
            ThrowAndThrows.delay(size);
            System.out.println("String should not be null");
            System.out.println(nullPointerException.getMessage());
        }
    }

}
