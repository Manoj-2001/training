/*
2)Write a program ListOfNumbers (using try and catch block).

----------WBS----------

Requirement:
```````````
Program to catch exception in list of numbers.

Entity:
``````
-TryCatchDemo

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a list of some random elements of different types
2) For each element in myList list
    2.1) Try to change the type of element to number
    2.2) Display the converted element
3) If interruption occurs handle the exception using catch block

Pseudo Code:
```````````
public class TryCatchDemo {

    public static void main(String[] args) {
        List myList = new ArrayList();
        myList.add(1);
        myList.add(2);
        myList.add('3');
        myList.add(4);
        myList.add(5);
        Iterator iterator = myList.iterator();
        while (iterator.hasNext()) {
            try {
                int value = iterator.next();
                System.out.println(value);
            }
            catch (Exception e) {
                System.out.println("Value not an Integer");
            }
        }
    }

}
*/
package com.training.java.ee.exception;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TryCatchDemo {

    public static void main(String[] args) {
        List myList = new ArrayList();
        myList.add(1);
        myList.add(2);
        myList.add('3');
        myList.add(4);
        myList.add(5);
        Iterator iterator = myList.iterator();
        while (iterator.hasNext()) {
            try {
                System.out.println((int) iterator.next());
            }
            catch (Exception e) {
                System.out.println("Value not an Integer");
            }
        }
    }

}
