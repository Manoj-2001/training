/*
1)Demonstrate the catching multiple exception with example.

----------WBS----------

Requirement:
```````````
To catch multiple exceptions.

Entity:
``````
-TryCatchDemo

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an array and store some random numbers to array
2) For each number
    2.1) Divide the number by next number in list
3) If any interruption occurs it get handled by catch block

Pseudo Code:
```````````
public class MultipleCatchDemo {

    public static void main(String[] args) {
        int[] array = {5, 2, 3, 1, 0, 9, 2, 7, 0, 4};
        for (int i = 0; i < array.length; i++) {
            try {
                System.out.println((array[i] / array[i + 1]));
            }
            catch (ArithmeticException ae) {
                System.out.println("Cannot divide by zero.");
            }
            catch (ArrayIndexOutOfBoundsException arrexception) {
                System.out.println("Array went out of its bound.");
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}

*/
package com.training.java.ee.exception;

public class MultipleCatchDemo {

    public static void main(String[] args) {
        int[] array = {5, 2, 3, 1, 0, 9, 2, 7, 0, 4};
        for (int i = 0; i < array.length; i++) {
            try {
                //accessing the current element and next element to it.
                System.out.println((array[i] / array[i + 1]));
            }
            catch (ArithmeticException ae) {
                System.out.println("Cannot divide by zero.");
            }
            catch (ArrayIndexOutOfBoundsException arrexception) {
                System.out.println("Array went out of its bound.");
            }
            catch (Exception e) {
                System.out.println("Exception occured: " + e.getMessage());
            }
        }
    }

}
