/*
6)Handle and give the reason for the exception in the following code: 
PROGRAM:
public class Exception {  
    public static void main(String[] args) {
        int arr[] ={1,2,3,4,5};
        System.out.println(arr[7]);
    }
}

*Display the output.

*/

package com.training.java.ee.exception;

public class OutOfBound {

    public static void main(String[] args) {
        int arr[] ={1,2,3,4,5};
        try {
            System.out.println(arr[7]);
            // Here accessing the index which is not present there. So it
            // will throw an exception.
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Accessing the array out of its boundry.");
        }
    }

}
