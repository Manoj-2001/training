/*
5)
i)Multiple catch block -  
       a]Explain with Example
       b]It is possible to have more than one try block? - Reason.
 ii)Difference between catching multiple exceptions and Multiple catch blocks.

*/
package com.training.java.ee.exception;

public class HandlingExceptions {

    public static void main(String[] args) {
        int numbers[] = {1,2,3,4,5};
        try {
            int number = numbers[3];
            System.out.println(number / 0);
        }
        catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBound) {
            System.out.println("Accessing the array out of its boundry.");
        }
        catch (ArithmeticException arithmeticException) {
            System.out.println("Cannot divide by zero.");
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

}
/*
(i)b]It is possible to have more than one try block? - Reason.

Answer:
``````
No, we cannot use multiple try blocks for a finally or catch block.
but we can have nested try catch block or separate try catch or finally blocks.

(ii)Difference between catching multiple exceptions and Multiple catch blocks.

Answer:
``````
catching multiple exceptions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If a catch block handles multiple exceptions, you can separate them using
a pipe (|)
e.g: catch (NullPointerException | ArithmeticException e)

Multiple catch blocks
~~~~~~~~~~~~~~~~~~~~~
Making more catch blocks for handling the exceptions differently.
e.g:
catch (ArithmeticException arithmeticException) {
    //handling differently
}
catch (NullPointerException nullPointerException) {
    //handling differently
}

*/

