package com.training.java.ee.exception;

/*
8)Explain the below program.

try{
    dao.readPerson();
}
catch (SQLException sqlException) {
    throw new MyException("wrong", sqlException);
}

-----------------

Answer
``````
If exception thrown by try block,
the catch block gets the exception and throwing to the constructor of the 
user defined Exception "MyException".

-----------------
*/
