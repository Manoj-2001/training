/*
3)Compare the checked and unchecked exception.

*/
package com.training.java.ee.exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CheckedAndUncheckedDemo {

    public static void main(String[] args) throws InterruptedException{
        Scanner scanner = new Scanner(System.in);
        boolean pass = false;
        do {
            try {
                System.out.print("Enter the first integer: ");
                int value1 = scanner.nextInt();
                System.out.print("Enter the second integer: ");
                int value2 = scanner.nextInt();
                System.out.println("Result: " + (value1 / value2));
                pass = true;
            }
            // Unchecked exceptions
            catch (ArithmeticException ae) {
                System.out.println("Cannot divide a number by zero.");
            }
            catch (InputMismatchException ime) {
                System.out.println("Only numbers accepted.");
                break;
            }
        } while (!pass);

        System.out.println("Start...");

        // This is a checked exception.
        // It should be handled or it show exception at the compile time.
        // It either suppressed or handled.
        Thread.sleep(5000);

        System.out.println("End.");

    }

}
