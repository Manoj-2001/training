/*

1) What are the arguments passed for the sleep() method?

*/

package com.training.java.ee.threading;

/*
Answer:
``````
Thread.sleep(long millis, int nanos)

 There are two overloading sleep methods one accept milli seconds and 
another accept milli second and nano second.
 For more accurate time scheduling developers use method of millis and nanos

*/