/*
3) Explain in words Concurrency and Parallelism( in two sentences).
*/

package com.training.java.ee.threading;

/*
Answer:
``````
Concurrency:
```````````
Concurrency is the ability to run several programs or several parts 
of a program in parallel. If a time consuming task can be 
performed asynchronously or in parallel, this improves the throughput
and the interactivity of the program.

Parallelism:
```````````
Parallelism is about doing lots of things at once.

An application can be concurrent � but not parallel, which means
that it processes more than one task at the same time, but no two tasks
are executing at the same time instant.
*/