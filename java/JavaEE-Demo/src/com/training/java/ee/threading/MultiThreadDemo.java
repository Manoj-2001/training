/*

2) Write a program which implement multithreading using sleep(),
setPriority(), getPriorty(), Name(), getId() methods.

----------WBS----------

Requirement:
```````````
Write a program which implement multithreading using sleep(),
setPriority(), getPriorty(), Name(), getId() methods.

Entity:
``````
-MultiThreadDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create two classes Star and Circle that has a method
that overrides run method in Thread.
2) Star run method displays '*' sign one by one for ten times.
3) Circle run method displays 'o' sign one by one for ten times.
4) Give priority to star and circle threads.
5) Give name to star and circle threads.
6) Make star and circle threads to sleep for some time while working.
7) Get the id of star and circle threads.
8) Invoke the start method to run the threads.

Pseudo Code:
```````````
class Star extends Thread {
    public void run() {
        int count = 10;
        while (count-- > 0) {
            System.out.println("*");
            Thread.sleep(10);
        }
    }
}

class Circle extends Thread {
    public void run() {
        int count = 10;
        while (count-- > 0) {
            System.out.println("o");
            Thread.sleep(20);
        }
    }
}

public class MultiThreadDemo {

    public static void main(String[] args) {
        Thread star = new Star();
        Thread circle = new Circle();

        star.setPriority(Thread.MIN_PRIORITY);
        circle.setPriority(Thread.MAX_PRIORITY);

        star.setName("Star Thread");
        circle.setName("Circle Thread");

        star.start();
        circle.start();

        System.out.println(star.getId());
        System.out.println(circle.getId());
    }

}


*/
package com.training.java.ee.threading;

class Star extends Thread {
    public void run() {
        int count = 10;
        while (count-- > 0) {
            System.out.println("*");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Circle extends Thread {
    public void run() {
        int count = 10;
        while (count-- > 0) {
            System.out.println("o");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}

public class MultiThreadDemo {

    public static void main(String[] args) {
        Thread star = new Star();
        Thread circle = new Circle();
        // Creating objects of classes

        star.setPriority(Thread.MIN_PRIORITY);
        circle.setPriority(Thread.MAX_PRIORITY);
        // setting the thread priority

        star.setName("Star Thread");
        circle.setName("Circle Thread");
        // setting thread name

        star.start();
        circle.start();
        // starting the thread

        System.out.println("Id of Star Thread " + star.getId());
        System.out.println("Id of Circle Thread " + circle.getId());
        // getting the id of star and circle threads
    }

}
