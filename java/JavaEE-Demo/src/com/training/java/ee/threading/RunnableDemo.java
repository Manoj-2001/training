/*

2) Write a program which implement multithreading using sleep(),
setPriority(), getPriorty(), Name(), getId() methods.

----------WBS----------

Requirement:
```````````
Write a program which implement multithreading using sleep(),
setPriority(), getPriorty(), Name(), getId() methods.

Entity:
``````
-MultiThreadDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create two classes Even and Odd that implements Runnable interface.
2) Even class run method displays only even numbers in the range of 20.
3) Odd class run method displays only odd numbers in the range of 20.
4) Creating even and odd objects of Even and Odd classes.
5) Creating Threads by passing objects into constructor.
6) Invoking the start method of Threads.

Pseudo Code:
```````````
class Even implements Runnable throws Exception {

    public void run() {
        for (int number = 1; number <= 20; number++) {
            if (number % 2 == 0) {
                System.out.println("Even number: " + number);
            }
            Thread.sleep(500);
        }
    }

}

class Odd implements Runnable throws Exception {

    public void run() {
        for (int number = 1; number <= 20; number++) {
            if (number % 2 != 0) {
                System.out.println("Odd number: " + number);
            }
            Thread.sleep(500);
        }
    }

}

public class RunnableDemo {
    public static void main(String[] args) {
        Even even = new Even();
        Odd odd = new Odd();

        // Thread(object, thread_name)
        Thread t1 = new Thread(even, "Even Thread");
        Thread t2 = new Thread(odd, "Odd Thread");

        t1.start();
        t2.start();
    }
}

*/

package com.training.java.ee.threading;

class Even implements Runnable {

    public void run() { // defines the run method in Runnable interface
        for (int number = 1; number <= 20; number++) {
            if (number % 2 == 0) {
                System.out.println("Even number: " + number);
            }

            try {
                Thread.sleep(500);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            } // checked exception
        }
    }

}

class Odd implements Runnable {

    public void run() { // defines the run method in Runnable interface
        for (int number = 1; number <= 20; number++) {
            if (number % 2 != 0) {
                System.out.println("Odd number: " + number);
            }

            try {
                Thread.sleep(500);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            } // checked exception
        }
    }

}

public class RunnableDemo {
    public static void main(String[] args) {
        Even even = new Even();
        Odd odd = new Odd();

        // Thread(object, thread_name)
        Thread t1 = new Thread(even, "Even Thread");
        Thread t2 = new Thread(odd, "Odd Thread");

        t1.start();
        t2.start();
    }
}
