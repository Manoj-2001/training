/*
1) Write a program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.

----------WBS----------

Requirement:
```````````
Program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.

Entity:
``````
-PropertiesDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a object properties of Properties class
2) Set some properties to properties object
3) Display all property using Iterator object with keySet
4) Stream to display all property in entrySet

Pseudo Code:
```````````
public class PropertiesDemo {

    public static void main(String[] args) {

        Properties properties = new Properties();

        properties.setProperty("user-name", "Manoj");
        properties.setProperty("email", "manoj@example.com");
        properties.setProperty("password", "04321");

        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        properties.entrySet().stream().forEach(System.out::println);
    }

}

*/

package com.training.java.ee.properties;

import java.util.Iterator;
import java.util.Properties;

public class PropertiesDemo {

    public static void main(String[] args) {

        // Creating an object of Properties class
        Properties properties = new Properties();

        properties.setProperty("user-name", "Manoj");
        properties.setProperty("email", "manoj@example.com");
        properties.setProperty("password", "04321");

        // Using Iterator to display key and values on properties
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        // Using entrySet method of HashTable to get the key value pair
        // using streams forEach to display key and values on properties
        properties.entrySet().stream().forEach(System.out::println);
    }

}
