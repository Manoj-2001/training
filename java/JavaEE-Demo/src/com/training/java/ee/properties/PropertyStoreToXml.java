/*
2) Write a program to add 5 elements to a xml file and print the elements
in the xml file using list. Remove the 3rd element from the xml file
and print the xml file.

----------WBS----------

Requirement:
```````````
2) Write a program to add 5 elements to a xml file and print the elements
in the xml file using list. Remove the 3rd element from the xml file
and print the xml file.

Entity:
``````
-PropertyStoreToXml

Method Signature:
````````````````
-public static List<String> toList(Set<Object> set
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a object properties of Properties class
2) Set some properties to properties object
3) Create a new XML file
4) Storing properties to XML file
5) Create a new object newProperties of Properties class
6) Loading data from XML file
7) Remove a specified property from properties
8) Display all the properties in newProperties object

Pseudo Code:
```````````
public class PropertyStoreToXml {

    public static List toList(Set set) {
        List list = new ArrayList();
        for (Object element : set) {
            list.add(element);
        } return list;
    }

    public static void main(String[] args) throws IOException {

        Properties properties = new Properties();
        properties.setProperty(key, value);
        properties.setProperty(key, value);
        properties.setProperty(key, value);

        // create a XML file
        FileOutputStream newFile = new FileOutputStream("src/com/training/java/ee/properties/Info.xml");

        // store properties to xml file
        properties.storeToXML(newFile, "Information about the project path");

        // import a XML file
        FileInputStream existingFile = new FileInputStream("src/com/training/java/ee/properties/Info.xml");

        Properties newProperties = new Properties();

        newProperties.loadFromXML(existingFile);

        List<String> keysList = toList(newProperties.keySet());

        keysList.remove(key);
    }

}
*/

package com.training.java.ee.properties;

import java.util.List;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

public class PropertyStoreToXml {

    // toList method to convert Set elements to list
    public static List<String> toList(Set<Object> set) {
        List<String> list = new ArrayList<>();
        for (Object element : set) {
            list.add((String) element);
        } return list;
    }

    public static void main(String[] args) throws IOException {

        // setting to some properties to properties object of Properties
        Properties properties = new Properties();
        properties.setProperty("path", "Demo/src/");
        properties.setProperty("project", "Demo");
        properties.setProperty("DBpass", "1234");
        properties.setProperty("resource", "https://example.com");
        properties.setProperty("resource author", "Jenkov");

        // create a XML file
        FileOutputStream newFile = new FileOutputStream("src/com/training/java/ee/properties/Info.xml");

        // store properties to xml file
        properties.storeToXML(newFile, "Information about the project path");

        // import a XML file
        FileInputStream existingFile = new FileInputStream("src/com/training/java/ee/properties/Info.xml");

        // creating a newProperties object of Property class
        Properties newProperties = new Properties();

        // loading properties from XML file to newProperties object
        newProperties.loadFromXML(existingFile);

        // converting set to list
        List<String> keysList = toList(newProperties.keySet());

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }

        // removing a key from keysList
        keysList.remove("DBpass");

        System.out.println();

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }
    }

}
