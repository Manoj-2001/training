/*
1)Write a code to change the number format to Denmark number format .

----------WBS----------

Requirement:
```````````
Write a code to change the number format to Denmark number format

Entity:
``````
-DenmarkNumberFormat

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an object locale of Locale class by specifying the language and region
of the country.
2) Create a number formatter and set the locale
3) format the number using formatter

Pseudo Code:
```````````
public class DenmarkNumberFormat {

    public static void main(String[] args) {
        Locale locale = new Locale("DK");
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        System.out.println(numberFormat.format(10.990));
    }

}

*/

package com.training.java.ee.internationalization;

import java.text.NumberFormat;
import java.util.Locale;

public class DenmarkNumberFormat {

    public static void main(String[] args) {
        // creating a locale of language and country code
        Locale locale = new Locale("da", "DK");
        // formating the number based on locale
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        System.out.println("Denmark Number Format: " + numberFormat.format(10.990));
    }

}
