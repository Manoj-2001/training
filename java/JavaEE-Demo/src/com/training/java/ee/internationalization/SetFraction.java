/*
2) Write a code to rounding the value to maximum of 3 digits by setting
maximum and minimum Fraction digits.

----------WBS----------

Requirement:
```````````
Write a code to rounding the value to maximum of 3 digits by setting
maximum and minimum Fraction digits.

Entity:
``````
-SetFraction

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an object numberFormat by invoking getInstance method of NumberFormat
2) Set minimum and maximum fraction digits on numberFormat
3) formatting the number using numberFormat.

Pseudo Code:
```````````
public class SetFraction {

    public static void main(String[] args) {
        NumberFormat numberFormat = NumberFormat.getInstance();

        numberFormat.setMinimumFractionDigits(1);
        numberFormat.setMaximumFractionDigits(3);

        System.out.println(numberFormat.format(453.84732));
    }

}

*/

package com.training.java.ee.internationalization;

import java.text.NumberFormat;

public class SetFraction {

    public static void main(String[] args) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        // setting minimum and maximum for fractions
        numberFormat.setMinimumFractionDigits(1);
        numberFormat.setMaximumFractionDigits(3);
        System.out.println(numberFormat.format(453.84732));
        System.out.println(numberFormat.format(11));
    }

}
