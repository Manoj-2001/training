/*
4) To print the following pattern of the Date and Time using SimpleDateFormat.
    "yyyy-MM-dd HH:mm:ssZ"

----------WBS----------

Requirement:
```````````
To print the following pattern of the Date and Time using SimpleDateFormat.
    "yyyy-MM-dd HH:mm:ssZ"

Entity:
``````
-SimpleDateFormatDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a pattern to format the date
2) Create a object formatter of SimpleDateFormat
3) Format the date using formatter.

Pseudo Code:
```````````
public class SimpleDateFormatDemo {

    public static void main(String[] args) {
        String pattern = "yyyy-MM-dd HH:mm:ss Z";

        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        String date = formatter.format(new Date());
        System.out.println(date);
    }

}

*/

package com.training.java.ee.internationalization;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateFormatDemo {

    public static void main(String[] args) {
        String pattern = "yyyy-MM-dd HH:mm:ss Z";
        // setting the pattern to formatter
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        // formatting the date object
        String date = formatter.format(new Date());
        System.out.println(date);
    }

}
