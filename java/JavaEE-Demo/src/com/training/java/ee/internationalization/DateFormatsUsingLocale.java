/*
3) To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL.

----------WBS----------

Requirement:
```````````
To print the following Date Formats in Date using locale.
DEFAULT, MEDIUM, LONG, SHORT, FULL.

Entity:
``````
-DateFormatsUsingLocale

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an object of Date.
2) Create an object of locale using language and region.
3) Format the date using default, medium, long, short, full
4) Display the formatted dates.

Pseudo Code:
```````````
public class DateFormatsUsingLocale {

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("Current date: " + date);

        Locale locale = new Locale("en", "CA");
        String defaultTime = DateFormat
                .getTimeInstance(DateFormat.DEFAULT, locale).format(date);
        System.out.println("DateFormat DEFAULT: " + defaultTime);

        String mediumDate = DateFormat
                .getDateInstance(DateFormat.MEDIUM, locale).format(date);
        System.out.println("DateFormat MEDIUM: " + mediumDate);

        String longDate = DateFormat
                .getDateInstance(DateFormat.LONG, locale).format(date);
        System.out.println("DateFormat LONG: " + longDate);

        String shortDate = DateFormat
                .getDateInstance(DateFormat.SHORT, locale).format(date);
        System.out.println("DateFormat SHORT: " + shortDate);

        String fullTime = DateFormat
                .getTimeInstance(DateFormat.FULL, locale).format(date);
        System.out.println("DateFormat FULL: " + fullTime);
    }

}

*/

package com.training.java.ee.internationalization;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatsUsingLocale {

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("Current date: " + date);

        Locale locale = new Locale("en", "CA");
        String defaultTime = DateFormat
                .getTimeInstance(DateFormat.DEFAULT, locale).format(date);
        System.out.println("DateFormat DEFAULT: " + defaultTime);

        String mediumDate = DateFormat
                .getDateInstance(DateFormat.MEDIUM, locale).format(date);
        System.out.println("DateFormat MEDIUM: " + mediumDate);

        String longDate = DateFormat
                .getDateInstance(DateFormat.LONG, locale).format(date);
        System.out.println("DateFormat LONG: " + longDate);

        String shortDate = DateFormat
                .getDateInstance(DateFormat.SHORT, locale).format(date);
        System.out.println("DateFormat SHORT: " + shortDate);

        String fullTime = DateFormat
                .getTimeInstance(DateFormat.FULL, locale).format(date);
        System.out.println("DateFormat FULL: " + fullTime);
    }

}
