/*
1.How to read the file contents  line by line in streams with example

----------WBS----------

Requirement:
```````````
To read the file contents  line by line in streams

Entity:
``````
-ReadFileContentUsingStream

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file
    1.1) If no file exists on the specified path throw exception
2) Get stream of lines in file using lines method in Files class.
3) Display the lines one by one using Stream's forEach method.

Pseudo Code:
```````````
public class ReadFileContentUsingStream {

    public static void main(String[] args) {
        // file path
        String filePath = "#path";

        // try with resource
        // getting the stream of lines in file
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

*/

package com.training.java.ee.iostream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileContentUsingStream {

    public static void main(String[] args) {
        // file path
        String filePath = "src/com/training/java/ee/iostream/content.txt";

        // try with resource
        // getting the stream of lines in file
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(line -> System.out.println("> " + line));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
