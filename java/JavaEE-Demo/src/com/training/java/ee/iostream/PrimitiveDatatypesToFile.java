/*
6.program to write primitive datatypes to file using by dataoutputstream.

----------WBS----------

Requirement:
```````````
Program to write primitive datatypes to file using by dataoutputstream.

Entity:
``````
-PrimitiveDatatypesToFile

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source of file
    1.1) If file not exists throw exception
2) Create an object of DataOutputStream to store values of primitive types
3) Using writeUTF to write String type values
4) Close the file.

Pseudo Code:
```````````
public class PrimitiveDatatypesToFile {

    public static void main(String[] args) {
        try {
            String path = "file path";
            DataOutputStream dataOutStream = new DataOutputStream(
                    new FileOutputStream(path));

            dataOutStream.writeInt(2108);
            dataOutStream.writeUTF("Manoj");
            dataOutStream.writeChar('S');
            dataOutStream.writeDouble(165.65);
            dataOutStream.writeBoolean(true);

            dataOutStream.close();
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
*/

package com.training.java.ee.iostream;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrimitiveDatatypesToFile {

    public static void main(String[] args) {
        try {
            String path = "src/com/training/java/ee/iostream/content new.txt";
            DataOutputStream dataOutStream = new DataOutputStream(
                    new FileOutputStream(path));

            // Writing primitive data to the file
            dataOutStream.writeInt(2108);
            dataOutStream.writeUTF("Manoj");
            dataOutStream.writeChar('S');
            dataOutStream.writeDouble(165.65);
            dataOutStream.writeBoolean(true);

            dataOutStream.close();
            System.out.println("Primitive datatypes written to file successfully!");
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
