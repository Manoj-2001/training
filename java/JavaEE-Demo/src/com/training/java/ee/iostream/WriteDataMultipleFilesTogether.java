/*
2) write a program that allows to write data to multiple files 
together using bytearray outputstream. 

----------WBS----------

Requirement:
```````````
To write data to multiple files together using bytearray outputstream.

Entity:
``````
-WriteDataMultipleFilesTogether

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file
    1.1) If no file exists on the specified path throw exception.
2) Get the data to stored to multiple files.
3) Convert the data to array of bytes.
4) write the array of bytes to the object of ByteArrayOutputStream
5) write the data back to the files using writeTo method in ByteArrayOutputStream
6) Finally close the files properly.

Pseudo Code:
```````````
public class WriteDataMultipleFilesTogether {

    public static void main(String[] args) throws IOException {
        FileOutputStream file1 = null;
        FileOutputStream file2 = null;
        ByteArrayOutputStream byteArrOutputStream = null;

        try {
            file1 = new FileOutputStream(#path);
            file2 = new FileOutputStream(#path);
            byteArrOutputStream = new ByteArrayOutputStream();

            byte[] bytes = data.getBytes();
            byteArrOutputStream.write(bytes);

            byteArrOutputStream.writeTo(file1);
            byteArrOutputStream.writeTo(file2);

            System.out.println("Data written to both the files.");
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        } finally {
            // closing the files and byte array output stream
            file1.close();
            file2.close();
            byteArrOutputStream.close();
        }
    }

}

*/

package com.training.java.ee.iostream;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteDataMultipleFilesTogether {

    public static void main(String[] args) throws IOException {
        FileOutputStream file1 = null;
        FileOutputStream file2 = null;
        ByteArrayOutputStream byteArrOutputStream = null;

        try {
            file1 = new FileOutputStream("src/com/training/java/ee/iostream/file1.txt");
            file2 = new FileOutputStream("src/com/training/java/ee/iostream/file2.txt");
            byteArrOutputStream = new ByteArrayOutputStream();

            String data = "Hello!\nThis data gonna written to both the files";
            byte[] bytes = data.getBytes();
            byteArrOutputStream.write(bytes);

            byteArrOutputStream.writeTo(file1);
            byteArrOutputStream.writeTo(file2);

            System.out.println("Data written to both the files.");
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        } finally {
            // closing the files and byte array output stream
            file1.close();
            file2.close();
            byteArrOutputStream.close();
        }
    }

}
