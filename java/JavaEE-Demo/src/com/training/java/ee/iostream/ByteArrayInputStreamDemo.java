/*
4.write a program for ByteArrayInputStream class to read byte array as input stream.

----------WBS----------

Requirement:
```````````
ByteArrayInputStream class to read byte array as input stream.

Entity:
``````
-ByteArrayInputStreamDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Convert string data into byte array using getBytes method
2) Get stream of bytes using ByteArrayInputStream
3) Read the byte from stream of bytes
    3.1) If readed data returns -1, end of file reached
4) Convert the int to character and display it.

Pseudo Code:
```````````
public class ByteArrayInputStreamDemo {

    public static void main(String[] args) {
        String string = "Hello World!";
        byte[] bytes = string.getBytes();

        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);

        int data;
        while ((data = byteStream.read()) != -1) {
            System.out.println(data + " : " + (char) data);
        }
    }

}
*/

package com.training.java.ee.iostream;

import java.io.ByteArrayInputStream;

public class ByteArrayInputStreamDemo {

    public static void main(String[] args) {
        String string = "Hello World!";
        // converting the string data into byte array
        byte[] bytes = string.getBytes();

        // getting the stream of bytes in byte array using ByteArrayInputStream
        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);

        // reading the byte and converting into character
        int data;
        while ((data = byteStream.read()) != -1) {
            System.out.println(data + " : " + (char) data);
        }
    }

}
