/*
5.Java program to read a file using filter and buffer input streams.

----------WBS----------

Requirement:
```````````
Java program to write and read a file using filter and buffer input streams

Entity:
``````
-FilterInputStreamDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source of file
    1.1) If file not exists throw exception
2) Get the file data as stream using fileInputStream
3) Get the buffered data using FilterInputStream
4) Read data by data from buffered data
5) Convert the data into character and display the converted data

Pseudo Code:
```````````
public class FilterInputStreamDemo {
    public static void main(String[] args) throws IOException {
        String filePath = "path";
        FileInputStream streamFile = new FileInputStream(filePath);
        FilterInputStream streamFilter = new BufferedInputStream(streamFile);
        int data;
        while ((data = streamFilter.read()) != -1) {
            System.out.print((char) data);
        }
        streamFile.close();
        streamFilter.close();
    }
}

*/

package com.training.java.ee.iostream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;

public class FilterInputStreamDemo {
    public static void main(String[] args) throws IOException {
        // file path
        String filePath = "src/com/training/java/ee/iostream/content.txt";

        // getting file as stream
        FileInputStream streamFile = new FileInputStream(filePath);
        // getting buffered stream of data
        FilterInputStream streamFilter = new BufferedInputStream(streamFile);

        // reading the data and converting into character
        int data;
        while ((data = streamFilter.read()) != -1) {
            System.out.print((char) data);
        }
        streamFile.close();
        streamFilter.close();
    }
}
