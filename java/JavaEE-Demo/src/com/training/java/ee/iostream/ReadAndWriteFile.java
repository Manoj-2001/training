/*
3.write a Java program reads data from a particular file using FileReader
and writes it to another, using FileWriter.

----------WBS----------

Requirement:
```````````
Java program reads data from a particular file using FileReader
and writes it to another, using FileWriter.

Entity:
``````
-ReadAndWriteFile

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the file
    1.1) If no file exists on the specified path throw exception.
2) Read the characters in file using Reader
3) Write the characters to new file using Writer
4) Close the files.

Pseudo Code:
```````````
public class ReadAndWriteFile {

    public static void main(String[] args) {
        int character;

        String fileToRead = "file path";
        String fileToWrite = "file path";
        FileReader reader;
        FileWriter writer;
        try {
            reader = new FileReader(fileToRead);
            writer = new FileWriter(fileToWrite);

            while ((character = reader.read()) != -1) {
                writer.write((char) character);
            }
        }
        catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

}
*/

package com.training.java.ee.iostream;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadAndWriteFile {

    public static void main(String[] args) {
        int character;

        String fileToRead = "src/com/training/java/ee/iostream/content.txt";
        String fileToWrite = "src/com/training/java/ee/iostream/content new.txt";
        FileReader reader;
        FileWriter writer;
        try {
            reader = new FileReader(fileToRead);
            writer = new FileWriter(fileToWrite);

            while ((character = reader.read()) != -1) {
                writer.write((char) character);
            }
        }
        catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        System.out.println("File copied");
    }

}
