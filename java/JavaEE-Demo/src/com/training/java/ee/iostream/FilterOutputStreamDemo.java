/*
5.Java program to write a file using filter and buffer output streams.

----------WBS----------

Requirement:
```````````
Java program to write and read a file using filter and buffer input streams

Entity:
``````
-FilterOutputStreamDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source of file
    1.1) If file not exists throw exception
2) Create an object of BufferedOutputStream to store buffered sream of data
3) Convert the data to be stored into bytes
4) Write into file using BufferedOutputStream

Pseudo Code:
```````````
public class FilterOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        String filePath = "path";
        FilterOutputStream streamFilter = new BufferedOutputStream(new FileOutputStream(filePath));

        byte[] byteArray = "\nJava Programming IO Demo".getBytes();

        for (byte aByte : byteArray) {
            streamFilter.write(aByte);
        }

        streamFilter.close();
    }
}
*/

package com.training.java.ee.iostream;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

public class FilterOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        String filePath = "src/com/training/java/ee/iostream/content.txt";
        // creating a filter output stream to store as bytes 
        FilterOutputStream streamFilter = new BufferedOutputStream(new FileOutputStream(filePath));

        // converting into bytes
        byte[] byteArray = "\nJava Programming IO Demo".getBytes();

        // filter output stream converts the byte and write into file
        for (byte aByte : byteArray) {
            streamFilter.write(aByte);
        }

        streamFilter.close();
        System.out.println("written successfully.");
    }
}