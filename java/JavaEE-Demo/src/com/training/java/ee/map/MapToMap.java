/*
1) Write a Java program to copy all of the mappings from the specified map to
another map?

----------WBS----------

Requirement:
```````````
To write a java program to copy entities of one map to another map.

Entity:
``````
-MapToMap

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a studentsList map and add some students name with
their student id.
2) Create an another studentsFinalList map and add all
the students of studentsList to studentsFinalList map.

Pseudo Code:
```````````
public class MapToMap {

    public static void main(String[] args) {
        Map<Integer, String> studentsList = new HashMap<>();
        tudentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        Map<Integer, String> studentsListFinal = new HashMap<>();
        studentsListFinal.putAll(studentsList);
    }

}

*/
package com.training.java.ee.map;

import java.util.HashMap;
import java.util.Map;

public class MapToMap {

    public static void main(String[] args) {
        Map<Integer, String> studentsList = new HashMap<>();
        Map<Integer, String> studentsListFinal = new HashMap<>();
        studentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        studentsListFinal.putAll(studentsList);
        System.out.println(studentsListFinal);
    }

}
