/*
2) Write a Java program to test if a map contains a mapping for the specified key?

----------WBS----------

Requirement:
```````````
To write a java program to check the key is present in the map.

Entity:
``````
-CheckKey

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a studentsList map and add some students with roll number
2) Check whether the student id is present in map.

Pseudo Code:
```````````
public class CheckKey {

    public static void main(String[] args) {
        Map<Integer, String> studentsList = new HashMap<>();
        tudentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        studentsListFinal.putAll(studentsList.containsKey(16101));
    }

}

*/
package com.training.java.ee.map;

import java.util.HashMap;
import java.util.Map;

public class CheckKey {

    public static void main(String[] args) {
        Map<Integer, String> studentsList = new HashMap<>();
        studentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        System.out.println(studentsList);
        System.out.println("Checking key '16110' is present in map: " +
        studentsList.containsKey(16110));
        System.out.println("Checking key '16101' is present in map: " +
        studentsList.containsKey(16101));
    }

}
