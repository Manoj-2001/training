/*
4) Write a Java program to get the portion of a map whose keys range from a
given key to another key?

----------WBS----------

Requirement:
```````````
To write a java program to get the portion of a map.

Entity:
``````
-PortionOfMap

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a studentsList map and add some students with roll number
2) Get the portion of entities in studentList map and store it in
newList map.

Pseudo Code:
```````````
public class PortionOfMap {

    public static void main(String[] args) {
        TreeMap<Integer, String> studentsList = new TreeMap<>();

        studentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        studentsList.put(16108, "Anand");

        Map<Integer, String> newList = studentsList.subMap(16105, 16110);
        System.out.println(newList);
    }

}

*/
package com.training.java.ee.map;

import java.util.Map;
import java.util.TreeMap;

public class PortionOfMap {

    public static void main(String[] args) {
        TreeMap<Integer, String> studentsList = new TreeMap<>();

        studentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        studentsList.put(16108, "Anand");
        studentsList.put(16111, "Gopal");
        studentsList.put(16113, "Krishnan");
        studentsList.put(16120, "Hari");

        Map<Integer, String> newList = studentsList.subMap(16105, 16110);
        System.out.println(newList);
    }

}
