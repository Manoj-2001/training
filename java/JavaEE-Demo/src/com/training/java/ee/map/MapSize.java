/*
3) Count the size of mappings in a map?

----------WBS----------

Requirement:
```````````
To write a java program to count the size of the mappings in map.

Entity:
``````
-MapSize

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a studentsList map and add some students with roll number
2) Get the size of the map using size() method.

Pseudo Code:
```````````
public class MapSize {

    public static void main(String[] args) {
        Map<Integer, String> studentsList = new HashMap<>();
        studentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        System.out.println(studentsList.size());
    }

}
*/
package com.training.java.ee.map;

import java.util.HashMap;
import java.util.Map;

public class MapSize {

    public static void main(String[] args) {
        Map<Integer, String> studentsList = new HashMap<>();
        studentsList.put(16101, "Mani");
        studentsList.put(16102, "Bala");
        studentsList.put(16103, "Balaji");
        studentsList.put(16104, "Priya");
        studentsList.put(16105, "Kumar");
        studentsList.put(16106, "Vinith");
        System.out.println("Map count: " + studentsList.size());
    }

}
