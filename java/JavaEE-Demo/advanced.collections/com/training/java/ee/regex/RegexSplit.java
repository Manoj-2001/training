/*
Problem statement:
`````````````````
Split any random text using any pattern you desire

----------WBS----------

Requirement:
```````````
Split any random text using any pattern you desire

Entity:
``````
-RegexSplit

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the input from user.
2) Create a pattern that
    2.1) Check whether the number plate has state and district code
    2.3) Check whether the number plate has license number
3) Checks whether the input matches the pattern.
    3.1) If the input matches split the details and store it in plateDetails array
         and display the plateDetails
    3.2) If the input doesn't match display "Wrong plate number"

Pseudo Code:
```````````
public class RegexSplit {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String numberPlate = scanner.nextLine();

        Pattern pattern.compile("[A-Z]{2}\\s[0-9]{2}\\s[A-Z]{2}\\s[0-9]{4}");
        Matcher matcher = pattern.matcher(numberPlate);

        if(matcher.find()) {
            System.out.print(numberPlate.split(" "));
        } else {
            System.out.print("Wrong number plate");
        }
    }
}
*/

package com.training.java.ee.regex;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexSplit {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Plate no. ");
        String numberPlate = scanner.nextLine();

        Pattern pattern = Pattern.compile("[A-Z]{2}\\s[0-9]{2}\\s[A-Z]{2}\\s[0-9]{4}");
        Matcher matcher = pattern.matcher(numberPlate);
        if (matcher.find()) {
            String[] plateDetails = numberPlate.split(" ");
            System.out.println(Arrays.toString(plateDetails));
        }
        else {
            System.out.print("Wrong plate number");
        }
    }

}
