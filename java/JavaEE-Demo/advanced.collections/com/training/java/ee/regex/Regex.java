package com.training.java.ee.regex;

/*
 * Demonstrate establish the difference between lookingAt() and matches()
 * ``````````````````````````````````````````````````````````````````````
 * lookingAt() method tries to match the input sequence, starting at 
 * the beginning
 * The matches() method returns true If the regular expression matches 
 * the whole text. If not, the matches() method returns false.
 * 
 * the different types of groups in regex
 * ``````````````````````````````````````
 * 1.Capturing group
 * 2.Non-capturing group
 * 3.Backreference
 * 4.Relative Backreference
 * 5.Failed backreference
 * 6.Invalid backreference
 * 7.Nested backreference
 * 8.Forward reference
 * 
 * the fields of the pattern class
 * ```````````````````````````````
 * The Pattern class defines an alternate compile method that accepts 
 * a set of flags affecting the way the pattern is matched.
 * 
 * -- Pattern.CANON_EQ Enables canonical equivalence.
 * -- Pattern.CASE_INSENSITIVE Enables case-insensitive matching.
 * -- Pattern.COMMENTS Permits whitespace and comments in the pattern.
 * -- Pattern.DOTALL Enables dotall
 * -- Pattern.LITERAL Enables literal parsing of the pattern.
 * -- Pattern.MULTILINE Enables multiline mode.
 * -- Pattern.UNICODE_CASE Enables Unicode-aware case folding.
 * -- Pattern.UNIX_LINES Enables UNIX lines mode.
 * 
 * Difference between replaceAll() and appendReplacement()
 * ```````````````````````````````````````````````````````
 * replaceAll() method replaces all the occurrences of existing string
 * with the new string
 * 
 * appendReplacement(StringBuffer sb, String replacement) method of Matcher class
 *  behaves as a append-and-replace method. This method reads the input string 
 *  and replace it with the matched pattern in the matcher string.
 */