/*
Problem statement:
`````````````````
1. create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit.

----------WBS----------

Requirement:
```````````
create a pattern for password which contains 8 to 15 characters in length
 --> Must have at least one upper case letter
 --> Must have at least one lower case letter
 --> Must have at least one digit

Entity:
``````
-RegexPassword

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the input from user.
2) Check the input has any null or space or any special characters
    2.1)If, Throw exception that input should not contain null or any special 
    characters.
3) Create a pattern that
    3.1) checks whether the password length lies around 8 to 15 characters.
    3.2) checks the password has at least one upper case character.
    3.2) checks the password has at least one lower case character.
    3.3) checks the password has at least one number(digit).
4) Checks whether the input matches the pattern.
    4.1) If the input matches print "Valid Password"
    4.2) If the input doesn't match print "Invalid Password"

Pseudo Code:
```````````
public class RegexPassword {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        // Get input from user using scanner

        if (password == null || password.contains(" ") || 
        password has special characters) {
            throw new Exception("Input should not contain null or any 
            special characters");
        }

        Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,15}$");
        Matcher matcher = pattern.matcher(password);

        if(matcher.find()) {
            System.out.println("Valid Password");
        } else {
            System.out.println("Invalid Password");
        }
    }
}
*/

package com.training.java.ee.regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class RegexPassword {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password: ");
        String password = scanner.nextLine();

        try {
            if (password.equals("") || password.contains(" ")) {
                throw new Exception("Input should not contain null or space");
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        if (Pattern
                .compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,15}$")
                .matcher(password).find()) {
            System.out.println("Valid Password");
        }
        else {
            System.out.println("Invalid Password");
        }
    }

}
