/*
Problem statement:
`````````````````
2.what is the difference between poll() and remove() method of queue interface?

----------WBS----------

Requirement:
```````````
What is the difference between poll() and remove() method of queue interface?

Entity:
``````
-RemoveVsPoll

Method signature:
````````````````
public static void main(String[] args)

/*
Answer:
``````
If trying to pick element from empty queue using poll method returns 'null'
If remove() method is used it will throw an NoSuchElementException.

we can simply use remove method for the specified or known size of queue.
poll() method is useful when the size of the queue is not judgeable.
when different thread accessing same queue, makes hard to judge.
*/

// Sample code
package com.training.java.ee.collections;

import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Queue;

public class RemoveVsPoll {

    public static void main(String[] args) {
        Queue<String> queue = new PriorityQueue<>();
        queue.add("Manoj");

        System.out.println(queue.remove());
        System.out.println(queue.poll());

        try {
            System.out.println(queue.remove());
        } catch (NoSuchElementException e) {
            System.out.println(e);
        }
        // remove method throws exception so it handled using catch block.
    }

}
