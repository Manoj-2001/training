/*
Problem statement:
`````````````````
1) create an array list with 7 elements, and create an empty linked list add
all elements of the array list to linked list ,traverse the elements and
display the result.

----------WBS----------

Requirement:
```````````
Create an array list with 7 elements, and create an empty linked list add
all elements of the array list to linked list ,traverse the elements and
display the result.

Entity:
``````
-ArrayListToLinkedList

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a languages array list and add 7 languages to it.
2) Create a empty linked list and add all the languages in
languages array list to linked list.
3) For each language in linked list
    3.1) display the name of the language

Pseudo Code:
```````````
public class ArrayListToLinkedList {
    public static void main(String[] args) {

        ArrayList<String> languages = new ArrayList<String>();

        languages.add("Python");
        // Adding 7 elements to array

        LinkedList<String> linkedList = new LinkedList<>();
        // Creating an empty linked list

        // Adding all elements of array list to linked list

        for (String language : linkedList) {
            System.out.println(language);
        }
        // Traversing all elements in linked list
    }
}
*/

package com.training.java.ee.collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayListToLinkedList {

    public static void main(String[] args) {

        ArrayList<String> languages = new ArrayList<>();
        languages.add("Java");
        languages.add("Python");
        languages.add("C");
        languages.add("C++");
        languages.add("JS");
        languages.add("Kotlin");
        languages.add("Ruby");

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.addAll(languages);
        // adding all elements of array list

        for (String language : linkedList) {
            System.out.println(language);
        }
        // traversing and displaying the elements
    }

}
