/*
Problem statement:
`````````````````
5.add and remove the elements in stack

----------WBS----------

Requirement:
```````````
Add and remove the elements in stack.

Entity:
``````
-StackDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Add some special characters to specialCharacters stack.
2) Remove a character from specialCharacters stack.

Pseudo Code:
```````````
public class StackDemo {
    public static void main(String[] args) {

        Stack<Character> specialCharacters = new Stack<>();
        // creating a stack

        stack.push('!');
        stack.push('@');
        stack.push('#');
        // add element to stack

        char removedCharacter = stack.pop();
        // remove element from stack
    }
}
*/

package com.training.java.ee.collections;

import java.util.Stack;

public class StackDemo {

    public static void main(String[] args) {

        Stack<Character> specialCharacters = new Stack<>();
        specialCharacters.push('!');
        specialCharacters.push('@');
        specialCharacters.push('#');
        specialCharacters.push('$');
        specialCharacters.push('%');
        System.out.println(specialCharacters);

        System.out.println("Removed element: " + specialCharacters.pop());

        System.out.println(specialCharacters);

    }

}
