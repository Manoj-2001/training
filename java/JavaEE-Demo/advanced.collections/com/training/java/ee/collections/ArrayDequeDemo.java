/*
Problem statement:
`````````````````
4.use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue.

----------WBS----------

Requirement:
```````````
Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue.

Entity:
``````
-ArrayDequeDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a laptops double ended queue and
add laptops to it.
2) Add a laptop name at first.
3) Add a laptop name at last.
4) Remove the laptop present at first in queue.
5) Remove the laptop present at last in queue.
6) Get the peek first laptop.
7) Get the peek last laptop.
8) Remove the first laptop in laptops dequeue using poll.
9) Remove the last laptop in laptops dequeue using poll.

Pseudo Code:
```````````
public class ArrayDequeDemo {
    public static void main(String[] args) {

        Deque<String> laptops = new Deque<>();
        // creating a dequeue

        laptops.add("Lenovo");
        laptops.add("Dell");
        // add some laptop to laptops dequeue

        laptops.addFirst("MacBook");
        // adding element at first

        laptops.addLast("HP");
        // adding element at last

        laptops.removeFirst();
        // removing element present at first

        laptops.removeLast();
        // removing element present at last

        laptops.peekFirst();
        // getting element present at first

        laptops.peekLast();
        // getting element present at last

        laptops.pollFirst();
        laptops.pollLast();
        // removing first and last element from dequeue
    }
}
*/

package com.training.java.ee.collections;

import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeDemo {

    public static void main(String[] args) {
        Deque<String> laptops = new ArrayDeque<>();
        laptops.add("Asus ROG Zephyrus G14");
        laptops.add("MacBook Air");
        laptops.addFirst("Dell XPS 13");
        // adding element at first
        laptops.add("MacBook Pro");
        laptops.add("HP Envy x360");
        laptops.addLast("Lenovo Chromebook Duet");
        // adding element at last
        laptops.add("HP Elite Dragonfly");

        System.out.println(laptops + "\n");

        laptops.removeFirst();
        laptops.removeLast();
        // removing the first and last element in dequeue

        System.out.println(laptops + "\n");

        System.out.println("Peek first element: " + laptops.peekFirst() + "\n");
        // getting element present at first
        System.out.println("Peek last element: " + laptops.peekLast() + "\n");
        // getting element present at last

        System.out.println("poll first: " + laptops.pollFirst() + "\n");
        System.out.println("poll last: " + laptops.pollLast() + "\n");
        // removing first and last element

        System.out.println(laptops);
    }

}
