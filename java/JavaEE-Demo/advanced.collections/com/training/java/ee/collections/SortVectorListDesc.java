/*
Problem statement:
`````````````````
3.code for sorting vector list in descending order.

----------WBS----------

Requirement:
```````````
Code for sorting vector list in descending order.

Entity:
``````
-SortVectorListDesc

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a games vector list
2) Add some games name to games list
3) Sort the list in the reverse order
4) Display the sorted list

Pseudo Code:
```````````
public class SortVectorListDesc {
    public static void main(String[] args) {

        Vector<String> games = new Vector<String>();
        // creating a vector list.

        games.add("PUBG");
        games.add("Free fire");
        // add element to vector list

        games.sort(Collections.reverseOrder());
        // Reversing the vector list
    }
}
*/

package com.training.java.ee.collections;

import java.util.Collections;
import java.util.Vector;

public class SortVectorListDesc {

    public static void main(String[] args) {
        Vector<String> games = new Vector<>();
        games.add("Assassin's Creed");
        games.add("GTA");
        games.add("Tom Clancy");
        games.add("NFS");
        games.add("PUBG");

        System.out.println(games);

        games.sort(Collections.reverseOrder());

        System.out.println(games);

    }

}
