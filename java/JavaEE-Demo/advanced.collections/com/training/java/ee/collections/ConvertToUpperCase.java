/*
Problem statement:
`````````````````
7) 8 districts are shown below
   Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy
   to be converted to UPPERCASE.

----------WBS----------

Requirement:
```````````
8 districts are shown below
Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy
to be converted to UPPERCASE.

Entity:
``````
-ConvertToUpperCase

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a districts list which has some districts.
2) Convert all the districts in the districts list into upper case.

Pseudo Code:
```````````
class MyUnaryOperator implements UnaryOperator {
    @Override
    public String apply(String string) {
        return string.toUpperCase();
    }
}
public class StudentsNameFilter {
    public static void main(String[] args) {

        List<String> districts = new List<>();
        // creating a list called districts

        districts.add("Madurai");
        districts.add("Coimbatore");
        districts.add("Theni");
        // adding some districts to districts list

        districts.replaceAll(new MyUnaryOperator());
    }
}
*/

package com.training.java.ee.collections;

import java.util.ArrayList;
import java.util.List;

public class ConvertToUpperCase {

    public static void main(String[] args) {
        List<String> districts = new ArrayList<>();
        districts.add("Madurai");
        districts.add("Coimbatore");
        districts.add("Theni");
        districts.add("Chennai");
        districts.add("Karur");
        districts.add("Salem");
        districts.add("Erode");
        districts.add("Trichy");
        System.out.println(districts);
        // adding districts to list

        districts.replaceAll(string -> string.toUpperCase());
        //replaceAll method needs unary operator as arguments for that 
        //it must implemented in a class, apply is a unimplemented method of interface
        //the definition is given in class is simply converting to upper case
        //replaceAll method pass every object of districts list to apply method of 
        //unary operator ... this can be simply written with lambda expression

        System.out.println(districts);
    }

}
