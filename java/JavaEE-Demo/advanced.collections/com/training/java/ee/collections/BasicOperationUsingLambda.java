/*
Problem statement:
`````````````````
8) Addition,Subtraction,Multiplication and Division concepts are achieved
using Lambda expression and functional interface.

----------WBS----------

Requirement:
```````````
Addition,Subtraction,Multiplication and Division concepts are achieved
using Lambda expression and functional interface.

Entity:
``````
-BasicOperationUsingLambda

Interface:
`````````
-Calculation

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Add two elements
2) Subtract two elements
3) Multiply two elements
4) Divide two elements

Pseudo Code:
```````````
interface Calculation<T> {
    public T calculate(Character sign, T value1, T value2);
}

public class BasicOperationUsingLambda {
    public static void main(String[] args) {
        Calculation<Integer> operation = (operator, val1, val2) -> {
            switch(operator) {
                case '*':
                    return val1 * val2;
                case '/':
                    return val1 / val2;
                case '+':
                    return val1 + val2;
                case '-':
                    return val1 - val2;
                default:
                    return;
            }
        };
        System.out.println(operation.calculate('*', 12, 10));
        System.out.println(operation.calculate('+', 12, 10));
    }
}
*/

package com.training.java.ee.collections;

interface Calculation<T> {
    public T calculate(Character sign, T value1, T value2);
}

public class BasicOperationUsingLambda {

    public static void main(String[] args) {
        Calculation<Integer> operation = (operator, val1, val2) -> {
            switch (operator) {
                case '*': 
                    return val1 * val2;
                case '/':
                    return val1 / val2;
                case '+':
                    return val1 + val2;
                case '-':
                    return val1 - val2;
                default:
                    return 0;
            }
        };
        // lambda expression which returns the resultant calculated result
        System.out.println("Addition: " + operation.calculate('+', 10, 2));
        System.out.println("Subtraction: " + operation.calculate('-', 10, 2));
        System.out.println("Multiplication: " + operation.calculate('*', 10, 2));
        System.out.println("Division: " + operation.calculate('/', 10, 2));
        System.out.println("Unknown operator: " + operation.calculate('#', 10, 2));
    }

}
