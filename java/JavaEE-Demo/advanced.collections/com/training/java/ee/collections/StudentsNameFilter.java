/*
Problem statement:
`````````````````
6.LIST CONTAINS 10 STUDENT NAMES
    krishnan, abishek, arun, vignesh, kiruthiga, murugan, adhithya,
balaji, vicky, priya and display only names starting with 'A'.

----------WBS----------

Requirement:
```````````
Display the names of students starting with 'A'.

Entity:
``````
-StudentsNameFilter

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Creating a list of names of students.
2) For each student in students list
    2.1) Check if the student name starts with 'A'
    2.2) Display the name of the student.

Pseudo Code:
```````````
public class StudentsNameFilter {
    public static void main(String[] args) {

        List<String> students = new List<>();
        // creating a list

        students.add("Krishnan");
        students.add("Abishek");
        students.add("Arun");
        // adding names to list

        for (String studentName : students) {
            if (studentName.startsWith("A")) {
                System.out.println(studentName);
            }
        }
    }
}
*/

package com.training.java.ee.collections;

import java.util.ArrayList;
import java.util.List;

public class StudentsNameFilter {

    public static void main(String[] args) {
        List<String> students = new ArrayList<>();
        students.add("Krishnan");
        students.add("Abishek");
        students.add("Arun");
        students.add("Vignesh");
        students.add("Kiruthiga");
        students.add("Murugan");
        students.add("Adhithya");
        students.add("Balaji");
        students.add("Vicky");
        students.add("Priya");

        for (String name : students) {
            if (name.startsWith("A")) {
                System.out.println(name);
            }
        }

    }

}
