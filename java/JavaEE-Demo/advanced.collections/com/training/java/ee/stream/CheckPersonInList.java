/*
Problem statement:
`````````````````
13. Consider the following Person:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Check if the above person is in the roster list obtained 
    from Person class.

----------WBS----------

Requirement:
```````````
Check if the above person is in the roster list obtained from Person class.

Entity:
``````
-CheckPersonInList

Method signature:
````````````````
public static void main(String[] args)
public static boolean contains(List<Person> list, Person o)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a list roster
3) Store a object of Person to a variable of that type
4) Check the object stored on a variable is present in list
    4.1) If present display as true
    4.2) Otherwise display as false

Pseudo Code:
```````````
public class CheckPersonInList {

    public static boolean contains(List<Person> persons, Person o) {
        for (Person person : persons) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " +
                contains(roster, aPerson));
    }

}
*/

package com.training.java.ee.stream;

import java.time.chrono.IsoChronology;
import java.util.List;

public class CheckPersonInList {

    public static boolean contains(List<Person> persons, Person o) {
        for (Person person : persons) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " +
                contains(roster, aPerson));
    }

}
