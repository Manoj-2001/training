/*
Problem statement:
`````````````````
3. Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

----------WBS----------

Requirement:
```````````
Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

Entity:
``````
-StreamFilterDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the class Person, the method returns a list
2) Store the returned list to a new list persons
3) Create a Stream of persons objects
3) Filter each person
    3.1) If the person's gender is male
4) Store the filtered persons to filteredPersons list
4) get first person from the filtered persons
5) get last person from the filtered persons
6) get random person from the filtered persons

Pseudo Code:
```````````
public class StreamFilterDemo {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
                .filter(person -> person.gender == Person.Sex.MALE)
                .collect(Collectors.toList());

        filteredPersons.get(0).printPerson();

        filteredPersons.get(filteredPersons.size() - 1).printPerson();

        Random random = new Random();
        filteredPersons.get(random.nextInt(filteredPersons.size())).printPerson();
    }

}
*/

package com.training.java.ee.stream;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamFilterDemo {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
                .filter(person -> person.gender == Person.Sex.MALE)
                .collect(Collectors.toList());
        //using Stream API filter getting the persons who are male.

        filteredPersons.get(0).printPerson();
        //getting the first person in the male list

        filteredPersons.get(filteredPersons.size() - 1).printPerson();
        //getting the last person in the male list

        Random random = new Random();
        filteredPersons.get(random.nextInt(filteredPersons.size())).printPerson();
        //using the object of Random class getting a random number given the range
        //as list size
        //the printPerson() method prints the person name along with his age.
    }

}
