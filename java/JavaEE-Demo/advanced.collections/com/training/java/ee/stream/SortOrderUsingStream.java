/*
Problem statement:
`````````````````
11. Sort the roster list based on the person's age in descending order using
java.util.Stream

----------WBS----------

Requirement:
```````````
Sort the roster list based on the person's age in descending order using
java.util.Stream

Entity:
``````
-SortOrderUsingStream

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a new list persons
3) sort the list based on the person age in descending order using Person 
class method compareByAge
4) For each person in persons list to print person details

Pseudo Code:
```````````
public class SortOrderUsingStream {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        persons.stream().sorted(Person::compareByAge).forEach(Person::printPerson);
    }

}
*/

package com.training.java.ee.stream;

import java.util.List;

public class SortOrderUsingStream {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        persons.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
        // stream() method can pipeline many different methods to get desired
        // results in single line of code
        // sorted() method accepts comparator here compareByAge method of person
        // class is passed
        // finally forEach to print person details

    }

}
