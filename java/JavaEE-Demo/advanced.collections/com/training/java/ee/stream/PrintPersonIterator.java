/*
Problem statement:
`````````````````
10.Iterate the roster list in Persons class and and print the person
without using forLoop/Stream 

----------WBS----------

Requirement:
```````````
Iterate the roster list in Persons class and and print the person
without using forLoop/Stream 

Entity:
``````
-PrintPersonIterator

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a new list persons
3) Create an Iterator object
4) If the iterator object has Person's objects
    4.1) Display the person details using printPerson method

Pseudo Code:
```````````
public class PrintPersonIterator {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        Iterator<Person> iterator = persons.iterator();
        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }
    }

}
*/

package com.training.java.ee.stream;

import java.util.Iterator;
import java.util.List;

public class PrintPersonIterator {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();
        // creating a object of iterator
        while (iterator.hasNext()) {
            iterator.next().printPerson();
            // next returns object of person
            //printPerson() prints the person name and age
        }
    }

}
