/*
Problem statement:
`````````````````
12. Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
    - Create the roster from the Person class and add each person in the
        newRoster to the existing list and print the new roster List.
    - Print the number of persons in roster List after the above addition.
    - Remove the all the person in the roster list

----------WBS----------

Requirement:
```````````
- Create the roster from the Person class and add each person
    in the newRoster to the existing list and print the new roster List.
- Print the number of persons in roster List after the above addition.
- Remove the all the person in the roster list

Entity:
``````
-NewRosterList

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a list roster
3) Creating a newRoster list and add new Person objects to it.
4) Add the objects of newRoster list to roster list.
5) For each person display the details of person
6) Display the size of the roster list
7) To clear all the elements in roster list.

Pseudo Code:
```````````
public class NewRosterList {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        newRoster.forEach(roster::add);
        // adding the objects(elements) in newRoster to roster list

        newRoster.forEach(Person::printPerson);

        roster.size();
        // printing the number of persons in list
        roster.clear();
        // removing all the persons on roster list
    }

}
*/

package com.training.java.ee.stream;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class NewRosterList {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        newRoster.forEach(roster::add);
        // adding the objects(elements) in newRoster to roster list
        newRoster.forEach(Person::printPerson);
        // printing the persons in newRoster list
        System.out.println("Number of persons in roster list: " + roster.size());
        // printing the number of persons in list
        roster.clear();
        // removing all the elements of roster list
        System.out.println("Number of persons in roster list: " + roster.size());
    }

}
