/*
Problem statement:
`````````````````
1) Write a program to filter the Person, who are male and age greater than 21

----------WBS----------

Requirement:
```````````
Write a program to filter the Person, who are male and age greater than 21

Entity:
``````
-FilterByGenderAndAge

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the class Person, the method returns a list
2) Store the returned list
3) Filter each person if
    3.1) The person gender is male and
    3.2) The person age is greater than 21.
4) Display the filtered persons using forEach method of Streams

Pseudo Code:
```````````
public class FilterByGenderAndAge {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
            .filter(person -> person.gender == Person.Sex.MALE)
            .filter(person -> person.getAge() > 21)
            .collect(Collectors.toList());

        filteredPersons.forEach(System.out::println);
    }

}
*/

package com.training.java.ee.stream;

import java.util.List;
import java.util.stream.Collectors;

public class FilterByGenderAndAge {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        //storing the returned list into persons list

        List<Person> filteredPersons = persons.stream()
            .filter(person -> person.gender == Person.Sex.MALE)
            .filter(person -> person.getAge() > 21)
            .collect(Collectors.toList());
        // filtering the persons based on their gender and age

        filteredPersons.forEach(Person::printPerson);
        // displaying the filtered persons
    }

}
