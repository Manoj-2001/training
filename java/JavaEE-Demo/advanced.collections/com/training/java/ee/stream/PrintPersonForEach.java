/*
Problem statement:
`````````````````
8. Print all the persons in the roster using java.util.Stream<T>#forEach 

----------WBS----------

Requirement:
```````````
Print all the persons in the roster using java.util.Stream<T>#forEach

Entity:
``````
-PrintPersonForEach

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a new list persons.
3) For each person in persons list
    3.1) displays the details of person using printPerson method of person object

Pseudo Code:
```````````
public class PrintPersonForEach {
    public static void main(String[] args) {
        List<Person> persons = person.createRoster();
        // creating a list and storing the returned list from method.

        persons.forEach(Person::printPerson);
        //stream API's forEach access objects of list sequentially and the 
        //method referenced to each object of the class.
}
*/

package com.training.java.ee.stream;

import java.util.List;

public class PrintPersonForEach {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        persons.forEach(Person::printPerson);
        // forEach person in persons list
        // using printPerson method to print the person name and age
    }

}
