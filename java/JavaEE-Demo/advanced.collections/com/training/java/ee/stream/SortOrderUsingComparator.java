/*
Problem statement:
`````````````````
9) Sort the roster list based on the person's age in descending order
using comparator

----------WBS----------

Requirement:
```````````
Sort the roster list based on the person's age in descending order
using comparator

Entity:
``````
-SortOrderUsingComparator

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a new list persons
3) sort the list based on the person age in descending order
4) for each person print the person's details

Pseudo Code:
```````````
public class SortOrderUsingComparator {
    public static void main(String[] args) {
        List<Person> persons = person.createRoster();
        // creating a list and storing the returned list from method.

        persons.sort(Comparator.comparing(Person::getAge).reversed());
        // sort method requires Comparator as parameter
        // Comparator has a method comparing which accepts function as parameter
        // reversed() method reverses the sorted list.
}
*/

package com.training.java.ee.stream;

import java.util.Comparator;
import java.util.List;

public class SortOrderUsingComparator {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        persons.sort(Comparator.comparing(Person::getAge).reversed());
        // sorting the list using Comparator methods
        for (Person person : persons) {
            person.printPerson();
        }
        // iterating the list to print the person details
    }

}
