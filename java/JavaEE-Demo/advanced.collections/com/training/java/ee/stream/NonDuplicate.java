/*
Problem statement:
`````````````````
7. Consider a following code snippet:
     List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
   - Get the non-duplicate values from the above list using java.util.Stream API

----------WBS----------

Requirement:
```````````
Consider a following code snippet:
     List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
   - Get the non-duplicate values from the above list using java.util.Stream API

Entity:
``````
-NonDuplicate

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the list of random numbers
2) Filter the numbers only which have no repeations, using frequency method 
to check the occurance is 1.
3) Display the filtered elements.

Pseudo Code:
```````````
public class NonDuplicate {
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);

        randomNumbers.stream()
        .filter(number -> Collections.frequency(randomNumbers, number) == 1);
        // filter the elements which not repeated
}
*/

package com.training.java.ee.stream;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NonDuplicate {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
        .filter(number -> Collections.frequency(randomNumbers, number) == 1)
        .forEach(System.out::println); 
    }

}
