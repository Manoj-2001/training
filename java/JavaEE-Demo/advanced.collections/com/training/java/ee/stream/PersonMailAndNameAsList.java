/*
Problem statement:
`````````````````
5. Write a program to collect the minimal person with name and email address
from the Person class using java.util.Stream<T> API as List

----------WBS----------

Requirement:
```````````
Write a program to collect the minimal person with name and email address
from the Person class using java.util.Stream<T> API as List

Entity:
``````
-PersonMailAndNameAsList

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a new list persons.
3) Collecting the emails of persons list using map method and convert to list
4) For each person
    4.1) Check person's email matches with email in emails list display the name
        of person with email address.

Pseudo Code:
```````````
public class PersonMailAndNameAsList {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        List<String> emails = persons.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());

        for (int emailIndex = 0; emailIndex < emails.size(); emailIndex++) {
            for (int personIndex = 0; personIndex < persons.size(); personIndex++) {
                if (persons.get(personIndex).emailAddress == emails.get(emailIndex)) {
                    System.out.println("Person name: " +
                        persons.get(personIndex).name + ", mail Id: " +
                        emails.get(emailIndex));
                }
            }
        }
    }

}
*/

package com.training.java.ee.stream;

import java.util.List;
import java.util.stream.Collectors;

public class PersonMailAndNameAsList {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        // creating a list and storing the returned list

        List<String> emails = persons.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        // collecting the emails from persons list using pipelined
        // methods of stream API

        for (int emailIndex = 0; emailIndex < emails.size(); emailIndex++) {
            for (int personIndex = 0; personIndex < persons.size(); personIndex++) {
                if (persons.get(personIndex).emailAddress == emails.get(emailIndex)) {
                    System.out.println("Person name: " +
                        persons.get(personIndex).name + ", mail Id: " +
                        emails.get(emailIndex));
                }
            }
        }
        // using email to match the email in persons list to
        // get the name of the person.
    }
}
