/*
Problem statement:
`````````````````
14. Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Remove the only person who are in the newRoster from the roster list.
       - Remove the following person from the roster List:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));

----------WBS----------

Requirement:
```````````
- Remove the only person who are in the newRoster from the roster list.
- Remove the following person from the roster List:
        new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

Entity:
``````
-RemovePersonFromList

Method signature:
````````````````
public static int contains(List<Person> list, Person o)
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the Person class, the method returns a list
2) Store the returned list to a list roster
3) Create a new roster list and store some Person objects
3) For each person in roster list
    3.1) Check if the person present in newRoster list
        3.1.1) If present remove the person from roster list
4) Check the person object aPerson persent in roster list
    4.1) If present remove the person from roster list

Pseudo Code:
```````````
public class RemovePersonFromList {

    public static int contains(List<Person> list, Person o) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).birthday.equals(o.birthday) &&
                    list.get(i).emailAddress.equals(o.emailAddress) &&
                    list.get(i).gender.equals(o.gender) &&
                    list.get(i).name.equals(o.name)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();

        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        roster.forEach(Person::printPerson);

        Person.createRoster().forEach(person -> {
            int index = contains(newRoster, person);
            if (index >= 0) roster.remove(index);
        });

        roster.forEach(Person::printPerson);

        Person aPerson = new Person(
              "Bob",
              IsoChronology.INSTANCE.date(2000, 9, 12),
              Person.Sex.MALE, "bob@example.com");
        // an object is stored to a variable of that type

        int index = contains(roster, aPerson);
        if (index >= 0) {
            roster.remove(index);
            System.out.println(roster.get(index).name +
                    " removed from list");
        } else System.out.println("The person not found in list");
    }

}
*/

package com.training.java.ee.stream;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class RemovePersonFromList {

    public static int contains(List<Person> list, Person o) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).birthday.equals(o.birthday) &&
                    list.get(i).emailAddress.equals(o.emailAddress) &&
                    list.get(i).gender.equals(o.gender) &&
                    list.get(i).name.equals(o.name)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        // creating a roster list of person class
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        // creating a new roster and adding some objects

        roster.forEach(Person::printPerson);

        Person.createRoster().forEach(person -> {
            int index = contains(newRoster, person);
            if (index >= 0) roster.remove(index);
        });
        System.out.println();

        roster.forEach(Person::printPerson);

        Person aPerson = new Person(
              "Bob",
              IsoChronology.INSTANCE.date(2000, 9, 12),
              Person.Sex.MALE, "bob@example.com");
        // an object is stored to a variable of that type

        int index = contains(roster, aPerson);
        if (index >= 0) {
            roster.remove(index);
            System.out.println(roster.get(index).name +
                    " removed from list");
        } else System.out.println("The person not found in list");
        // checking the occurance of the if present the object is 
        //removed from list
    }

}
