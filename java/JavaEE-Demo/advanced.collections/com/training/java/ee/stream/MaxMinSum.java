/*
Problem statement:
`````````````````
4. Consider a following code snippet:
        List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API

----------WBS----------

Requirement:
```````````
Consider a following code snippet:
        List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API

Entity:
``````
-MaxMinSum

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a list of random numbers.
2) Stream method to get stream of objects
3) Collect stream of objects
4) Sum all the collected integers using summingInt of Comparator
5) Get minimum number from collected numbers using getMin method.
6) Get maximum number from collected numbers using getMax method.

Pseudo Code:
```````````
public class MaxMinSum {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);

        int sum = randomNumbers.stream()
                .collect(Collectors.summingInt(Integer::intValue));

        int maxInList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMax();

        int minInList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMin();
    }

}
*/

package com.training.java.ee.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MaxMinSum {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        // list of random numbers

        int sum = randomNumbers.stream()
                .collect(Collectors.summingInt(Integer::intValue));
        // stream method used to get the desired result with the pipelined methods
        // summingInt method helps to sum all the integer values

        int maxInList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMax();
        // getMax method returns maximum element from collected integers
        // summarizingInt method accepts integer values

        int minInList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMin();
        // getMin method returns minimum element from collected integers

        System.out.println("Sum: " + sum);
        System.out.println("Minimum in list: " + minInList);
        System.out.println("Maximum in list: " + maxInList);
    }

}
