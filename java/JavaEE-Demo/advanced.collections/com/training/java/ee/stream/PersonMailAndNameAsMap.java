/*
Problem statement:
`````````````````
2. Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API

----------WBS----------

Requirement:
```````````
Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API

Entity:
``````
-PersonMailAndNameAsMap

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Invoke the createRoster method using the class Person, the method returns a list
2) Store the returned list to a new list persons
3) Create a Stream of persons objects
4) Collect the stream of objects
5) Map the name and email of collected persons and store to a Map mappedData
6) Get the entry set and display all the mapped data using Stream's forEach

Pseudo Code:
```````````
public class PersonMailAndNameAsMap {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        Map<String, String> mappedData = persons.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));

        mappedData.entrySet().forEach(System.out::println);
        // printing all the mapped data
    }

}

*/

package com.training.java.ee.stream;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonMailAndNameAsMap {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();

        Map<String, String> mappedData = persons.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        // stream method returns stream of objects which can be parallely processed
        // collect method collects the stream of data(object)
        // Using streams Collector to map all the persons email and their name

        mappedData.entrySet().forEach(System.out::println);
        // printing all the mapped data
    }

}
