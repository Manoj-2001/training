/*
Problem statement:
`````````````````
3.demonstrate linked hash set to array() method in java

----------WBS----------

Requirement:
```````````
Demonstrate linked hash set to array() method in java

Entity:
``````
-SetToArray

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Add languages to a programmingLanguages linked hash set.
2) Convert the programmingLanguages set into array.

Pseudo Code:
```````````
public class LinkedHashSetDemo {
    public static void main(String[] args) {

        LinkedHashSet<Type>() programmingLanguages = new LinkedHashSet<Type>();

        programmingLanguages.add(element);
        // adding elements to linked hash set

        programmingLanguages.toArray();
        // this method returns the array of all elements in the set
    }
}
*/

package com.training.java.ee.map.set;

import java.util.LinkedHashSet;

public class SetToArray {

    public static void main(String[] args) {
        LinkedHashSet<String> programmingLanguages = new LinkedHashSet<>();
        programmingLanguages.add("C");
        programmingLanguages.add("C++");
        programmingLanguages.add("Java");
        programmingLanguages.add("Python");
        programmingLanguages.add("Java");
        programmingLanguages.add("JS");

        System.out.println(programmingLanguages);
        // before converting the set into array

        System.out.println(programmingLanguages.toArray());
        // after converting the set into array

    }

}
