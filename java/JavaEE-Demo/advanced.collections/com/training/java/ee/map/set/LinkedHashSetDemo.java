/*
Problem statement:
`````````````````
2.demonstrate program explaining basic add and traversal operation of 
linked hash set

----------WBS----------

Requirement:
```````````
Demonstrate program explaining basic add and traversal operation of 
linked hash set

Entity:
``````
-LinkedHashSetDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) To add programming languages to a programming languages linked hash set.
2) To go through programming language in the programming languages linked hash 
set to display all the languages.

Pseudo Code:
```````````
public class LinkedHashSetDemo {
    public static void main(String[] args) {

        LinkedHashSet<Type>() programmingLanguages = new LinkedHashSet<Type>();

        programmingLanguages.add(element);
        // adding elements to linked hash set

        Iterator<Type> iterator = programmingLanguages.iterator();
        // traversing all the elements in the iterator.
    }
}
*/

package com.training.java.ee.map.set;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

    public static void main(String[] args) {
        LinkedHashSet<String> programmingLanguages = new LinkedHashSet<>(10, 2);
        programmingLanguages.add("C");
        programmingLanguages.add("C++");
        programmingLanguages.add("Java");
        programmingLanguages.add("Python");
        programmingLanguages.add("Java");
        programmingLanguages.add("JS");
        /*
         * adding elements to linked hash set
         * while adding the elements the order of the elements won't change
         * if same element repeats the repeated element not get added into set.
        */
        Iterator<?> language = programmingLanguages.iterator();
        while(language.hasNext()) {
            System.out.println(language.next());
        }
        // traversing all elements in the set

    }

}
