/*
Problem statement:
`````````````````
4.java program to demonstrate insertions and string buffer in tree set

----------WBS----------

Requirement:
```````````
To demonstrate insertions and string buffer in tree set

Entity:
``````
-TreeSetDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) To add numbers to a numbers tree set.
2) To add animals to a animals tree set using string builder or string buffer.
3) If any interruption occurs because of casting string to string buffer.
    3.1) It can be handled by implementing Comparator interface or Comparable 
         interface. or
    3.2) Converting StringBuffer to String using method.
Pseudo Code:
```````````
public class TreeSetDemo implements Comparator<StringBuffer> {
    public static void main(String[] args) {

        TreeSet<Integer>() numbers = new TreeSet<Integer>();

        numbers.add(element);
        // adding elements to linked hash set

        TreeSet<StringBuffer> animals = new TreeSet<StringBuffer>();

        animals.add(new StringBuffer("Dog"));
        // adding objects of StringBuffer class to the tree set.

        (display animals in animals tree set.
        the animals are sorted alphabetically using comparator)
    }
}
*/

package com.training.java.ee.map.set;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo implements Comparator<StringBuffer> {
    
    public static void main(String[] args) 
    {
        TreeSet<Integer> numbers = new TreeSet<>();
        numbers.add(21);
        numbers.add(12);
        numbers.add(5);
        numbers.add(17);
        numbers.add(6);

        System.out.println(numbers);

        Set<StringBuffer> animals = new TreeSet<>(new TreeSetDemo());
        animals.add(new StringBuffer("Dog"));
        animals.add(new StringBuffer("Zebra"));
        animals.add(new StringBuffer("Cow"));
        animals.add(new StringBuffer("Goat"));
        animals.add(new StringBuffer("Snake"));

        System.out.println(animals);
    }

    @Override
    public int compare(StringBuffer string1, StringBuffer string2) {
        return string1.toString().compareTo(string2.toString());
    }
} 
