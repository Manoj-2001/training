/*
Problem statement:
`````````````````
5.demonstrate java program to working of map interface using
( put(), remove(), booleanContainsValue(), replace() ).
If possible try remaining methods.

----------WBS----------

Requirement:
```````````
To demonstrate map interface using
( put(), remove(), booleanContainsValue(), replace() )
If possible try remaining methods.

Entity:
``````
-MapDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Put students and their marks to a studentsMarks map.
2) Remove students from studentsMarks map.
3) Check whether a random student's marks is present in studentsMarks map.
4) Change the marks of particular student using student's id.

Pseudo Code:
```````````
public class MapDemo {
    public static void main(String[] args) {

        Map<Integer,List<Integer>> studentsMarks = new HashMap<>();
        studentsMarks.put(01, (90, 89, 67, 78, 85));
        // Add elements to studentsMarks map

        studentsMarks.remove(01);
        // Remove elements with specified key

        studentsMarks.containsKey(01);
        // Check the element is present in map

        studentsMarks.replace(01, (92, 80, 77, 79, 95));
        // Replace the value of a key
    }
}
*/

package com.training.java.ee.map.set;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapDemo {

    public static void main(String[] args) {
        Map<Integer, List<Integer>> studentsMarks = new HashMap<>();
        studentsMarks.put(01, Arrays.asList(90, 89, 67, 78, 85));
        studentsMarks.put(02, Arrays.asList(78, 80, 65, 90, 91));
        studentsMarks.put(03, Arrays.asList(56, 60, 75, 60, 61));
        studentsMarks.put(04, Arrays.asList(77, 55, 62, 93, 71));
        studentsMarks.put(05, Arrays.asList(88, 85, 0, 67, 61));
        System.out.println(studentsMarks);
        // adding key value pair to map 'studentsMarks'

        studentsMarks.remove(04);
        System.out.println(studentsMarks);
        // removing a entry using key

        boolean check = studentsMarks.containsKey(05);
        System.out.println("contains: " + check);
        // checking the key is present in map

        if (check) {
            studentsMarks.replace(05, Arrays.asList(88, 85, 52, 67, 61));
        }
        // replacing the value of the key present in map

        studentsMarks.entrySet().forEach(System.out::println);
        // getting the entry set using entrySet method 

        System.out.println(studentsMarks.hashCode());

        studentsMarks.clear();
        // clear all the entries in map
        System.out.println(studentsMarks);
    }

}
