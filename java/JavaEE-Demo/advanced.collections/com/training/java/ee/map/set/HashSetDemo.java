/*
Problem statement:
`````````````````
1) java program to demonstrate adding elements, displaying,
 removing, and iterating in hash set.

----------WBS----------

Requirement:
```````````
Java program to demonstrate adding elements, displaying, removing, 
and iterating in hash set.

Entity:
``````
-HashSetDemo

Method signature:
````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) To add names to a names set.
2) To display the names set.
3) To remove a name from names set.
4) To go through name in the names set.

Pseudo Code:
```````````
class HashSetDemo {

    public static void main(String[] args) {

        Set<T> names = new HashSet<T>();
        // creating a set.

        names.add(element);
        //adding element to set

        System.out.println(names);
        //displaying the set

        names.remove(element);
        //removing element from set

        for (T name : names) {
            //iterating the elements of set
        }
    }
}

*/

package com.training.java.ee.map.set;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {

    public static void main(String[] args) {

        Set<String> names = new HashSet<>();
        // creating a set
        names.add("Arun");
        names.add("Ganesh");
        names.add("Haran");
        names.add("Manoj");
        names.add("Saran");
        // adding elements to set

        System.out.println(names);
        // displaying the set

        System.out.println("Item removed: " + names.remove("Bala"));
        // removing an element

        for (String name : names) {
            System.out.println(name);
        }
        // iterating elements in set

    }

}
