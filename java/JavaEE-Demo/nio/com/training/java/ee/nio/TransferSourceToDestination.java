/*
1) create two files named source and destination . Transfer the data from
source file to destination file using nio filechannel

----------WBS----------

Requirement:
```````````
Transfer the data from source file to destination file using nio filechannel

Entity:
``````
-TransferSourceToDestination

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source and destination file paths
2) If a file not present on the specified location
    2.1) Throw an exception
3) Create a FileInputStream and FileOutputStream to parse stream of data
4) Use getChannel method from FileChannel
5) Use transferFrom method to transfer data in source channel to destination channel
6) Close the files after work done.

Pseudo Code:
```````````
public class TransferSourceToDestination {

    public static void main(String[] args) throws IOException {

        String sourceFile = (source file path);
        String destFile = (destination file path);

        FileChannel sourceChannel = null;
        FileChannel destChannel = null;

        try {
            sourceChannel = new FileInputStream(sourceFile).getChannel();
            destChannel = new FileOutputStream(destFile).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
        finally {
            sourceChannel.close();
            destChannel.close();
        }
    }

}
*/

package com.training.java.ee.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class TransferSourceToDestination {

    @SuppressWarnings("resource")
    public static void main(String[] args) throws IOException {
        // source and destination file location
        String sourceFile = "nio/com/training/java/ee/nio/source.txt";
        String destFile = "nio/com/training/java/ee/nio/destination.txt";

        FileChannel sourceChannel = null;
        FileChannel destChannel = null;

        // get the channel from file input and output streams
        try {
            sourceChannel = new FileInputStream(sourceFile).getChannel();
            destChannel = new FileOutputStream(destFile).getChannel();
            // transfer the data of source channel to destination channel
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } catch (IOException ioe) {
            System.out.println(ioe);
        } finally {
            sourceChannel.close();
            destChannel.close();
        }
    }

}
