/*
2) To obtain current directory and parent directory using codes(. & ..)

----------WBS----------

Requirement:
```````````
To obtain current directory and parent directory

Entity:
``````
-CurrentAndParentDirectory

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the current directory using getProperty method in System
2) Display the current directory
3) Get the path of current directory using get method of Paths
4) Use getParent method to get the parent directory and display it.

Pseudo Code:
```````````
public class CurrentAndParentDirectory {

    public static void main(String[] args) {
        // current directory
        String currentDirectory = System.getProperty("user.dir");
        System.out.println(currentDirectory);

        // parent directory
        Path currentPath = Paths.get(currentDirectory);
        Path parentDirectory = currentPath.getParent();
        System.out.println(parentDirectory);
    }

}
*/

package com.training.java.ee.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class CurrentAndParentDirectory {

    public static void main(String[] args) {
        // current directory
        String currentDirectory = System.getProperty("user.dir");
        System.out.println("current dir --> " + currentDirectory);

        // parent directory
        Path currentPath = Paths.get(currentDirectory);
        Path parentDirectory = currentPath.getParent();
        System.out.println("parent dir --> " + parentDirectory);
    }

}
