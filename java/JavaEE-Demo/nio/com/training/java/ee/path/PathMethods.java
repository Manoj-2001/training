/*
3) To demonstrate toAbsolutePath(), normalize(), getName(),
getFileName() and getFileCount()

----------WBS----------

Requirement:
```````````
To demonstrate toAbsolutePath(), normalize(), getName(),getFileName()
and getFileCount()

Entity:
``````
-PathMethods

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of a file using get method of Paths
2) Get the absolute path of the file using toAbsolutePath method
3) Get the normalized file path using normalize method in Path
4) Get the particular name in path using index
5) Get the file name on the specified path
6) Get the name count in path specified using getNameCount method

Pseudo Code:
```````````
public class PathMethods {

    public static void main(String[] args) {
        Path path = Paths.get(#path);

        path.toAbsolutePath();
        path.normalize();
        path.getName(3);
        path.getFileName();
        path.getNameCount();
    }

}
*/

package com.training.java.ee.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathMethods {

    public static void main(String[] args) {
        Path path = Paths.get("nio/com/training/java/ee/path/PathMethods.java");

        // toAbsolutePath()
        System.out.println("Absolute path = " + path.toAbsolutePath());

        // normalize()
        Path newpath = Paths.get("C:/Users/User/eclipse-workspace/../PathMethods.java");
        System.out.println("Normalized path = " + newpath.normalize());

        // getName()
        System.out.println(path.getName(3)); // get the directory name using index

        // getFileName()
        System.out.println("file-name = " + path.getFileName());

        // getFileCount()
        System.out.println("path name count = " + path.getNameCount() +
                "\n" + "newpath name count = " + newpath.getNameCount());
    }

}
