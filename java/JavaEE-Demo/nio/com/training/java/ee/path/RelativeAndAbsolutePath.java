/*
1) To demonstrate absolute and relative path

----------WBS----------

Requirement:
```````````
To demonstrate absolute and relative path

Entity:
``````
-RelativeAndAbsolutePath

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of the file using get method of Paths
2) Get the relative path, at default the path is relative
3) Get the absolute path using the toAbsolutePath method.

Pseudo Code:
```````````
public class RelativeAndAbsolutePath {

    public static void main(String[] args) {
        Path path = Paths.get(#file_path);
        System.out.println("Relative path: " + path);
        System.out.println("Absolute path: " + path.toAbsolutePath());
    }

}
*/

package com.training.java.ee.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class RelativeAndAbsolutePath {

    public static void main(String[] args) {
        Path path = Paths.get("nio/com/training/java/ee/path/RelativeAndAbsolutePath.java");
        System.out.println("Relative path: " + path); // relative path
        System.out.println("Absolute path: " + path.toAbsolutePath()); // absolute path
    }

}
