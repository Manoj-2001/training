/*
1) Write a program to write and read the string to a channel
using buffer(to demonstrate Pipe)

----------WBS----------

Requirement:
```````````
To write and read the string to a channel using buffer(to demonstrate Pipe)

Entity:
``````
-StringToChannel

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the string data to stored into channel
2) Open the Pipe using open method
3) Use sink and source channels to read and write data
4) Allocate memory size to ByteBuffer
5) Put array of bytes into ByteBuffer
6) Write bytes data into sink channel
7) Read the bytes on buffer using sourceChannel
8) Close the channels


Pseudo Code:
```````````
public class StringToChannel {
    public static void main(String[] args) throws IOException {

        String data = "The data to be written to channel";

        Pipe pipe = Pipe.open();
        Pipe.SinkChannel sinkChannel = pipe.sink();
        ByteBuffer buffer = ByteBuffer.allocate(512);

        buffer.clear();
        buffer.put(data.getBytes());
        buffer.flip();

        // writing then string data to Pipe's SinkChannel
        while (buffer.hasRemaining()) {
            sinkChannel.write(buffer);
        }
        sinkChannel.close();

        Pipe.SourceChannel sourceChannel = pipe.source();
        buffer.clear();

        // reading the buffer data using Pipe's SourceChannel
        if (sourceChannel.read(buffer) > 0) {

            buffer.flip();
            while (buffer.hasRemaining()) {
                System.out.print((char) buffer.get());
            }

            buffer.clear();
        }
        sourceChannel.close();
    }
}

*/

package com.training.java.ee.pipe;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

public class StringToChannel {
    public static void main(String[] args) throws IOException {

        String data = "The data to be written to channel";

        Pipe pipe = Pipe.open();
        Pipe.SinkChannel sinkChannel = pipe.sink();
        ByteBuffer buffer = ByteBuffer.allocate(512);

        buffer.clear();
        buffer.put(data.getBytes());
        buffer.flip();

        // writing then string data to Pipe's SinkChannel
        while (buffer.hasRemaining()) {
            sinkChannel.write(buffer);
        }
        sinkChannel.close();

        Pipe.SourceChannel sourceChannel = pipe.source();
        buffer.clear();

        // reading the buffer data using Pipe's SourceChannel
        if (sourceChannel.read(buffer) > 0) {

            buffer.flip();
            while (buffer.hasRemaining()) {
                System.out.print((char) buffer.get());
            }
            buffer.clear();
        }
        sourceChannel.close();
    }
}
