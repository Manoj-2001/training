/*
2) Search any file using walkFileTree() method with enum
instance(SKIP_SIBLINGS,SKIP_SUBTREE).

----------WBS----------

Requirement:
```````````
Search any file using walkFileTree() method with enum instance
(SKIP_SIBLINGS,SKIP_SUBTREE).

Entity:
``````
-SearchFile

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Check the file is exist using exists method in Files
2) Create directories on the specified path
3) Copy the file on the specified path to the destination file on the specified path
4) Move the file on the specified path to the specified path
5) Delete the directory on the path specified.

Pseudo Code:
```````````
public class FileOperationsDemo {

    public static void main(String[] args) throws IOException, InterruptedException {
        // checking the file is exists or not
        System.out.println("Path exists: " + Files.exists(path));

        // creating new directory
        if (Files.createDirectories(path) != null) {
            System.out.println("Directories created successfully");
        } else System.out.println("Failed to create directories");

        // copying file
        Files.copy(filePath, copyPath, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("File copied.");

        // moving file
        Files.move(copyPath, movePath, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("File moved");

        // deleting directory
        Files.delete(path);
        System.out.println("Directory deleted");
    }

}
*/

package com.training.java.ee.files;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class SearchFile {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("nio/com/training/java/ee/files");

        FileVisitor<Path> fileVisitor = new SimpleFileVisitor<Path>() {
            @SuppressWarnings("static-access")
            public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
            }

            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println(file);
                return super.visitFile(file, attrs);
            }
        };

        Files.walkFileTree(path, fileVisitor);
    }

}
