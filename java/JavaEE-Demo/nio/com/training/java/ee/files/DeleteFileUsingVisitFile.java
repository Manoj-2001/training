/*
3) Delete a file using visitFile() method.

----------WBS----------

Requirement:
```````````
Delete a file using visitFile() method.

Entity:
``````
-DeleteFileUsingVisitFile

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of the file using get method of Paths
2) Define the visitFile method in SimpleFileVisitor classa and create an object
of the SimpleFileVisitor.
3) Invoke the walkFileTree method which has path and fileVisitor as parameter
4) The method checks for file if file not found
    4.1) Throw an exception
5) Delete the file.


Pseudo Code:
```````````
public class DeleteFileUsingVisitFile {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(#file_path);

        FileVisitor fileVisitor = new SimpleFileVisitor<Path>() {

            public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

        });

        Files.walkFileTree(path, fileVisitor);
    }

}
*/

package com.training.java.ee.files;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteFileUsingVisitFile {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("nio/com/training/java/ee/files/delete.txt");

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

        });
    }

}
