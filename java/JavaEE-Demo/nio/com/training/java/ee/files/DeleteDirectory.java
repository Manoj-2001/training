/*
4) Delete the directory along with the files recursively.

----------WBS----------

Requirement:
```````````
Delete the directory along with the files recursively

Entity:
``````
-DeleteDirectory

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the path of the directory using get method of Paths
2) Define the visitFile and postVisitResult methods to SimpleFileVisitor class.
3) To delete the files in directories use visitFile method
4) To delete the directories using postVisitResult method to achieve.
5) Invoke the walkFileTree method of Files with the parameters path and
FileVistor.

Pseudo Code:
```````````
public class DeleteDirectory {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get(#directory path);

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }

        });
    }
}
*/

package com.training.java.ee.files;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteDirectory {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("nio/com/training/java/ee/files/sample/");

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                Files.delete(file); // this will work because it's always a File
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir); // this will work because Files in the directory are already deleted
                return FileVisitResult.CONTINUE;
            }

        });
    }
}
