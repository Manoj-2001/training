/*
1) Perform file operations using exists(), createDirectory(), copy(), move() and
delete() methods.

----------WBS----------

Requirement:
```````````
Perform file operations using exists(), createDirectory(), copy(), move() and
delete() methods.

Entity:
``````
-FileOperationsDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the source and destination file paths
2) If a file not present on the specified location
    2.1) Throw an exception
3) Create a FileInputStream and FileOutputStream to parse stream of data
4) Use getChannel method from FileChannel
5) Use transferFrom method to transfer data in source channel to destination channel
6) Close the files after work done.

Pseudo Code:
```````````
public class TransferSourceToDestination {

    public static void main(String[] args) throws IOException {

        String sourceFile = (source file path);
        String destFile = (destination file path);

        FileChannel sourceChannel = null;
        FileChannel destChannel = null;

        try {
            sourceChannel = new FileInputStream(sourceFile).getChannel();
            destChannel = new FileOutputStream(destFile).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
        finally {
            sourceChannel.close();
            destChannel.close();
        }
    }

}
*/

package com.training.java.ee.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;

public class FileOperationsDemo {

    public static void main(String[] args) throws IOException, InterruptedException {
        Path path = Paths.get("nio/com/training/java/ee/files/file.txt");
        // checking the file is exists or not
        System.out.println("Path exists: " + Files.exists(path));

        Thread.sleep(3000);

        Path newpath = Paths.get("nio/com/training/java/ee/files/sample/directory");
        // creating new directory
        if (Files.createDirectories(newpath) != null) {
            System.out.println("Directories created successfully");
        } else System.out.println("Failed to create directories");

        Thread.sleep(3000);

        Path filePath = Paths.get("nio/com/training/java/ee/files/file.txt");
        Path copyPath = Paths.get("nio/com/training/java/ee/files/sample/copy.txt");
        // copying file
        Files.copy(filePath, copyPath, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("File copied.");

        Thread.sleep(3000);

        Path movePath = Paths.get("nio/com/training/java/ee/files/sample/directory/move.txt");
        // moving file
        Files.move(copyPath, movePath, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("File moved");

        Thread.sleep(3000);

        // deleting file inside directory
        Files.walk(newpath).map(Path::toFile).forEachOrdered(File::delete);
        System.out.println("File inside the directories deleted");

        Thread.sleep(3000);

        // deleting directory
        Files.delete(newpath);
        System.out.println("Directory deleted");
    }

}
