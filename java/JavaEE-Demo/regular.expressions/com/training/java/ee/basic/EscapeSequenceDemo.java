/*
2) write a program to display the following using escape sequence in java

    A. My favorite book is "Twilight" by Stephanie Meyer

    B. She walks in beauty, like the night, 
       Of cloudless climes and starry skies 
       And all that's best of dark and bright 
       Meet in her aspect and her eyes�

    C. "Escaping characters", � 2019 Java

----------WBS----------

Requirement:
```````````
Write a program to display the following using escape sequence

Entity:
``````
-EscapeSequenceDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) To use escape sequence to escape the double quotes in string.
2) To use escape sequence to break to new line
3) To use unicode to display symbols.

Pseudo Code:
```````````
public class EmailValidationDemo {

    public static void main(String[] args) {
        System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");

        System.out.println("\n" + "She walks in beauty, like the night, \r\n" +
                "Of cloudless climes and starry skies \r\n" +
                "And all that's best of dark and bright \r\n" +
                "Meet in her aspect and her eyes�");

        System.out.println("\n" + "\"Escaping characters\", \u00A9 2019 Java");
    }

}
*/

package com.training.java.ee.basic;

public class EscapeSequenceDemo {

    public static void main(String[] args) {
        System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
        System.out.println("\n" + "She walks in beauty, like the night, \r\n" +
                "Of cloudless climes and starry skies \r\n" +
                "And all that's best of dark and bright \r\n" +
                "Meet in her aspect and her eyes�");
        // \r\n to indicate an end of line
        System.out.println("\n" + "\"Escaping characters\", \u00A9 2019 Java");
        // unicode u00A9
    }

}
