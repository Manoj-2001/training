/*
1.)Write a Java program to calculate the revenue from a sale based on the
unit price and quantity of a product input by the user. The discount rate is 
10% for the quantity purchased between 100 and 120 units, and 15% for the
quantity purchased greater than 120 units. If the quantity purchased is less
than 100 units, the discount rate is 0%. See the example output as shown below:

    Enter unit price: 25
    Enter quantity: 110
    The revenue from sale: 2475.0$
    After discount: 275.0$(10.0%)

----------WBS----------

Requirement:
```````````
Write a Java program to calculate the revenue from a sale based on the
unit price and quantity of a product. The discount rate is 
10% for the quantity purchased between 100 and 120 units, and 15% for the
quantity purchased greater than 120 units. If the quantity purchased is less
than 100 units, the discount rate is 0%.

Entity:
``````
-LogicalOperatorDemo

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the unit price and product quantity from user
2) Check the quantity
    2.1) If the quantity more than 120, 15% discount should be given
    2.2) If the quantity lies in between 120 and 100, 10% discount should be given
    2.3) Otherwise no discount should be given.
3) Calculate the revenue based on discount applied.

Pseudo Code:
```````````
public class EmailValidationDemo {

    public static void main(String[] args) {

        // getting the input from user
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter unit price: ");
        double unitPrice = scanner.nextDouble();
        System.out.println("Enter quantity: ");
        double productQuantity = scanner.nextDouble();
        double discount;

        if (productQuantity > 120) {
            discount = (unitPrice * productQuantity) * (1/10);
        } else if (productQuantity >= 100){
            discount = (unitPrice * productQuantity) * (1/15);
        } else {
            discount = 0;
        }

        double revenue = unitPrice * productQuantity + revenue;
        System.out.println(revenue);
    }

}
*/

package com.training.java.ee.basic;

import java.util.Scanner;

public class LogicalOperatorDemo {

    public static void main(String[] args) {

        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);

        // getting the input from user
        System.out.println("Enter unit price: ");
        int unitPrice = scanner.nextInt();
        System.out.println("Enter quantity: ");
        int productQuantity = scanner.nextInt();

        // total sales amount without discount
        double total = unitPrice * productQuantity;
        double discountAmount = 0;
        int discount = 0;

        // discount price according to the quantity
        if (productQuantity > 120) {
            discount = 15;
            discountAmount = total / discount;
        } else if (productQuantity >= 100) {
            discount = 10;
            discountAmount = total / discount;
        }

        // revenue after discount
        double revenue = total - discountAmount;
        System.out.format("The revenue from sale: %.1f$", revenue);
        System.out.format("\n" +
                "After discount: %.1f$(%d%%)", discountAmount, discount);

    }

}
