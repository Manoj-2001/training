/*
2) For the following code use the split method() and print in sentence

----------WBS----------

Requirement:
```````````
For the following code use the split method() and print in sentence

Entity:
``````
-SplitString

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the url input as String.
2) split the url based on the packages
3) Display the splited paths

Pseudo Code:
```````````
public class SplitString {

    public static void main(String[] args) {
        String url = "http://tutorials.jenkov.com/java/core-concepts.html";
        for (String path : url.split("/")) {
            System.out.println(path);
        }
    }

}

*/

package com.training.java.ee.regexmethods;

public class SplitString {

    public static void main(String[] args) {
        // url as source
        String url = "http://tutorials.jenkov.com/java/core-concepts.html";

        // spliting the url based on sub packages on url
        for (String path : url.split("/")) {
            System.out.println(path);
        }
    }

}
