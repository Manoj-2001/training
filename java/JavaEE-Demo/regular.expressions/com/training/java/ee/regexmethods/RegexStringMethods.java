/*
1) write a program for Java String Regex Methods?

----------WBS----------

Requirement:
```````````
Write a program for Java String Regex Methods?

Entity:
``````
-RegexStringMethods

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Check the name matches the regular expression.
2) Replace all the particular word to some other word.
3) Replace only the first word on repeated words.
4) Split the sentence based on regex.

Pseudo Code:
```````````
public class RegexStringMethods {

    public static void main(String[] args) {
        // matches(regex)
        String name = "Manoj";
        boolean match = name.matches("(.*)an(.*)");
        System.out.println("Checking the name 'Manoj' has 'an' inside: " + match);

        // replaceAll(regex, replacement)
        String sentence = "A dog's owner left his dog in railway station";
        System.out.println(sentence.replaceAll("dog", "cat"));

        // replaceFirst(regex, replacement)
        System.out.println(sentence.replaceFirst("dog", "big marketing company"));

        // split(regex)
        String directory = "D:/files/data/image/car.png";
        System.out.println("Detailed path");

        for (String path : directory.split("/")) {
            System.out.println(path);
        }
        // split(regex, limit)
        System.out.println("\nSpecified path");

        for (String path : directory.split("/", 3)) {
            System.out.println(path);
        }
    }

}

*/

package com.training.java.ee.regexmethods;

public class RegexStringMethods {

    public static void main(String[] args) {
        // matches(regex)
        String name = "Manoj";
        // .* means either nothing or anything present
        boolean match = name.matches("(.*)an(.*)");
        System.out.println("Checking the name 'Manoj' has 'an' inside: " + match);

        // replaceAll(regex, replacement)
        String sentence = "A dog's owner left his dog in railway station";
        // replaces all the regex with the replacement string
        System.out.println(sentence.replaceAll("dog", "cat"));

        // replaceFirst(regex, replacement)
        // replaces first regex with the replacement string
        System.out.println(sentence.replaceFirst("dog", "big marketing company"));

        // split(regex)
        String directory = "D:/files/data/image/car.png";
        System.out.println("Detailed path");
        // splits the string to strings whenever there is an regex
        for (String path : directory.split("/")) {
            System.out.println(path);
        }
        // split(regex, limit)
        System.out.println("\nSpecified path");
        // splits the string to strings based on the limit too
        //if limit is 2 output will 2 strings
        for (String path : directory.split("/", 3)) {
            System.out.println(path);
        }
    }

}
