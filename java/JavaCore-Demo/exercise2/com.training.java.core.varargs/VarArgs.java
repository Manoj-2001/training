/*
24. demonstrate overloading with varArgs

----------WBS----------

Requirement:
```````````
To demonstrate overloading with varArgs

Entity:
``````
-VarArgs

Method Signature:
````````````````
-public void sort(int ... a1)
-public void sort(boolean pass, int ... a2)
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create a object of class VarArgs
2) Invoke the sort method using object
3) A sort method accepts varArgs as parameter
4) Another sort method accepts boolean and varArgs as parameter.
5) The method get overrided according to the parameters passed.

*/

package varargs;

import java.util.Arrays;

class VarArgs {
    public void sort(int ... a1) {
        System.out.println("After sorting: ");
        Arrays.sort(a1, 0, a1.length);
        for (int i : a1) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
    public void sort(boolean pass, int ... a2) {
        if (pass) {
            Arrays.sort(a2, 0, a2.length);
            for (int i : a2) {
                System.out.print(i + " ");
            }
        } else {
            System.out.println("No need to sort");
        }
    }
    public static void main(String[] args) {
        VarArgs obj = new VarArgs();
        obj.sort(7, 3, 6, 11, 0, 2, 34);
        obj.sort(false, 1, 2, 3, 4, 5);
    }
}
