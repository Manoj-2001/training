/*
45. Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
*/

package string;

class Hannah {
    public static String hannah;
    public static void main(String[] args) {
        hannah = "Did Hannah see bees? Hannah did.";
        System.out.println(hannah.length());
        System.out.println(hannah.charAt(12));
        String[] words = hannah.split(" ");
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                if (word.charAt(i) == 'b') {
                    System.out.println(word);
                }
            }
        }
    }
}