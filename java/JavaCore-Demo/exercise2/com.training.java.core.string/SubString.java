/*
46. How long is the string returned by the following expression? What is the string?
"Was it a car or a cat I saw?".substring(9, 12)
*/

package string;

class SubString {
    public static void main(String[] args) {
        String str = "Was it a car or a cat I saw?";
        System.out.println(str);
        System.out.println("String length: " + str.length());
        String substr = str.substring(9, 12);
        System.out.println(substr);
        System.out.println("String length: " + substr.length());
    }
}
