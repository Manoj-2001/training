/*
49. Write a program that computes your initials from your full name and displays them.

----------WBS----------

Requirement:
```````````
Initials from your full name

Entity:
``````
-Initial

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Get the input from user using Scanner
2) Split the name
3) Get the first character and convert into uppercase.
4) Display the initial for the given name.

*/

package string;
import java.util.Scanner;
class Initial {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        String words[] = name.split(" ");
        for (String word : words) {
            char letter = word.charAt(0);
            char initial = Character.toUpperCase(letter);
            System.out.print(initial + " ");
        }
        
    }
}