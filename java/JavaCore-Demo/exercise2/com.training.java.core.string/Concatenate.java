/*
48. Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";
*/

package string;

class Concatenate {
    public static void main(String[] args) {
        String hi = "Hi, ";
        String mom = "mom.";
        System.out.println(hi + mom);
        System.out.println(String.format("%s%s", hi, mom));
    }
}
