/*
44. What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");
*/

package string;

class StringBuilderDemo {
    public static void main(String[] args) {
        // default string builder has a capacity of 16 characters.
        StringBuilder sb1 = new StringBuilder();
        System.out.println(sb1.capacity() + " Characters"); // 16 Characters
        
        // passing a string makes the length added to the default capacity 
        StringBuilder sb2 = new StringBuilder("Able was I ere I saw Elba.");
        System.out.println(sb2.capacity() + " Characters"); // 42 Characters      
    }
}
