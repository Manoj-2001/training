/*
43. The following code creates one array and one string object. How many references to 
    those objects exist after the code executes?
    Is either object eligible for garbage collection?
        String[] students = new String[10];
        String studentName = "Peter Parker";
        students[0] = studentName;
        studentName = null;
*/

package string;

class StudentName {
    public static void main(String[] args) {
        String[] students = new String[10];
        String studentName = "Peter Parker";
        System.out.println(studentName);
        students[0] = studentName;
        studentName = null;
    }
}
/*

Answer:
``````
How many references to those objects exist after the code executes?
        students array has two references after execution of code. At first two lines of code,
    the students object has the memory address of String[] array and studentName variable has the
    memory address of string memory "Peter Parker". Next two lines of code shows, a copy of memory-
    address stored in studentName points to the array index of 0, now students[0] has the memory-
    address of "Peter Parker". The studentName assigned to null this means it has no links to memory
    or no memory address.
Is either object eligible for garbage collection?
        No, because still the copy of memory address of the studentName stored in array 
    at index 0.

*/