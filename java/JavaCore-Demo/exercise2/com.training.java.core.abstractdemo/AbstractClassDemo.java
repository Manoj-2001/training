/*
37. Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the
      calculation for each class respectively.

----------WBS----------

Requirement:
```````````
To demonstrate abstract classes using shape class.

Entity:
``````
-Shape
-Circle
-Square

Method Signature:
````````````````
-public abstract void calculateArea()
-public abstract void calculatePerimeter()
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Abstract methods and properties declared inside the abstract class Shape.
2) Circle and Square classes extends the abstract class.
3) Create an object of Circle class with specific radius.
4) Create an object of Square class with specific side.
5) Invoke the calculateArea method of object of Circle class
6) Invoke the calculatePerimeter method of Square class object.
*/

package com.training.java.core.abstractdemo;

abstract class Shape {
    public double area;
    public double perimeter;

    public abstract void calculateArea();
    public abstract void calculatePerimeter();
}
class Circle extends Shape {
    public double radius;
    public static final double pi = 3.141592;
    
    public Circle(double radius) {
        this.radius = radius;
    }
    public void calculateArea() {
        area = pi * radius * radius;
        System.out.println(String.format("Area of circle: %.2f", area));
    }
    public void calculatePerimeter() {
        perimeter = 2 * pi * radius;
        System.out.println(String.format("Perimeter of circle: %.2f", perimeter));
    }
}
class Square extends Shape {
    public double side;
    
    public Square(double side) {
        this.side = side;
    }
    public void calculateArea() {
        area = side * side;
        System.out.format("Area of square: %.2f\n", area);
    }
    public void calculatePerimeter() {
        perimeter = 4 * side;
        System.out.format("Perimeter of square: %.2f\n", perimeter);
    }
}

public class AbstractClassDemo {
    public static void main(String[] args) {
        
        Shape circle = new Circle(3.21);
        circle.calculateArea();
        circle.calculatePerimeter();
        
        Shape square = new Square(5.5);
        square.calculateArea();
        square.calculatePerimeter();
    }
}