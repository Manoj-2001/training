/*
38. print the absolute path of the .class file of the current class

----------WBS----------

Requirement:
```````````
To print the absolute path of the .class file of the current class

Entity:
``````
-AbsolutePath

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) The getClassLoader method of ClassLoader class locates the file path
2) getResource method of URL class gets the class file which name passed as parameter.
*/

package java.lang;
import java.net.*;
class AbsolutePath {
    public AbsolutePath() {
       ClassLoader loader = this.getClass().getClassLoader();
       URL path = loader.getResource("AbsolutePath.class");
       System.out.println(path);
    }
    public static void main( String[] args ) {
        new AbsolutePath();
    }
}