/*
40. demonstrate object equality using Object.equals() vs ==, using String objects

----------WBS----------

Requirement:
```````````
To demonstrate object equality using Object.equals() vs ==, using String objects.

Entity:
``````
-ObjectEquality

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create two different String objects name1 and name2.
2) Check the name1 and name2 are equal using == operator.
3) Check the name1 and name2 are equal using equals method.
*/

package java.lang;
class ObjectEquality {
    public static void main(String[] args) {
        String name1 = new String("Hello World");
        String name2 = new String("Hello World");
        
        System.out.println("Operator result: " + (name1 == name2)); //checks memory location
        System.out.println("Method result: " + name1.equals(name2)); //checks value
    }
}