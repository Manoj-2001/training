/*
36. Write a class that implements the CharSequence interface found in the java.lang package.
  Your implementation should return the string backwards. Write a small main method to test 
  your class; make sure to call all four methods.

----------WBS----------

Requirement:
```````````
The class should return the string backwards

Entity:
``````
-Test
-CharSequenceReverseString

Function Declaration:
````````````````````
-public char charAt(int index)
-public int length()
-public String subSequence(int start, int end)
-public String toString()

Jobs to be done:
```````````````
1) Create a class Test that implements the CharSequence interface.
2) Class should define all 4 methods of interface.
3) override the default toString method to reverse the
string and storing on StringBuilder.
*/

package interfacedemo;
class Test implements CharSequence {
    
    String string;
    
    public Test(String string) {
        this.string = string;
    }
    public char charAt(int index) {
        return string.charAt(index);
    }
    public int length() {
        return string.length();
    }
    public String subSequence(int start, int end) {
        return string.substring(start, end);
    }
    public String toString() {
        int insertIndex = 0;
        StringBuilder reversed = new StringBuilder();
        for (int index = string.length() - 1; index >= 0; index--) {
            reversed.insert(insertIndex, string.charAt(index));
            insertIndex ++;
        }
        return reversed.toString();
    }
}
public class CharSequenceReverseString {
    public static void main(String[] args) {
        Test obj = new Test("MANOJ");
        System.out.println(obj); // this statement equivalent to obj.toString();
    }
}