/*
33. What methods would a class that implements the java.lang.CharSequence interface have
    to implement?
*/

package interfacedemo;

class CharSequenceDemo {
    public static void main(String[] args) {
        String name = "Albert Einstein";
        Integer birthYear = new Integer(4);
        Integer year = new Integer(4);
        birthYear = 1879;
        year = 1879;
        System.out.println(name.charAt(7));
        System.out.println(name.length());
        System.out.println(name.subSequence(0,8));
        System.out.println(birthYear.equals(year.toString()));
    }
}
