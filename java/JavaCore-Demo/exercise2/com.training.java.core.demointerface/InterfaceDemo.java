/*
34. What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }
*/

package interfacedemo;

interface Interface {
    void aMethod(int aValue);
}
class A implements Interface {
    public void aMethod(int aValue) {
        System.out.println("A value from aMethod: " + aValue);
    }
}
public class InterfaceDemo {
    public static void main(String[] args) {
        A obj = new A();
        obj.aMethod(5);
    }
}