/*
35. Is the following interface valid?
    public interface Marker {}
*/

package interfacedemo;
interface Marker {
}

//Answer
/*
Yes the interface is valid, the interface without any methods called 
marking class or tagging class.
*/