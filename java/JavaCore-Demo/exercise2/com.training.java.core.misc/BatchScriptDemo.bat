:: 50. write a batch script to execute all your sample programs
:: 
:: ----------WBS----------
:: 
:: Requirement:
:: ```````````
:: Batch script to execute all the sample programs
:: 
:: Entity:
:: ``````
:: None
:: 
:: Function Declaration:
:: ````````````````````
:: None
:: 
:: Jobs to be done:
:: ```````````````
::   Compile the all java files using batch statement
::   Finally run a java class. 


echo Compiling all java files..
javac *.java

echo Running VarArgs java file..
java VarArgs

cls
echo All Class files deleted