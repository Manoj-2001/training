/*
20. print fibinocci using for loop, while loop and recursion

----------WBS----------

Requirement:
```````````
fibinocci using for loop, while loop and recursion

Entity:
``````
-FibinocciSeries
-Fibinocci

Function Declaration:
````````````````````
-public void doRecursion(int range, int firstNumber, int secondNumber)
-public void forLoop(int range)
-public void whileLoop(int range)
-public void recursion(int range)
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create an object of FibinocciSeries class
2) Invoke the forLoop method to display the fibonacci series
3) Invoke the whileLoop method to display the fibonacci series
4) Invoke the recursion method
    4.1) the recursion method invokes the doRecursion method
    4.2) the method doRecursion displays the fibanocci series
*/
package com.training.java.core.controlflow;
class FibinocciSeries {
    public int firstNumber;
    public int secondNumber;
    public int sum;

    public void doRecursion(int range, int firstNumber, int secondNumber) {
        if(range > 0) {
            sum = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = sum;
            System.out.print(sum + " ");
            doRecursion(range - 1, firstNumber, secondNumber);
        }
    }

    public void forLoop(int range) {
        firstNumber = 0;
        secondNumber = 1;
        String series = "";
        for (int count = 0; count < range; count++) {
            if (count == 0) {
                series += Integer.toString(firstNumber) + " ";
            } else if (count == 1) {
                series += Integer.toString(secondNumber) + " ";
            } else {
                sum = firstNumber + secondNumber;
                series += Integer.toString(sum) + " ";
                firstNumber = secondNumber;
                secondNumber = sum;
            }
        }
        System.out.println(series);
    }

    public void whileLoop(int range) {
        firstNumber = 0;
        secondNumber = 1;
        String series = "";
        int count = 0;
        while (count < range) {
            if (count == 0) {
                series += Integer.toString(firstNumber) + " ";
            } else if (count == 1) {
                series += Integer.toString(secondNumber) + " ";
            } else {
                sum = firstNumber + secondNumber;
                series += Integer.toString(sum) + " ";
                firstNumber = secondNumber;
                secondNumber = sum;
            }
            count += 1;
        }
        System.out.println(series);
    }

    public void recursion(int range) {
        System.out.print("0 1 ");
        doRecursion(range - 2, 0, 1);
    }

}
public class Fibinocci {
    public static void main(String[] args) {
        FibinocciSeries fibinocci = new FibinocciSeries();
        fibinocci.forLoop(10) ;
        fibinocci.whileLoop(15);
        fibinocci.recursion(10);
    }
}
