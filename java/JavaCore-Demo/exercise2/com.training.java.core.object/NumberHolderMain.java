/*
4)Given the following class, called NumberHolder, write some code that creates an instance
of the class and initializes its two member variables with provided values, and then displays
the value of each member variable.

----------WBS----------

Requirement:
```````````
Code that creates an instance of the class and initializes its two member variables with provided
values, and then displays the value of each member variable.

Entity:
``````
-NumberHolder
-NumberHolderMain

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Instantiate the NumberHolder class constructor with values
2) Display the recieved values.
*/

package object.exercise;

class NumberHolder {
    public int anInt;
    public float aFloat;
    
    public NumberHolder(int anInt, float aFloat) {
        this.anInt = anInt;
        this.aFloat = aFloat;
        System.out.println(String.format("Int value: %d\nFloat value: %f", this.anInt, this.aFloat));
    }
}

public class NumberHolderMain {
    public static void main(String[] args) {
        NumberHolder obj = new NumberHolder(2, 5.5f);
    }
}