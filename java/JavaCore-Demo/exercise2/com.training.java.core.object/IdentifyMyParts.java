/*
2)What is the output from the following code:
*/
package object.exercise;

class IdentifyMyParts {
    static int x; //class variable
    int y; //instance variable
    
    public static void main(String[] args) {
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y); //a.y = 5
        System.out.println("b.y = " + b.y); //b.y = 6
        System.out.println("a.x = " + a.x); //a.x = 2
        System.out.println("b.x = " + b.x); //b.x = 2
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x); //IdentifyMyParts.x = 2
    }
}
/*
If the variable 'x' is declared as non-static then compilation error will occur while compiling
"System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);"
*/



