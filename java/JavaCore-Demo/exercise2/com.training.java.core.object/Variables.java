/*
1)Consider the following class:
*/

package object.exercise;

/*

    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?
//Answer:
Class Variables:
    Class variables are declared inside the class with static keyword, here variable 'x' is the
static variable. When an object change the value of the variable, the value will change for
class not for an instance.

Instance Variable:
    Instance variable are also called as non-static variable, these variables are declared inside 
class, method or in constructor.here 'y' is the instance variable. The objects which are using
instance variable can change the assigned value according to their needs.

*/