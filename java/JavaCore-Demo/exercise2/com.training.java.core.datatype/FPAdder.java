/*
15. Create a program that is similar to the previous one but instead of reading integer arguments, 
it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits to the right of the 
decimal point.
    For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
    The program would display 108.75. Depending on your locale, the decimal point might be 
a comma (,) instead of a period (.).

----------WBS----------

Requirement:
```````````
Create a program that reads an unspecified number of integer arguments from the command line 
and adds them together.
The program should display an error message if the user enters only one argument.

Entity:
``````
-FPAdder

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Pass the values through command line
2) If the size of the args array less than 2
	2.1) Display an error message
3) Sum all the values in array args
4) Format the sum using decimal formatter

*/

package data.type;
import java.text.DecimalFormat;
class FPAdder {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Command line args should have atleast 2 arguments");
        } else {
            double sum = 0.0;
            for(int index = 0; index < args.length; index++) {
                sum += Double.valueOf(args[index]);
            }
            DecimalFormat formatter = new DecimalFormat("#,##,###.##");
            String output = formatter.format(sum);
            System.out.println(output);
        }
    }
}