/*
8)print the type of the result value of following expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56

----------WBS----------

Requirement:
```````````
To print the type of the result value of following expressions

Entity:
``````
-ValType
-DataTypes

Method Signature:
````````````````
-public static void main(String[] args)
-public void type(int value)
-public void type(double value)
-public void type(char value)

Jobs to be done:
```````````````
1) Create a class DataTypes with type methods overloaded
2) Pass expression in type method, the method get called according to the 
resultant value type.
*/
package data.type;
class DataTypes {
    public void type(int value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(double value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(char value) {
        System.out.println(((Object)value).getClass().getName());
    }
}
public class ValType {
    public static void main(String[] args) {
        DataTypes obj = new DataTypes();
        obj.type(100 / 24);
        obj.type(100.10 / 10);
        obj.type('Z' / 2);
        obj.type(10.5 / 0.5);
        obj.type(12.4 % 5.5);
        obj.type(100 % 56);
    }
}

