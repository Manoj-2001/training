/*
10. demonstrate overloading with Wrapper types

----------WBS----------

Requirement:
```````````
Demonstrate overloading with Wrapper types

Entity:
``````
-WrapperType
-OverloadingWrapperType

Function Declaration:
````````````````````
-public static void main(String[] args)
-public void type(Integer value)
-public void type(Double value)
-public void type(String value)
-public void type(Character value)

Jobs to be done:
```````````````
1) Create a class WrapperType with methods type overloaded.
2) Pass the value to the method, the method get called according to the 
value type.
*/
package data.type;
class WrapperType {
    public void type(Integer value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(Double value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(Character value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(String value) {
        System.out.println(((Object)value).getClass().getName());
    }
}
public class OverloadingWrapperType {
    public static void main(String[] args) {
        WrapperType obj = new WrapperType();
        obj.type(12.2);
        obj.type(10);
        obj.type("apple");
        obj.type('a');
    }
}