/*
7. program to store and retrieve four boolean flags using a single int

----------WBS----------

Requirement:
```````````
Program to store and retrieve four boolean flags using a single int.

Entity:
``````
-BooleanFlag
-StudentMark

Method Signature:
````````````````
-public static void main(String[] args)
-public void resultStatus()

Jobs to be done:
```````````````
1) Create a StudentMark class with constructor
2) Students subjects marks passed as parameter to constructor
3) For each student
	3.1) If the marks of th subjects greater than 65 than they passed
	3.2) Otherwise the student failed to pass.
*/
package data.type;
class StudentMark {
    final static double averageMark = 65;
    boolean pass = false;
    int rollNumber;
    double tamil, english, mathematics, computerScience;
    
    public StudentMark(int rollNo, double tam, double eng, double math, double cs) {
        this.rollNumber = rollNo;
        this.tamil = tam;
        this.english = eng;
        this.mathematics = math;
        this.computerScience = cs;
    }
    
    public void resultStatus() {
        if ((tamil >= averageMark) && (english >= averageMark) && (mathematics >= averageMark) &&
           (computerScience >= averageMark)) {
            pass = true;
        }
        if (pass) {
            System.out.println("You have passed the exam");
        } else {
            System.out.println("You failed to pass the exam");
        }
    }
}
public class BooleanFlag {
    public static void main(String[] args) {
        StudentMark chris = new StudentMark(165001, 78.9, 82.5, 89, 95.6);
        StudentMark mark = new StudentMark(165002, 68.0, 56.9, 70.2, 69);
        
        chris.resultStatus();
        mark.resultStatus();
    }
}