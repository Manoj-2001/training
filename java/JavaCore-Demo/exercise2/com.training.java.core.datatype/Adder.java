/*
14. Create a program that reads an unspecified number of integer arguments from the command line 
and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message 
if the user enters only one argument.

----------WBS----------

Requirement:
```````````
Create a program that reads an unspecified number of integer arguments from the command line 
and adds them together.
The program should display an error message if the user enters only one argument.

Entity:
``````
-Adder

Method Signature:
````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) The arguments which are passed through Command line while running bytecode which are stored in a
String[] array.
	1.1) If number of arguments less than 2 display a error statement
2) Convert into type integer and sum the values.
3) Display the value.
*/
package data.type;
class Adder {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Command line args should have atleast 2 arguments");
        } else {
            int sum = 0;
            for (int index = 0; index < args.length; index++) {
                sum += Integer.valueOf(args[index]);
            }
            System.out.println(sum);
        }
    }
}