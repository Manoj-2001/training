/*
11. print the classname of all the primitive data types (Note: not the wrapper types)

----------WBS----------

Requirement:
```````````
Print the classname of all the primitive data types

Entity:
``````
-PrimitiveDataTypeClassName

Function Declaration:
````````````````````
-public static void main(String[] args)

Jobs to be done:
```````````````
1) Create some variables of primitive type and add some values to it.
2) Get the class of object using getClass method
3) Get the name of object using getName method
    
*/
package data.type;
class PrimitiveDataTypeClassName {
    public static void main(String[] args) {
        int intDataType = 0;
        float floatDataType = 0f;
        double doubleDataType = 0;
        long longDataType = 0;
        char charDataType = 'a';
        boolean booleanDataType = false;
        
        System.out.println(((Object)intDataType).getClass().getName());
        System.out.println(((Object)floatDataType).getClass().getName());
        System.out.println(((Object)doubleDataType).getClass().getName());
        System.out.println(((Object)charDataType).getClass().getName());
        System.out.println(((Object)longDataType).getClass().getName());
        System.out.println(((Object)booleanDataType).getClass().getName());
    }
}