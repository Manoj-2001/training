/*
9. demonstrate what happens when mixing primitive with respective Wrapper types for the 
above operations

----------WBS----------

Requirement:
```````````
demonstrate what happens when mixing primitive with respective Wrapper types for the 
above operations

Entity:
``````
-DataType
-PrimitiveWithWrapperType

Function Declaration:
````````````````````
-public static void main(String[] args)
-public void type(int value)
-public void type(double value)
-public void type(char value)
-public void type(Integer value)
-public void type(Double value)
-public void type(Character value)

Jobs to be done:
```````````````
1) Create a class DataType with methods type overloaded
2) Pass expression in method, the method get called according to the 
resultant value type.
*/
package data.type;
class DataType {
    public void type(int value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(double value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(char value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(Integer value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(Character value) {
        System.out.println(((Object)value).getClass().getName());
    }
    public void type(Double value) {
        System.out.println(((Object)value).getClass().getName());
    }
}
public class PrimitiveWithWrapperType {
    public static void main(String[] args) {
        DataType obj = new DataType();
        obj.type(100 / 24);
        obj.type(100.10 / 10);
        obj.type('Z' / 2);
        obj.type(10.5 / 0.5);
        obj.type(12.4 % 5.5);
        obj.type(100 % 56);
    }
}