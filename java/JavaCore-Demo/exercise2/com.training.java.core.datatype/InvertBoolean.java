/*
12. To invert the value of a boolean, which operator would you use?
*/

package data.type;

class InvertBoolean {
    public static void main(String[] args) {
        boolean invert = false;
        if (!invert) {
            System.out.println("Value inverted: " + invert);
            invert = !invert; // ! operator is used to invert boolean values.
            System.out.println("Value inverted: " + invert);
        }
    }
}