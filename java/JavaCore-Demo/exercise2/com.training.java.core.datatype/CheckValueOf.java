/*
13. What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))
*/

package data.type;

class CheckValueOf {
    public static void main(String[] args) {
        System.out.println(Integer.valueOf(1).getClass());
        System.out.println(Long.valueOf(1).getClass());
        System.out.println((Integer.valueOf(1)).equals(Long.valueOf(1))); // False
    }
}

//Answer
//  It results in False, because the values are same but differs in type.
//  one is of Integer class and another one Long class.