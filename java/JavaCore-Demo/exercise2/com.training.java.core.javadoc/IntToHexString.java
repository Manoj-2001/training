/*
27. What Integer method can you use to convert an int into a string that expresses the number 
  in hexadecimal?
    For example, what method converts the integer 65 into the string "41"?
*/

package java.doc;

class IntToHexString {
    public static void main(String[] args) {
        int ten = 10;
        int sixtyFive = 65;
        
        System.out.println(Integer.toHexString(ten)); // a
        System.out.println(Integer.toHexString(sixtyFive)); // 41
    }
}