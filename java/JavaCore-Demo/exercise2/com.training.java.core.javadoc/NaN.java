/*
29. What Double method can you use to detect whether a floating-point number has the special 
value Not a Number (NaN)?
*/

package java.doc;

class NaN {
    public static void main(String[] args) {
        System.out.println(Double.isNaN(1.10));
        System.out.println(Double.isNaN(10.0 / 0));
    }
}