/*
26. What methods would a class that implements the java.lang.CharSequence interface have 
to implement?
*/

package java.doc;

/*
Answer
``````
1) charAt(index)
2) length()
3) subSequence(startValue, endValue)
4) toString()

*/