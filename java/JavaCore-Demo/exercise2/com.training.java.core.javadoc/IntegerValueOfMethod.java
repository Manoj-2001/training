/*
28. What Integer method would you use to convert a string expressed in base 5 into the 
equivalent int?
    For example, how would you convert the string "230" into the integer value 65? 
Show the code you would use to accomplish this task.
*/

package java.doc;
import java.util.Scanner;
class IntegerValueOfMethod {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the string number: ");
        String value = scanner.nextLine();
        System.out.print("Enter the base value: ");
        int base = scanner.nextInt();
        System.out.println(Integer.valueOf(value, base));
    }
}