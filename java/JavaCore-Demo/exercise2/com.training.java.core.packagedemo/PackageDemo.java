/*
31. Assume you have written some classes. Belatedly, you decide they should be split into three packages, as listed in the following table.
  Furthermore, assume the classes are currently in the default package (they have no package statements).
        Package Name    Class Name
        mygame.server   Server
        mygame.shared   Utilities
        mygame.client   Client
    - Which line of code will you need to add to each source file to put each class in the right package?
    - To adhere to the directory structure, you will need to create some subdirectories in the development directory and
      put source files in the correct subdirectories. What subdirectories must you create? Which subdirectory does each source file go in?
    - Do you think you'll need to make any other changes to the source files to make them compile correctly? If so, what?
/*

Answer
``````
Which line of code will you need to add to each source file to put each class in the right package?
    The packages should be added at the beginning of the java file
    here, In Server.java -> package mygame.server;
          In Utilities.java -> package mygame.shared;
          In Client.java -> package mygame.client;

To adhere to the directory structure, you will need to create some subdirectories in the 
development directory and put source files in the correct subdirectories. What subdirectories must
you create? Which subdirectory does each source file go in?
    server, shared and client are the three subdirectories inside the directory mygame.
        mygame/server/Server.java
        mygame/shared/Utilities.java
        mygame/client/Client.java

Do you think you'll need to make any other changes to the source files to make them compile
correctly? If so, what?
    Yes, the java files should be imported inside the java file which inherits or accessing it.
    eg. import mygame.server.Client;
    if the subdirectories have multiple files that can be imported by.
    eg. import mygame.server.*;

*/