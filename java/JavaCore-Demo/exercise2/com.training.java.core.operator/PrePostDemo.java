/*
19)In the following program, explain why the value "6" is printed twice in a row:
    class PrePostDemo {
        public static void main(String[] args){
            int i = 3;
            i++;
            System.out.println(i);    // "4"
            ++i;
            System.out.println(i);    // "5"
            System.out.println(++i);  // "6"
            System.out.println(i++);  // "6"
            System.out.println(i);    // "7"
        }
    }

*/

package operator;
class PrePostDemo {
    public static void main(String[] args){
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6"
        System.out.println(i++);  // "6"
        System.out.println(i);    // "7"
    }
}
/*
//Answer

First statement, i has a value of 3.
Second Statement, i++ which means after completely executing second statement i get incremented.
Third Statement, Printing the value of i, now its 4.
Fourth Statement, ++i which means before executing the statement i get incremented now i is 5.
Fifth Statement, Printing the value of i is 5.
Sixth Statement, value of variable get incremented and then printed.
Seventh Statement, First the value printed and i get incremented.
Eighth Statement, Printing the value of i,'7'.
*/