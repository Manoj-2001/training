/*
17. compare the enum values using equal method and == operator

----------WBS----------

Requirement:
```````````
Compare the enum values using equal method and == operator

Entity:
``````
-Enum

Function Declaration:
````````````````````
public static void main(String[] args)

Jobs to be done:
```````````````
1) Create two enum classes Month and Year.
2) Compare the values using equals method, it will produce the result as true or false.
3) Compare the values using == operator, it will produce error 'incomparable types' if that values
not match.
*/

package enumeration;
public class Enum {
    public enum Month {
        MAR,
        APR,
        MAY,
        JUN,
        JUL,
        AUG
    }
    public enum Year {
        Y2018,
        Y2019,
        Y2020,
        Y2021,
        Y2022,
        Y2023
    }
    public static void main(String[] args) {
        Year year = Year.Y2020;
        Month aug = Month.AUG;
        if (year == Year.Y2020) {
            System.out.println("Year: " + year);
        }
        System.out.println(year.equals(aug)); // false
      //System.out.println(year == aug); incomparable types: Year and Month
    }
}
