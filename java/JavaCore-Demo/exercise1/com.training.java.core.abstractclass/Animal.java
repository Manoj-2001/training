/*
A concrete example of an abstract class would be a class called Animal. You see many animals 
in real life, but there are only kinds of animals. That is, you never look at something purple 
and furry and say "that is an animal and there is no more specific way of defining it". 
Instead, you see a dog or a cat or a pig... all animals. The point is, that you can never see an 
animal walking around that isn't more specifically something else (duck, pig, etc.). The Animal 
is the abstract class and Duck/Pig/Cat are all classes that derive from that base class. Animals 
might provide a function called "Age" that adds 1 year of life to the animals. It might also 
provide an abstract method called "IsDead" that, when called, will tell you if the animal has 
died. Since IsDead is abstract, each animal must implement it. So, a Cat might decide it is dead 
after it reaches 14 years of age, but a Duck might decide it dies after 5 years of age. The 
abstract class Animal provides the Age function to all classes that derive from it, but each of 
those classes has to implement IsDead on their own.
*/
package abstractclass;
abstract class Animal{
    
    int age;
    
    public abstract void isDead();
    
    public void growUp() {
        age += 1;
        System.out.println(String.format("%d years old.",age));
    }
    
    public void growUp(int passAge) {
        age += passAge;
        System.out.println(String.format("%d years old.",age));
    }
}
class Cat extends Animal {
    
    public Cat(int age) {
        this.age = age;
    }
        
    public void isDead() {
        if(age>14) {
            System.out.println("Cat died.");
        }else {
            System.out.println("Not yet died");
        }
    }
}

class Duck extends Animal {
    
    public Duck(int age) {
        this.age = age;
    }
        
    public void isDead() {
        if(age>5) {
            System.out.println("Duck died.");
        }else {
            System.out.println("Not yet died");
        }
    }
}

class Dog extends Animal {
    
    public Dog(int age) {
        this.age = age;
    }
        
    public void isDead() {
        if(age>17) {
            System.out.println("Dog died.");
        }else {
            System.out.println("Not yet died");
        }
    }
}

class Turtle extends Animal {
    
    public Turtle(int age) {
        this.age = age;
    }
        
    public void isDead() {
        if(age>80) {
            System.out.println("Turtle died.");
        }else {
            System.out.println("Not yet died");
        }
    }
}