package abstractclass;
public class MainAnimal {
    
    public static void main(String[] args) {
        
        Animal cat = new Cat(10);
        Animal duck = new Duck(1);
        Turtle turtle = new Turtle(50);
        cat.growUp();
        cat.growUp(3);
        cat.isDead();
        duck.growUp(5);
        duck.isDead();
        turtle.growUp();
        turtle.growUp(2);
        turtle.isDead();
        turtle.growUp();
    }
}