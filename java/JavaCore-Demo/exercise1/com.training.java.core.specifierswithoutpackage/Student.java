package access.specifers.without.pack;
public class Student {
    
    public String school;
    public String name;
    private int rollNumber;
    private String password;
    protected String contact;
    
    Student() {
        System.out.println("Student object created without any details!");
    }
    
    Student(int rollNumber, String name, String school, String contact) {
        this.rollNumber = rollNumber;
        this.name = name;
        this.school = school;
        this.contact = contact;
    }
    
    public String showDetails() {
        return ("Student Info:- \n"+"Roll number: "+ rollNumber +"\nName: "+ name +"\nSchool: "+ school +"\nContact number: "+contact);
    }
    
    public void login(int rollNumber, String password) {
        this.rollNumber = rollNumber;
        this.password = password;
        System.out.println(rollNumber + " logged in successfully...");
    }
    
    public static void main(String[] args) {
        
        Student student1 = new Student(61021,"Manoj.S","S.S.M.H.S School","7582378920");
        Student student2 = new Student(61022,"Hari Pranav.S","S.S.M.H.S School","8983748273");
        System.out.println(student1.showDetails());
        Student student3 = new Student();
        student2.login(61022, "Hari#123*");
    }
}