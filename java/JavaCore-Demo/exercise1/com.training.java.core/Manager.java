package com.training.java.core;

public class Manager {

    public int manId;
    public String name;
    protected char gender;
    protected String nationality;
    protected float salary;
    protected long contactNo;
    protected String email;
    
    public class Employee {
        
        public int empId;
        public String name;
        char gender;
        String nationality;
        float salary;
        long contactNo;
        String email;
        
        public void showDetails() {
            System.out.println(String.format("EmployeeId: %d\nName: %s\nSalary: %.2f\nContact: %d", empId, name, salary, contactNo));
        }
    }

    {
        nationality = "Indian";
        salary = 50000;
    }

    public void showDetails() {
        System.out.println(String.format("ManagerId: %d\nName: %s\nSalary: %.2f\nContact: %d", manId, name, salary, contactNo));
    }
//    public void empAddress(String dNo, String street, String city, int pin) {
//        
//    }
}
