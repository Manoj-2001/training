package control.statements;
public class BreakAndContinue {
    public static void main(String[] args) {
        System.out.println("Using Break Statement");
        for(int i = 1; i <= 10; i++) {
            if(i == 5) break;
            System.out.println(i);
        }
        
        System.out.println("Using Continue Statement");
        for(int i = 1; i <= 10; i++) {
            if(i == 5) continue;
            System.out.println(i);
        }
    }
}
