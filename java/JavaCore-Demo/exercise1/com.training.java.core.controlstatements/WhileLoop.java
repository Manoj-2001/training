package control.statements;
class WhileLoop {
    public static void main(String[] args) {
        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
        int i=0;
        while(i < cars.length) {
            System.out.println("Car "+ (i+1) +": " + cars[i]);
            i++;
        }
    }
}