package control.statements;
import java.util.Scanner;
public class SwitchDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a special character: ");
        char specialCharacter = scanner.next().charAt(0);
        
        switch (specialCharacter) {
            case '!':
                System.out.println("Exclamation");
                break;
            case '@':
                System.out.println("At");
                break;
            case '#':
                System.out.println("Hash");
                break;
            case '$':
                System.out.println("Dollar");
                break;
            case '%':
                System.out.println("Percent");
                break;
            case '^':
                System.out.println("Caret");
                break;
            case '&':
                System.out.println("Ampersand");
                break;
            case '~':
                System.out.println("Tilde");
                break;
            default:
                System.out.println("Not Specified");
                break;
        }
    }
}