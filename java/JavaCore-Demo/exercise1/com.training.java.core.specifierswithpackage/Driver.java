package access.specifiers.with.pack;

import com.training.java.core.Manager;

public class Driver {
    public static void main(String[] args) {
        Manager chris = new Manager();
        chris.manId = 8003;
        chris.name = "Chris Hemsworth";
        Manager.Employee james = chris.new Employee();
        james.empId = 8016;
        james.name = "James Cameron";
//        james.salary = 43000;
        chris.showDetails();
        james.showDetails();
    }
}
