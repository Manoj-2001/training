package interfacedemo;
interface Atm {
    public abstract void withdrawMoney(int amount);
    public abstract void depositMoney(int amount);
    public abstract void balanceEnquiry(int pin);
}

class Bank implements Atm {
    private int amt;
    private String accountType;
    private int pin;
    
    public Bank(String accountType, int pin) {
        this.accountType = accountType;
        this.pin = pin;
    }
    
    public void withdrawMoney(int amount) {
        if(amt >= amount) {
            amt = amt - amount;
        }else {
            System.out.println("You have not enough money to withdraw.");
        }
    }
    
    public void depositMoney(int amount) {
        amt = amt + amount;
    }
    
    public void balanceEnquiry(int pin) {
        if(this.pin == pin) {
            System.out.println("Your "+ accountType +" has balance of "+ amt +" rupees.");
        }else {
            System.out.println("Check you entered the pin correctly.");
        }
    }
}
