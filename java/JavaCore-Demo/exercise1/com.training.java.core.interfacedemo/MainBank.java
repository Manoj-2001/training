package interfacedemo;
class MainBank {
    public static void main(String[] args) {
        Bank cub = new Bank("Savings Account",1923);
        Bank canara = new Bank("Savings Account",2103);
        cub.depositMoney(15500);
        cub.balanceEnquiry(1923);
        cub.withdrawMoney(10000);
        cub.balanceEnquiry(1923);
        cub.withdrawMoney(8000);
        canara.balanceEnquiry(2100);
        canara.balanceEnquiry(2103);
    }
}
