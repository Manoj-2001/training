package exception.handling;
class ThrowAndThrowsDemo {
    public static void main(String[] args) throws ArithmeticException{
        try {
            int number1 = 0;
            int number2 = 0;
            int result = number1 / number2;
            System.out.println(result);
        }
        catch (ArithmeticException e) {
            System.out.println("Arithmetic exception from try block");
            throw e;
        }
        catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }
}