package in.kpr.training.jdbc.service;

import java.sql.SQLException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.model.Address;

public class AddressServiceTest {

    //positive scenarios
    @Test(priority = 2)
    public void createAddressTest() throws SQLException, AppException {
        Address address = new Address("K.C Street", "Thiruvarur", 614001);
        AddressService addressService = new AddressService();
        long id = addressService.create(address);
        System.out.println(id);
        Assert.assertTrue(id > 0);
    }

    @Test(priority = 4)
    public void readAddressTest() throws SQLException {
        AddressService addressService = new AddressService();
        Address address = addressService.read(12);
        Address expectedAddress = new Address(12, "K.V Street", "Park Town", 600003);
        Assert.assertTrue(address.getStreet().equals(expectedAddress.getStreet())
                && address.getCity().equals(expectedAddress.getCity()));
    }

    @Test(priority = 5)
    public void readAllAddressTest() throws SQLException {
        AddressService addressService = new AddressService();
        List<Address> addresses = addressService.readAll();
        Assert.assertTrue(addresses.size() > 0);
    }

    @Test(priority = 7)
    public void updateAddressTest() throws AppException, SQLException {
        Address address = new Address(17, "5th Street", "Mylapore", 600004);
        AddressService addressService = new AddressService();
        addressService.update(address);
        Address updatedAddress = addressService.read(17);
        Assert.assertEquals(address.getStreet(), updatedAddress.getStreet());
    }

    @Test(priority = 9)
    public void deleteAddressTest() throws SQLException, AppException {
        AddressService addressService = new AddressService();
        addressService.delete(56);
        Address deletedAddress = addressService.read(54);
        Assert.assertNull(deletedAddress);
    }

    //negative scenarios
    @Test(priority = 1)
    public void createAddressWithoutPostalCodeTest() throws SQLException, AppException {
        Address address = new Address("M.M Street", "Thiruvarur", 0);
        AddressService addressService = new AddressService();
        Assert.assertThrows(() -> addressService.create(address));
    }

    @Test(priority = 3)
    public void readAddressWithUnknownIdTest() throws SQLException {
        AddressService addressService = new AddressService();
        Address address = addressService.read(10);
        Assert.assertNull(address);
    }

    @Test(priority = 6)
    public void updateAddressWithUnknownIdTest() throws AppException {
        Address address = new Address(10, "5th Street", "Madurai", 610012);
        AddressService addressService = new AddressService();
        Assert.assertThrows(() -> addressService.update(address));
    }

    @Test(priority = 8)
    public void deleteAddressWithUnknownIdTest() throws AppException {
        AddressService addressService = new AddressService();
        Assert.assertThrows(() -> addressService.delete(10));
    }

}
