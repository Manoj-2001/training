package in.kpr.training.jdbc.service;

import java.sql.Date;
import java.sql.SQLException;

import org.testng.Assert;
import org.testng.annotations.Test;

import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.model.Address;
import in.kpr.training.jdbc.model.Person;

public class PersonServiceTest {

    //positive scenarios
    @Test(priority = 3)
    public void createPersonWithExistingAddressTest() throws SQLException, AppException {
        Address address = new Address("Church Road", "Vepery", 600007);
        Person person = new Person("Manoj Kumar.R", "manojk.r@gmail.com", address,
                Date.valueOf("2001-08-23"));
        PersonService personService = new PersonService();
        long personId = personService.create(person);
        Person readedPerson = personService.read(personId, false);
        Assert.assertTrue(personId > 0 && readedPerson.getAddressId() == 16);
    }

    @Test(priority = 4)
    public void createPersonWithNewAddressTest() throws SQLException, AppException {
        Address address = new Address("K.R.V Street", "Thiruvarur", 614001);
        Person person = new Person("Abi.G", "abishek33@gmail.com", address,
                Date.valueOf("2001-06-17"));
        PersonService personService = new PersonService();
        long personId = personService.create(person);
        Assert.assertTrue(personId > 0);
    }

    @Test(priority = 6)
    public void readPersonWithoutAddressTest() throws SQLException {
        PersonService personService = new PersonService();
        Person person = personService.read(14, false);
        Person expectedPerson = new Person(14, "Ganesh.G", "ganesh45@gmail.com", 17,
                Date.valueOf("2000-01-23"), Date.valueOf("2020-11-05"));
        Assert.assertEquals(person.getEmail(), expectedPerson.getEmail());
    }

    @Test(priority = 7)
    public void readPersonWithAddressTest() throws SQLException {
        PersonService personService = new PersonService();
        Person person = personService.read(14, true);
        Address address = new Address(17, "4th Street", "Mylapore", 600004);
        Person expectedPerson = new Person(14, "Ganesh.G", "ganesh45@gmail.com",
                address, Date.valueOf("2000-01-23"), Date.valueOf("2020-11-05"));
        Assert.assertEquals(person.getAddress().getStreet(),
                expectedPerson.getAddress().getStreet());
    }

    @Test(priority = 9)
    public void updatePersonTest() throws AppException, SQLException {
        Person person = new Person(15, "Shankar.S", "shankar01@gmail.com", 17,
                Date.valueOf("2001-01-01"));
        PersonService personService = new PersonService();
        personService.update(person);
        Person updatedPerson = personService.read(15, false);
        Assert.assertEquals(person.getAddressId(), updatedPerson.getAddressId());
    }

    @Test(priority = 11)
    public void deletePersonTest() throws AppException, SQLException {
        PersonService personService = new PersonService();
        personService.delete(24);
        Person deletedPerson = personService.read(25, false);
        Assert.assertNull(deletedPerson);
    }

    //negative scenarios
    @Test(priority = 1)
    public void createPersonWithDuplicateEmailTest() throws SQLException, AppException {
        Address address = new Address("Church Road", "Vepery", 600007);
        Person person = new Person("Manoj Kumar.R", "manoj21@gmail.com", address,
                Date.valueOf("2001-08-23"));
        PersonService personService = new PersonService();
        Assert.assertThrows(() -> personService.create(person));
    }

    @Test(priority = 2)
    public void createPersonWithEmptyPostalCodeTest() throws SQLException, AppException {
        Address address = new Address("Church Road", "Vepery", 0);
        Person person = new Person("Manoj Kumar.R", "manojk.r@gmail.com", address,
                Date.valueOf("2001-08-23"));
        PersonService personService = new PersonService();
        Assert.assertThrows(() -> personService.create(person));
    }

    @Test(priority = 5)
    public void readPersonWithUnknownIdTest() throws SQLException {
        PersonService personService = new PersonService();
        Person person = personService.read(40, true);
        Assert.assertNull(person);
    }

    @Test(priority = 8)
    public void updatePersonWithUnknownIdTest() throws AppException {
        Person person = new Person(55, "Sandeep.S", "sandeep.s@gmail.com", 17,
                Date.valueOf("2001-04-10"));
        PersonService personService = new PersonService();
        Assert.assertThrows(() -> personService.update(person));
    }

    @Test(priority = 10)
    public void deletePersonWithUnknownIdTest() throws AppException {
        PersonService personService = new PersonService();
        Assert.assertThrows(() -> personService.delete(40));
    }

}
