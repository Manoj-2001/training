package in.kpr.training.jdbc.constant;

public class QueryStatement {

    public static final String INSERT_ADDRESS = new StringBuilder()
            .append("INSERT INTO address (street       ")
            .append("                    ,city         ")
            .append("                    ,postal_code) ")
            .append("VALUES (?, ?, ?)                  ")
            .toString();

    public static final String READ_ADDRESS = new StringBuilder()
            .append("SELECT id          ")
            .append("      ,street      ")
            .append("      ,city        ")
            .append("      ,postal_code ")
            .append("  FROM address     ")
            .append(" WHERE id = ?      ")
            .toString();

    public static final String READ_ALL_ADDRESS = new StringBuilder()
            .append("SELECT id          ")
            .append("      ,street      ")
            .append("      ,city        ")
            .append("      ,postal_code ")
            .append("  FROM address     ")
            .toString();

    public static final String UPDATE_ADDRESS = new StringBuilder()
            .append("UPDATE address         ")
            .append("   SET street = ?      ")
            .append("      ,city = ?        ")
            .append("      ,postal_code = ? ")
            .append(" WHERE id = ?          ")
            .toString();

    public static final String DELETE_ADDRESS = new StringBuilder()
            .append("DELETE FROM address ")
            .append(" WHERE id = ?       ")
            .toString();

    public static final String INSERT_PERSON = new StringBuilder()
            .append("INSERT INTO person (name        ")
            .append("                   ,email       ")
            .append("                   ,address_id  ")
            .append("                   ,birth_date) ")
            .append("VALUES (?, ?, ?, ?)             ")
            .toString();

    public static final String READ_PERSON = new StringBuilder()
            .append("SELECT id           ")
            .append("      ,name         ")
            .append("      ,email        ")
            .append("      ,address_id   ")
            .append("      ,birth_date   ")
            .append("      ,created_date ")
            .append("  FROM person       ")
            .append(" WHERE id = ?       ")
            .toString();

    public static final String READ_ALL_PERSON = new StringBuilder()
            .append("SELECT id                             ")
            .append("      ,street                         ")
            .append("      ,city                           ")
            .append("      ,postal_code                    ")
            .append("      ,person.id                      ")
            .append("      ,name                           ")
            .append("      ,email                          ")
            .append("      ,address_id                     ")
            .append("      ,birth_date                     ")
            .append("      ,created_date                   ")
            .append("  FROM person                         ")
            .append(" INNER JOIN address                   ")
            .append("    ON person.address_id = address.id ")
            .toString();

    public static final String UPDATE_PERSON = new StringBuilder()
            .append("UPDATE person         ")
            .append("   SET name = ?       ")
            .append("      ,email = ?      ")
            .append("      ,address_id = ? ")
            .append("      ,birth_date = ? ")
            .append(" WHERE id = ?         ")
            .toString();

    public static final String DELETE_PERSON = new StringBuilder()
            .append("DELETE FROM person ")
            .append(" WHERE id = ?      ")
            .toString();

    public static final String GET_ID_FOR_EMAIL = new StringBuilder()
            .append("SELECT id        ")
            .append("  FROM person    ")
            .append(" WHERE email = ? ")
            .append("HAVING id != ?   ")
            .toString();

    public static final String GET_ID_FOR_NAME = new StringBuilder()
            .append("SELECT id       ")
            .append("  FROM person   ")
            .append(" WHERE name = ? ")
            .append("HAVING id != ?  ")
            .toString();

    public static final String GET_ID_FOR_ADDRESS = new StringBuilder()
            .append("SELECT id              ")
            .append("  FROM address         ")
            .append(" WHERE street = ?      ")
            .append("   AND city = ?        ")
            .append("   AND postal_code = ? ")
            .toString();

    public static final String GET_ADDRESS_ID_COUNT = new StringBuilder()
            .append("SELECT COUNT(id) AS id_count           ")
            .append("                   ,address_id         ")
            .append("  FROM person                          ")
            .append(" WHERE address_id = (SELECT address_id ")
            .append("                       FROM person     ")
            .append("                      WHERE id = ?)    ")
            .toString();

    public static final String SEARCH_ADDRESS = new StringBuilder()
            .append("SELECT id                                 ")
            .append("      ,street                             ")
            .append("      ,city                               ")
            .append("      ,postal_code                        ")
            .append("  FROM address                            ")
            .append(" WHERE street LIKE CONCAT('%',?,'%')      ")
            .append("   AND city LIKE CONCAT('%',?,'%')        ")
            .append("   AND postal_code LIKE CONCAT('%',?,'%') ")
            .toString();

}
