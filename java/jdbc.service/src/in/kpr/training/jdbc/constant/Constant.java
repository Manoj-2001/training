package in.kpr.training.jdbc.constant;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import in.kpr.training.jdbc.model.Address;
import in.kpr.training.jdbc.model.Person;

public class Constant {

    public static final int MAX_THREADS          = 3;
    public static final String ID                = "id";
    public static final String ID_COUNT          = "id_count";
    public static final String GENERATED_ID      = "generated_key";
    public static final String STREET            = "street";
    public static final String CITY              = "city";
    public static final String POSTAL_CODE       = "postal_code";
    public static final String NAME              = "name";
    public static final String EMAIL             = "email";
    public static final String ADDRESS_ID        = "address_id";
    public static final String BIRTH_DATE        = "birth_date";
    public static final String CREATED_DATE      = "created_date";
    public static final String URL               = "url";
    public static final String USER              = "user";
    public static final String PASSWORD          = "password";
    public static final String POSTAL_CODE_REGEX = "^([0-9]+)(?![a-z])$";
    public static final String CITY_REGEX        = "^([a-zA-Z]+)(?![ ])$";
    public static final String STREET_REGEX      = new StringBuilder()
            .append("(?=([0-9].+[a-zA-Z])|.*( Street| Road| Highway| Avenue))[a-zA-Z0-9. ]+")
            .toString();

    public static Address getAddressFromResultSet(ResultSet rs) throws SQLException {
        return new Address(rs.getLong(ID),
                           rs.getString(STREET),
                           rs.getString(CITY),
                           rs.getInt(POSTAL_CODE));
    }

    public static void setAddressToPreparedStatement(PreparedStatement ps,
            Address address) throws SQLException {
        ps.setString(1, address.getStreet());
        ps.setString(2, address.getCity());
        ps.setInt(3, address.getPostalCode());
    }

    public static Person getPersonFromResultSet(ResultSet rs) throws SQLException {
        return new Person(rs.getLong(ID),
                          rs.getString(NAME),
                          rs.getString(EMAIL),
                          rs.getLong(ADDRESS_ID),
                          rs.getDate(BIRTH_DATE),
                          rs.getDate(CREATED_DATE));
    }

    public static Person getPersonFromResultSet(ResultSet rs,
            Address address) throws SQLException {
        return new Person(rs.getLong(ID),
                          rs.getString(NAME),
                          rs.getString(EMAIL),
                          address, 
                          rs.getDate(BIRTH_DATE),
                          rs.getDate(CREATED_DATE));
    }

    public static void setPersonToPreparedStatement(PreparedStatement ps, long addressId,
            Person person) throws SQLException {
        ps.setString(1, person.getName());
        ps.setString(2, person.getEmail());
        ps.setLong(3, addressId);
        ps.setDate(4, person.getBirthDate());
    }

}
