/*

----------WBS----------

Entity:
``````
-ConnectionService
-Constant

Jobs to be done:
```````````````
Create a method initConnection:
1) Using getResourceAsStream and classLoader to get the data from db properties file
as stream.
2) Store the stream of data to InputStream object.
3) Create a new properties file db.
4) Load the date from stream to db file.
5) Get the url, user, and password from db file
6) Create connection using DriverManager.
7) Return the connection object.

Create a method releaseConnection:
1) Close the connection

Create a method commit:
1) Commit the changes using connection.

Create a method rollback:
1) To rollback the changes

Create a method disableAutoCommit:
1) To set the auto commit as false.

Pseudo Code:
````````````
public class ConnectionService {

    private Connection connection;

    public Connection initConnection() {
        try {
            Properties db = new Properties();
            InputStream stream = getClass()
                    .getClassLoader().getResourceAsStream(#properties file);
            db.load(stream);

            String url = db.getProperty(Constant.URL);
            String user = db.getProperty(Constant.USER);
            String password = db.getProperty(Constant.PASSWORD);

            connection = DriverManager.getConnection(url, user, password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return connection;
    }

    public void releaseConnection() {
        connection.close();
    }

    public void commit() {
        connection.commit();
    }

    public void rollback() {
        connection.rollback();
    }

    public void disableAutoCommit() {
        connection.setAutoCommit(false);
    }

}

*/

package in.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import in.kpr.training.jdbc.constant.Constant;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;

//public class ConnectionService extends ThreadLocal<Connection> {
//    private static ConnectionService connectionService;
//
//    private ConnectionService() throws AppException {
//        initConnection();
//    }
//
//    public static ConnectionService initializeService() throws AppException {
//        if (connectionService == null) {
//            connectionService = new ConnectionService();
//        }
//        return connectionService;
//    }
//
//    private void initConnection() throws AppException {
//        try {
//            Properties db = new Properties();
//            db.load(ConnectionService.class.getClassLoader().getResourceAsStream("db.properties"));
//
//            String url = db.getProperty(Constant.URL);
//            String user = db.getProperty(Constant.USER);
//            String password = db.getProperty(Constant.PASSWORD);
//
//            super.set(DriverManager.getConnection(url, user, password));
//        } catch (Exception ex) {
//            throw new AppException(ErrorCode.E311, ex);
//        }
//    }
//
//    public Connection getConnection() {
//        return super.get();
//    }
//
//    public static ConnectionService getConnectionService() {
//        return ConnectionService.connectionService;
//    }
//
//    public void releaseConnection() {
//        try {
//            super.remove();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void commit() {
//        try {
//            if (! getConnection().getAutoCommit()) {
//                getConnection().commit();
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void rollback() {
//        try {
//            getConnection().rollback();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void disableAutoCommit() {
//        try {
//            getConnection().setAutoCommit(false);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//}

public class ConnectionService {

    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    public ConnectionService() throws AppException {
        initConnection();
    }

    private void initConnection() throws AppException {
        try {
            Properties db = new Properties();
            db.load(ConnectionService.class.getClassLoader().getResourceAsStream("db.properties"));

            String url = db.getProperty(Constant.URL);
            String user = db.getProperty(Constant.USER);
            String password = db.getProperty(Constant.PASSWORD);

            threadLocal.set(DriverManager.getConnection(url, user, password));
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E311, ex);
        }
    }

    public static Connection getConnection() {
        return threadLocal.get();
    }

    public void releaseConnection() {
        try {
            threadLocal.remove();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void commit() {
        try {
            if (! getConnection().getAutoCommit()) {
                getConnection().commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void rollback() {
        try {
            getConnection().rollback();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void disableAutoCommit() {
        try {
            getConnection().setAutoCommit(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
