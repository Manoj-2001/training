/*
Perform CRUD services in PersonService with Cascading

----------WBS----------

Entity:
``````
-PersonService
-Constant
-QueryStatement
-AppException
-Person
-PersonValidator

Method Signature:
````````````````
-public static long create(Person person)
-public static void read(long id)
-public static void readAll()
-public static boolean update(Person person)
-public static boolean delete(long id)

Jobs to be done:
```````````````
Create Person :
1) Create a method create with Person object as parameter
2) Verify the postal code using verifyPostalCode method of AddressValidator
3) Establish connection using initConnection method of ConnectionService.
4) Invoke the checkEmailIsUnique method of PersonValidator with person and connection object.
5) Invoke the isPresent method of AddressValidator with address and connection object. And store
the returned id.
6) Check the returned id is zero.
    6.1) Invoke disableAutoCommit method of ConnectionService.
    6.1) Create object addressService of AddressService.
    6.2) Invoke the create method using addressService. And store the returned id.
7) Invoke the prepareStatement method with INSERT_PERSON query using con
8) Set values to PreparedStatement ps
9) Check executeUpdate is 1 and generatedKeys is not 0
    9.1) Invoke commit method of ConnectionService.
    9.2) Return personId.
10) If exception occurs, invoke the rollback method of ConnectionService.

Read Person:
1) Create a method read with id and boolean value includeAddress as parameter.
2) Establish connection using initConnection method of ConnectionService.
3) Invoke the prepareStatement method with READ_PERSON query using con
4) Set values to PreparedStatement ps
5) Perform executeQuery and get the ResultSet rs
6) Check the includeAddress is true.
    6.1) Parse the values from rs to person object with address
    6.2) return person
7) Parse the values from rs to person without address
8) Return person.

Read all Person:
1) Establish connection using initConnection method of ConnectionService.
2) Invoke the prepareStatement method with READ_ALL_PERSON query using con
3) Perform executeQuery and get the ResultSet rs
5) Using while loop, check rs has any values using next method
    5.1) Get values from rs and parse into Person object person
    5.2) Add the person to a list persons.
6) Check If list has any person
    6.1) Return persons list
7) Return null.

Update Person:
1) Create a method update with Person object as parameter
2) Establish connection using initConnection method of ConnectionService.
3) Invoke checkEmailIsUnique method of PersonService
4) Invoke the prepareStatement method with UPDATE_PERSON query using con
5) Set values to PreparedStatement ps
6) Check executeUpdate is not 1
    6.1) Throw an AppException with error code.

Delete Person:
1) Create a method delete with id as parameter
2) Establish connection using initConnection method of ConnectionService.
3) Invoke the prepareStatement method with DELETE_PERSON query using con
4) Set values to PreparedStatement ps
5) Check executeUpdate is not 1
    6.1) Throw an AppException with error code.

Pseudo Code:
```````````
public class PersonService {

    public static long create(Person person) throws SQLException, AppException {

        AddressValidator.verifyPostalCode(person.getAddress());

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            PreparedStatement ps;
            long addressId;

            PersonValidator.checkEmailIsUnique(person, con);
            addressId = AddressValidator.isPresent(person.getAddress(), con);

            // if address id is 0, create a address using AddressService create method
            if (addressId == 0) {
                connectionService.disableAutoCommit();
                AddressService addressService = new AddressService();
                addressId = addressService.create(person.getAddress());
            }

            // prepare statement using INSERT_PERSON query
            // set values to PreparedStatement ps

            long personId;
            if (ps.executeUpdate() == 1 
                    && (personId = ps.getGeneratedKeys().getLong(Constant.ID)) != 0) {
                connectionService.commit();
                return personId;
            } else {
                throw new AppException(305);
            }
        }
    }

    public Person read(long id, boolean includeAddress) throws SQLException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using READ_PERSON query
            // set values to PreparedStatement ps
            ResultSet rs = ps.executeQuery();

            if (! rs.next()) {
                return null;
            }

            if (! includeAddress) {
                Person person = new Person(id, name, email, addressId, birthDate, createdDate);
                return person;
            }

            Address address = new Address(addressId, street, city, postalCode);
            Person person = new Person(id, name, email, address, birthDate, createdDate);
            return person;
        }
    }

    public List<Person> readAll() throws SQLException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using READ_ALL_PERSON query
            ResultSet rs = ps.executeQuery();
            List<Person> persons = new ArrayList<>();

            while (rs.next()) {
                persons.add(new Person(id, name, email, addressId, birthDate, createdDate));
            }
            return persons.size() > 0 ? persons : null;
        }
    }

    public void update(Person person) throws AppException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            PersonValidator.checkEmailIsUnique(person, con);

            // prepare statement using UPDATE_PERSON query
            // set values to PreparedStatement ps

            if (ps.executeUpdate() != 1) {
                throw new AppException(307);
            }
        }
    }

    public void delete(long id) throws AppException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using DELETE_PERSON query
            // set values to PreparedStatement ps

            if (ps.executeUpdate() != 1) {
                throw new AppException(308);
            }
        }
    }

}
*/

package in.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import in.kpr.training.jdbc.constant.Constant;
import in.kpr.training.jdbc.constant.QueryStatement;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;
import in.kpr.training.jdbc.model.Address;
import in.kpr.training.jdbc.model.Person;
import in.kpr.training.jdbc.validation.PersonValidator;

public class PersonService {

    public void checkNameIsUnique(Person person, Connection con) {
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.GET_ID_FOR_NAME)) {
            ps.setString(1, person.getName());
            ps.setLong(2, person.getId());

            if (ps.executeQuery().next()) {
                throw new AppException(ErrorCode.E316);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void checkEmailIsUnique(Person person, Connection con) {
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.GET_ID_FOR_EMAIL)) {
            ps.setString(1, person.getEmail());
            ps.setLong(2, person.getId());

            if (ps.executeQuery().next()) {
                throw new AppException(ErrorCode.E312);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public long create(Person person) {

        PersonValidator.validatePerson(person);

        try (Connection con = ConnectionService.getConnection()) {

            checkNameIsUnique(person, con);
            checkEmailIsUnique(person, con);
            AddressService addressService = new AddressService();
            long addressId = addressService.isPresent(person.getAddress(), con);

            PreparedStatement ps;
            ps = con.prepareStatement(QueryStatement.INSERT_PERSON,
                    Statement.RETURN_GENERATED_KEYS);
            Constant.setPersonToPreparedStatement(ps, addressId, person);

            ResultSet rs;
            if (ps.executeUpdate() == 1 && (rs = ps.getGeneratedKeys()).next()) {
                ConnectionService.commit();
                return rs.getLong(Constant.GENERATED_ID);
            }
        } catch (Exception ex) {
            ConnectionService.rollback();
            throw new AppException(ErrorCode.E306, ex);
        }
        return 0;
    }

    public Person read(long id, boolean includeAddress) {

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.READ_PERSON);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (!rs.next()) {
                return null;
            }
            if (!includeAddress) {
                return Constant.getPersonFromResultSet(rs);
            }
            AddressService addressService = new AddressService();
            Address address = addressService.read(rs.getLong(Constant.ADDRESS_ID));
            return Constant.getPersonFromResultSet(rs, address);

        } catch (Exception ex) {
            throw new AppException(ErrorCode.E307, ex);
        }
    }

    public List<Person> readAll() {

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.READ_ALL_PERSON);
            ResultSet rs = ps.executeQuery();
            List<Person> persons = new ArrayList<>();

            while (rs.next()) {
                Address address = Constant.getAddressFromResultSet(rs);
                persons.add(Constant.getPersonFromResultSet(rs, address));
            }
            return persons.size() > 0 ? persons : null;
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E308, ex);
        }
    }

    public void update(Person person) {

        PersonValidator.validatePerson(person);

        try (Connection con = ConnectionService.getConnection()) {

            checkEmailIsUnique(person, con);
            checkNameIsUnique(person, con);
            AddressService addressService = new AddressService();
            long addressId = addressService.isPresent(person.getAddress(), con);

            PreparedStatement ps = con.prepareStatement(QueryStatement.UPDATE_PERSON);
            Constant.setPersonToPreparedStatement(ps, addressId, person);
            ps.setLong(5, person.getId());

            if (ps.executeUpdate() != 1) {
                throw new AppException(ErrorCode.E319);
            }
            ConnectionService.commit();
        } catch (Exception ex) {
            ConnectionService.rollback();
            throw new AppException(ErrorCode.E309, ex);
        }
    }

    public void delete(long id) {

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.GET_ADDRESS_ID_COUNT);
            ps.setLong(1, id);

            ResultSet rs;
            (rs = ps.executeQuery()).next();

            int count = rs.getInt(Constant.ID_COUNT);
            long addressId = rs.getLong(Constant.ADDRESS_ID);

            if (count == 1) {
                AddressService addressService = new AddressService();
                addressService.delete(addressId);
            } else {
                ps = con.prepareStatement(QueryStatement.DELETE_PERSON);
                ps.setLong(1, id);
                if (ps.executeUpdate() != 1) {
                    throw new AppException(ErrorCode.E319);
                }
            }
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E310, ex);
        }
    }

}
