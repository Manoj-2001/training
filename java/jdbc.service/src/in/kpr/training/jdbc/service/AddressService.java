/*
Perform CRUD services in AddressService

----------WBS----------

Entity:
``````
-AddressService
-Constant
-QueryStatement
-AppException
-Address
-AddressValidator

Method Signature:
````````````````
-public static long create(Address address)
-public static void read(long id)
-public static void readAll()
-public static boolean update(Address address)
-public static boolean delete(long id)

Jobs to be done:
```````````````
Create Address:
1) Create a method create with Address object as parameter
2) Verify the postal code using verifyPostalCode method of AddressValidator
3) Establish connection using initConnection method of ConnectionService.
4) Invoke the prepareStatement method with INSERT_ADDRESS query using con
5) Set values to PreparedStatement ps
6) Check executeUpdate is 1 and generatedKeys is not 0
    6.1) Return addressId.


Read Address:
1) Create a method read with id as parameter.
2) Establish connection using initConnection method of ConnectionService.
3) Invoke the prepareStatement method with READ_ADDRESS query using con
4) Set values to PreparedStatement ps
5) Perform executeQuery and get the ResultSet rs
6) Check rs has any values using next method
    6.1) If not return null.
7) Get street, city, postal code and parse into Address object
8) Return address.

Read all Address:
1) Establish connection using initConnection method of ConnectionService.
2) Invoke the prepareStatement method with READ_ALL_ADDRESS query using con
3) Perform executeQuery and get the ResultSet rs
5) Using while loop, check rs has any values using next method
    5.1) Get street, city, postal code and parse into Address object
    5.2) Add the address to a list addresses.
6) Check If list has address
    6.1) Return addresses list
7) Return null.

Update Address:
1) Create a method update with Address object as parameter
2) Verify the postal code using verifyPostalCode method of AddressValidator
3) Establish connection using initConnection method of ConnectionService.
4) Invoke the prepareStatement method with UPDATE_ADDRESS query using con
5) Set values to PreparedStatement ps
6) Check executeUpdate is not 1
    6.1) Throw an AppException with error code.

Delete Address:
1) Create a method delete with id as parameter
2) Establish connection using initConnection method of ConnectionService.
3) Invoke the prepareStatement method with DELETE_ADDRESS query using con
4) Set values to PreparedStatement ps
5) Check executeUpdate is not 1
    6.1) Throw an AppException with error code.

Pseudo Code:
```````````
public class AddressService {

    public long create(Address address) throws SQLException, AppException {

        AddressValidator.verifyPostalCode(address);

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using INSERT_ADDRESS query

            // set values to PreparedStatement ps

            long addressId;
            if (ps.executeUpdate() == 1 && (addressId = ps.getGeneratedKeys() != 0) {
                return addressId;
            } else {
                throw new AppException(301);
            }
        }
    }

    public Address read(long id) throws SQLException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using READ_ADDRESS query
            // set values to PreparedStatement ps

            ResultSet rs = ps.executeQuery();

            if (! rs.next()) {
                return null;
            }

            Address address = new Address(id, street, city, postalCode);
            return address;
        }
    }

    public List<Address> readAll() throws SQLException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using READ_ALL_ADDRESS query

            ResultSet rs = ps.executeQuery();
            List<Address> addresses = new ArrayList<>();

            while (rs.next()) {
                addresses.add(new Address(id, street, city, postalCode));
            }
            return addresses.size() > 0 ? addresses : null;
        }
    }

    public void update(Address address) throws AppException {

        AddressValidator.verifyPostalCode(address);

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using UPDATE_ADDRESS query
            // set values to PreparedStatement ps

            if (ps.executeUpdate() != 1 ) {
                throw new AppException(303);
            }
        }
    }

    public void delete(long id) throws AppException {

        ConnectionService connectionService = new ConnectionService();
        try (Connection con = connectionService.initConnection()) {

            // prepare statement using UPDATE_ADDRESS query
            // set values to PreparedStatement ps

            if (ps.executeUpdate() != 1) {
                throw new AppException(304);
            }
        }
    }

}
*/

package in.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import in.kpr.training.jdbc.constant.Constant;
import in.kpr.training.jdbc.constant.QueryStatement;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;
import in.kpr.training.jdbc.model.Address;
import in.kpr.training.jdbc.validation.AddressValidator;

class AddressService {

    public long isPresent(Address address, Connection con) throws SQLException {
        ResultSet rs = null;
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.GET_ID_FOR_ADDRESS)) {

            Constant.setAddressToPreparedStatement(ps, address);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong(Constant.ID);
            }

            ConnectionService.disableAutoCommit();
            return create(address);
        } finally {
            rs.close();
        }
    }

    public long create(Address address) {

        AddressValidator.validateAddress(address);

        try (Connection con = ConnectionService.getConnection();
                PreparedStatement ps = con.prepareStatement(QueryStatement.INSERT_ADDRESS,
                        Statement.RETURN_GENERATED_KEYS)) {

            Constant.setAddressToPreparedStatement(ps, address);

            ResultSet rs;
            if (ps.executeUpdate() == 1 && (rs = ps.getGeneratedKeys()).next()) {
                return rs.getLong(Constant.GENERATED_ID);
            }
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E301, ex);
        }
        return 0;
    }

    public Address read(long id) {

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.READ_ADDRESS);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (! rs.next()) {
                return null;
            }
            return Constant.getAddressFromResultSet(rs);
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E302, ex);
        }
    }

    public List<Address> readAll() {

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.READ_ALL_ADDRESS);
            ResultSet rs = ps.executeQuery();
            List<Address> addresses = new ArrayList<>();

            while (rs.next()) {
                addresses.add(Constant.getAddressFromResultSet(rs));
            }
            return addresses.size() > 0 ? addresses : null;
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E303, ex);
        }
    }

    public void update(Address address) {

        AddressValidator.validateAddress(address);

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.UPDATE_ADDRESS);
            Constant.setAddressToPreparedStatement(ps, address);
            ps.setLong(4, address.getId());

            if (ps.executeUpdate() != 1) {
                throw new AppException(ErrorCode.E319);
            }
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E304, ex);
        }
    }

    public void delete(long id) {

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.DELETE_ADDRESS);
            ps.setLong(1, id);

            if (ps.executeUpdate() != 1) {
                throw new AppException(ErrorCode.E319);
            }
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E305, ex);
        }
    }

    public List<Address> search(String text) {

        if (text.isEmpty()) {
            return null;
        }

        StringBuilder street = new StringBuilder(""),
                        city = new StringBuilder(""),
                  postalCode = new StringBuilder("");

        String[] words = text.split(", ");
        for (String word : words) {
            if (Pattern.matches(Constant.POSTAL_CODE_REGEX, word)) {
                postalCode.append(word);
            } else if (Pattern.matches(Constant.STREET_REGEX, word)) {
                street.append(word);
            } else if (Pattern.matches(Constant.CITY_REGEX, word)) {
                city.append(word);
            }
        }

        try (Connection con = ConnectionService.getConnection()) {

            PreparedStatement ps = con.prepareStatement(QueryStatement.SEARCH_ADDRESS);
            ps.setString(1, street.toString());
            ps.setString(2, city.toString());
            ps.setString(3, postalCode.toString());

            ResultSet rs = ps.executeQuery();
            List<Address> addresses = new ArrayList<>();

            while (rs.next()) {
                addresses.add(Constant.getAddressFromResultSet(rs));
            }
            return addresses.size() > 0 ? addresses : null;
        } catch (Exception ex) {
            throw new AppException(ErrorCode.E320);
        }
    }

//    public static void main(String[] args) {
//        ThreadPool threadPool = new ThreadPool();
//        threadPool.submit(() -> {
//            AddressService as = new AddressService();
//            try {
//                List<Address> addresses = as.readAll();
//                System.out.println(Thread.currentThread().getName() + " " + addresses);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//        });
//        threadPool.submit(() -> {
//            AddressService as = new AddressService();
//            try {
//                Address address = as.read(23);
//                System.out.println(Thread.currentThread().getName() + " " + address);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//        });
//        threadPool.submit(() -> {
//            AddressService as = new AddressService();
//            try {
//                Address address = as.read(34);
//                System.out.println(Thread.currentThread().getName() + " " + address);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//        });
//        threadPool.submit(() -> {
//            AddressService as = new AddressService();
//            try {
//                Address address = as.read(23);
//                System.out.println(Thread.currentThread().getName() + " " + address);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//        });
//        threadPool.submit(() -> {
//            AddressService as = new AddressService();
//            try {
//                Address address = as.read(62);
//                System.out.println(Thread.currentThread().getName() + " " + address);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//        });
//        threadPool.submit(() -> {
//            AddressService as = new AddressService();
//            try {
//                List<Address> addresses = as.readAll();
//                System.out.println(Thread.currentThread().getName() + " " + addresses);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//        });
//        threadPool.submit(() -> {
//            PersonService ps = new PersonService();
//            List<Person> persons = null;
//            try {
//                persons = ps.readAll();
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + " " + persons);
//        });
//        threadPool.submit(() -> {
//            PersonService ps = new PersonService();
//            Person person = null;
//            try {
//                person = ps.read(0, false);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + " " + person);
//        });
//        threadPool.submit(() -> {
//            PersonService ps = new PersonService();
//            Person person = null;
//            try {
//                person = ps.read(12, true);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + " " + person);
//        });
//        threadPool.submit(() -> {
//            PersonService ps = new PersonService();
//            Person person = null;
//            try {
//                person = ps.read(12, false);
//            } catch (AppException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + " " + person);
//        });
//        threadPool.shutdown();
//    }

}
