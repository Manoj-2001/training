package in.kpr.training.jdbc.service;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import in.kpr.training.jdbc.constant.Constant;

class ThreadPool extends ThreadPoolExecutor {
    private ConnectionService connectionService;

    public ThreadPool() {
        super(Constant.MAX_THREADS, Constant.MAX_THREADS, 0L,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        try {
            connectionService = new ConnectionService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        try {
            ConnectionService.getConnection().close();
            if (ConnectionService.getConnection().isClosed()) {
                connectionService.releaseConnection();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
