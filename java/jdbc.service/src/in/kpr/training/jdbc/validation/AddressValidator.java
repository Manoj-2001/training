package in.kpr.training.jdbc.validation;

import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;
import in.kpr.training.jdbc.model.Address;

public class AddressValidator {

    public static void validateAddress(Address address) {
        if (address.getStreet() == null || address.getStreet() == " "
                || address.getCity() == null || address.getCity() == " ") {
            throw new AppException(ErrorCode.E312);
        }
        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.E313);
        }
    }

}
