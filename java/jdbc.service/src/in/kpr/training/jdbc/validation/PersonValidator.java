package in.kpr.training.jdbc.validation;

import java.text.SimpleDateFormat;

import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;
import in.kpr.training.jdbc.model.Person;

public class PersonValidator {

    public static void validatePerson(Person person) {
        if (person.getFirstName() == null || person.getFirstName() == " "
                || person.getLastName() == null || person.getLastName() == " ") {
            throw new AppException(ErrorCode.E315);
        }
        if (person.getBirthDate() == null) {
            throw new AppException(ErrorCode.E318);
        }
        AddressValidator.validateAddress(person.getAddress());
    }

    public static String validateDate(String dateFormat) {
        try {
            SimpleDateFormat inputFormatter = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat outputFormatter = new SimpleDateFormat("yyyy-MM-dd");

            return outputFormatter.format(inputFormatter.parse(dateFormat));
        }
        catch (Exception ex) {
            throw new AppException(ErrorCode.E317,ex);
        }
    }

}
