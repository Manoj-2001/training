/*

----------WBS----------

Entity:
``````
-ErrorCode

Jobs to be done:
```````````````
1) Create enumurator ErrorCode
2) Create enum constructors with error code and corresponding error message
3) Map error code and message and store it in ERROR_CODE_MESSAGES
4) Create getErrorMessage method to get/return message for specified error code.

*/

package in.kpr.training.jdbc.exception;

public enum ErrorCode {

    E301("Failed to store address"),
    E302("Failed to read address"),
    E303("Failed to read all addresses"),
    E304("Failed to update address"),
    E305("Failed to delete address"),
    E306("Failed to store person details"),
    E307("Failed to read person details"),
    E308("Failed to read all persons details"),
    E309("Failed to update person details"),
    E310("Failed to delete person details"),
    E311("Failed to connect with server"),
    E312("Street or city should not be empty"),
    E313("Postal code should not be empty"),
    E314("Duplicate email address found");

    private String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return this.message;
    }

}