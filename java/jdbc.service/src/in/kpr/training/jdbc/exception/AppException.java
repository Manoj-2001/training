/*

----------WBS----------

Entity:
``````
-AppException

Jobs to be done:
```````````````
1) Create AppException class should extends RunTimeException
2) Create a constructor of AppException with error code as parameter
3) Get the error message using getErrorMessage method of ErrorCode
4) Invoke the super constructor with the error message as parameter.

*/

package in.kpr.training.jdbc.exception;

public class AppException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AppException(ErrorCode code) {
        super(code.getErrorMessage());
    }

    public AppException(ErrorCode code, Exception ex) {
        super(code + "-" +code.getErrorMessage(), ex);
    }

}
