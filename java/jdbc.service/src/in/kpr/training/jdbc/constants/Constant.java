package in.kpr.training.jdbc.constants;

public class Constant {

    public static final String ID = "id",
            STREET = "street",
            CITY = "city",
            POSTAL_CODE = "postal_code",
            NAME = "name",
            EMAIL = "email",
            ADDRESS_ID = "address_id",
            BIRTH_DATE = "birth_date",
            CREATED_DATE = "created_date",
            URL = "url",
            USER = "user",
            PASSWORD = "password";

}
