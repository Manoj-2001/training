package in.kpr.training.jdbc.constants;

public class QueryStatement {

    public static final String INSERT_ADDRESS = new StringBuilder(
            "INSERT INTO address (street, city, postal_code) "
          + "VALUES (?, ?, ?)").toString();
    public static final String READ_ADDRESS = new StringBuilder(
            "SELECT street, city, postal_code "
            + "FROM address "
           + "WHERE id = ?").toString();
    public static final String READ_ALL_ADDRESS = new StringBuilder(
            "SELECT id, street, city, postal_code "
            + "FROM address").toString();
    public static final String UPDATE_ADDRESS = new StringBuilder(
            "UPDATE address "
             + "SET street = ?, city = ?, postal_code = ? "
           + "WHERE id = ?").toString();
    public static final String DELETE_ADDRESS = new StringBuilder(
            "DELETE FROM address "
           + "WHERE id = ?").toString();
    public static final String INSERT_PERSON = new StringBuilder(
            "INSERT INTO person (name, email, address_id, birth_date) "
          + "VALUES (?, ?, ?, ?)").toString();
    public static final String READ_PERSON = new StringBuilder(
            "SELECT street, city, postal_code, name, email, address_id, birth_date, created_date "
            + "FROM person "
           + "INNER JOIN address ON person.address_id = address.id "
           + "WHERE person.id = ?").toString();
    public static final String READ_ALL_PERSON = new StringBuilder(
            "SELECT street, city, postal_code, name, email, address_id, birth_date "
            + "FROM person "
           + "INNER JOIN address ON person.address_id = address.id").toString();
    public static final String UPDATE_PERSON = new StringBuilder(
            "UPDATE person "
             + "SET name = ?, email = ?, address_id = ?, birth_date = ? "
           + "WHERE id = ?").toString();
    public static final String DELETE_PERSON = new StringBuilder(
            "DELETE FROM person "
           + "WHERE id = ?").toString();
    public static final String GET_ID_FOR_EMAIL = new StringBuilder(
            "SELECT id "
            + "FROM person "
           + "WHERE email = ? "
          + "HAVING id != ?").toString();
    public static final String GET_ID_FOR_ADDRESS = new StringBuilder(
            "SELECT id "
            + "FROM address "
           + "WHERE street = ? AND city = ? AND postal_code = ?").toString();
}
