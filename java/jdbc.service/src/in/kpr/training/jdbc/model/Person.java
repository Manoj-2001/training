/*

----------WBS----------

POJO class for Person

Entity:
``````
-Person

Method Signature:
````````````````
-public long getId()
-public String getName()
-public String getEmail()
-public long getAddressId()
-public String getBirthDate()
-public String getCreatedDate()
-public void setId(long id)
-public void setName(String name)
-public void setEmail(String email)
-public void setAddressId(long address_id)
-public void setBirthDate(String birth_date)

Jobs to be done:
```````````````
1) Create Person class and private properties as same as in data base.
2) Create a constructor of the class to store the person details to properties.
3) Create getters and setters to get and set details/properties.
(Here no setters for id and created_date property, It is auto generated in database)

Pseudo Code:
```````````
public class Person {

    // properties as same as database person table columns
    private long id;
    private String name;
    private String email;
    private long addressId;
    private String birthDate;
    private String createdDate;
    private Address address;

    // constructor
    public Person(long id, String name, String email, long addressId, String birthDate, String createdDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.addressId = addressId;
        this.birthDate = birthDate;
        this.createdDate = createdDate;
    }

    [getters and setters to access the private properties]

}

*/

package in.kpr.training.jdbc.model;

import java.util.Date;

public class Person {

    // properties
    private long id;
    private String firstName;
    private String lastName;
    private String name;
    private String email;
    private long addressId;
    private Date birthDate;
    private Date createdDate;
    private Address address;

    // constructors
    // to read person
    public Person(long id, String name, String email, long addressId, Date birthDate, Date createdDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.addressId = addressId;
        this.birthDate = birthDate;
        this.createdDate = createdDate;
    }

    // to read person with address
    public Person(long id, String name, String email, Address address, Date birthDate, Date createdDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.birthDate = birthDate;
        this.createdDate = createdDate;
    }

    // to update person
    public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.name = firstName + " " + lastName;
        this.email = email;
        this.addressId = addressId;
        this.birthDate = birthDate;
    }

    // to update person with address
    public Person(long id, String firstName, String lastName, String email, Address address, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.name = firstName + " " + lastName;
        this.email = email;
        this.address = address;
        this.birthDate = birthDate;
    }

    // to create person with address
    public Person(String firstName, String lastName, String email, Address address, Date birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.name = firstName + " " + lastName;
        this.email = email;
        this.address = address;
        this.birthDate = birthDate;
    }

    // for test case
    public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate,
            Date createdDate) {
        this.id = id;
        this.name = firstName + " " + lastName;
        this.email = email;
        this.addressId = addressId;
        this.birthDate = birthDate;
        this.createdDate = createdDate;
    }

    // getters and setters
    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public long getAddressId() {
        return addressId;
    }

    public java.sql.Date getBirthDate() {
        return (java.sql.Date) birthDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return String.format("Person = ['%d', '%s', '%s', '%s', '%tF', '%tF']",
                id, name, email, address.toString(), birthDate, createdDate);
    }

}
