/*

----------WBS----------

POJO class for Address

Entity:
``````
-Address

Method Signature:
````````````````
-public long getId()
-public String getStreet()
-public String getCity()
-public int getPostalCode()
-public void setId()
-public void setStreet(String street)
-public void setCity(String city)
-public void setPostalCode(int postal_code)

Jobs to be done:
```````````````
1) Create Address class
2) Create private properties as same as in data base
3) Create a constructor of the class to store and retrieve the address
4) Create getters and setters to get and set address properties

Pseudo Code:
```````````
public class Address {

    // private properties
    private long id;
    private String street;
    private String city;
    private int postal_code;

    // constructor
    public Address(long id, String street, String city, int postal_code) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.postal_code = postal_code;
    }

    [getters and setters to access the private properties]

}

*/

package in.kpr.training.jdbc.model;

public class Address {

    // properties
    private long id;
    private String street;
    private String city;
    private int postalCode;

    // constructors
    public Address(long id, String street, String city, int postalCode) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    public Address(String street, String city, int postalCode) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    // getters and setters
    public long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return String.format("Address = ['%d', '%s', '%s', '%d']", id, street, city, postalCode);
    }

}