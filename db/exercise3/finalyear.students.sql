/*
2. Select final year students(Assume that universities has Engineering
Depts only) details who are studying under a particular university and
selected cities alone.
ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS,
COLLEGE_NAME, DEPARTMENT_NAME
*/
USE `universitydb`;
SELECT student.`roll_number`
	  ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,student.`academic_year` AS 'batch'
      ,college.`name` AS 'college'
      ,department.`dept_name` AS 'department'
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN student 
       ON college.`id` = student.`college_id`
       INNER JOIN college_department 
       ON student.`cdept_id` = college_department.`cdept_id`
       INNER JOIN department 
       ON college_department.`udept_code` = department.`dept_code`
 WHERE university.university_name = 'Anna University'
   AND college.city = 'Coimbatore'
HAVING batch = (SELECT min(`academic_year`)
				  FROM student);