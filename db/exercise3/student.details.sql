 /*
3. Select students details who are studying under a particular university
and selected cities alone. Fetch 20 records for every run.
ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS,
COLLEGE_NAME, DEPARTMENT_NAME, HOD_NAME
*/
USE `universitydb`;
SELECT student.`roll_number`
	  ,student.`name` 'student name'
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name`
      ,department.`dept_name` AS 'department'
      ,employee.`name` AS 'HOD name'
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN student 
       ON college.`id` = student.`college_id`
       INNER JOIN employee 
       ON college.`id` = employee.`college_id`
       INNER JOIN designation 
       ON employee.`desig_id` = designation.`id`
	   INNER JOIN college_department 
       ON student.`cdept_id` = college_department.`cdept_id`
       INNER JOIN department 
       ON college_department.`udept_code` = department.`dept_code`
 WHERE university.`university_name` = 'Anna University'
   AND student.`cdept_id` = employee.`cdept_id`
   AND college.`city` = 'Coimbatore'
   AND designation.`name` = 'HOD'
 LIMIT 20
OFFSET 0;
-- OFFSET Pagination --
   
