/*
10. Display below result in one SQL run
a) Shows students details who scored above 8 GPA for the given
semester
b) Shows students details who scored above 5 GPA for the given
semester
*/
-- (a)
USE `universitydb`;
SELECT student.`id`
	  ,student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,college.`code`
      ,college.`name`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN student 
       ON college.`id` = student.`college_id`
       INNER JOIN semester_result 
       ON student.`id` = semester_result.`stud_id`
 WHERE semester_result.`semester` = 5
   AND semester_result.`gpa` > 8

UNION
-- (b)
SELECT student.`id`
	  ,student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,college.`code`
      ,college.`name`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN student 
       ON college.`id` = student.`college_id`
       INNER JOIN semester_result 
       ON student.`id` = semester_result.`stud_id`
 WHERE semester_result.`semester` = 5
   AND semester_result.`gpa` > 5
 ORDER BY `gpa`;