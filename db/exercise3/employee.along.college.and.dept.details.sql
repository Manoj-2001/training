/*
4. List Employees details from a particular university along with their
college and department details. Details should be sorted by rank and
college name
*/
USE `universitydb`;
SELECT employee.`id`
	  ,employee.`name`
      ,employee.`dob`
      ,designation.`name` AS 'designation'
      ,department.`dept_name` AS 'department'
      ,designation.`rank` 
      ,college.`name` AS 'college'
      ,university.`univ_code` AS 'university code'
      ,university.`university_name` AS 'university'
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN employee 
       ON college.`id` = employee.`college_id`
       INNER JOIN designation 
       ON employee.`desig_id` = designation.`id`
       INNER JOIN college_department 
       ON employee.`cdept_id` = college_department.`cdept_id`
       INNER JOIN department 
       ON college_department.`udept_code` = department.`dept_code`
 WHERE university.`university_name` = 'Anna University'
 ORDER BY college.`name`
         ,designation.`rank`
