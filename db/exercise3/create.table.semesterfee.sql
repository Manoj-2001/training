/*
6. Create new entries in SEMESTER_FEE table for each student from all
the colleges and across all the universities. These entries should be
created whenever new semester starts.. Each entry should have below
default values;
a) AMOUNT - Semester fees
b) PAID_YEAR - Null
c) PAID_STATUS - Unpaid
*/
CREATE TABLE semester_fee (`sno` INT PRIMARY KEY
						  ,`cdept_id` INT
						  ,`stud_id` INT
                          ,`semester` TINYINT NOT NULL
                          ,`amount` FLOAT DEFAULT '50000.00'
                          ,`paid_year` YEAR DEFAULT NULL
                          ,`paid_status` VARCHAR(10) DEFAULT 'Unpaid'
                          ,FOREIGN KEY(`cdept_id`)
                          REFERENCES college_department(`cdept_id`)
                          ,FOREIGN KEY(`stud_id`)
                          REFERENCES student(`id`)
                          );
INSERT INTO universitydb.semester_fee(`sno`, `cdept_id`, `stud_id`, `semester`) 
VALUES (138, 507, 526, 5),
       (139, 507, 527, 3),
       (140, 507, 528, 1);