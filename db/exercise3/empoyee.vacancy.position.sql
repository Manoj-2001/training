/*
8. Find employee vacancy position in all the departments from all the
colleges and across the universities. Result should be populated with
following information.. Designation, rank, college, department and
university details
*/
USE `universitydb`;
SELECT designation.`name`
      ,designation.`rank`
      ,college.`name`
      ,department.`dept_name`
      ,university.`univ_code`
      ,university.`university_name`
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN department 
       ON department.`univ_code` = university.`univ_code`
       INNER JOIN college_department 
       ON department.`dept_code` = college_department.`udept_code`
       INNER JOIN employee 
       ON employee.`cdept_id` = college_department.`cdept_id`
       INNER JOIN designation 
       ON designation.`id` = employee.`desig_id`
 WHERE employee.`college_id` = college.`id`
   AND employee.`id` IS NULL
   AND employee.`desig_id` IS NOT NULL;
  
