/* 5. List Students details along with their GRADE,CREDIT and GPA details
from all universities. Result should be sorted by college_name and
semester. Apply paging also.
*/
USE `universitydb`;
SELECT student.`id`
      ,student.`roll_number`
	  ,student.`name`
      ,student.`gender`
      ,student.`phone` AS 'student contact'
      ,college.`name` AS 'college name'
      ,semester_result.`semester`
      ,semester_result.`credits`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN student 
       ON college.`id` = student.`college_id`
       INNER JOIN semester_result 
       ON student.`id` = semester_result.`stud_id`
 ORDER BY college.`name`
         ,student.`id`
         ,semester_result.`semester`
 LIMIT 20
OFFSET 0;
-- OFFSET Pagination --