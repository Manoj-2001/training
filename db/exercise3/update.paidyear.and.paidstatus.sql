/*
7. Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who
has done the payment. Entries should be updated based on student
ROLL_NUMBER.
*/
USE `universitydb`;
-- a) Single Update - Update one student entry
UPDATE `semester_fee`
   SET paid_year = 2020
	  ,paid_status = 'Paid'
 WHERE (SELECT roll_number
		  FROM student
		 WHERE student.`id` = semester_fee.`stud_id`) = '20IT014';

-- b) Bulk Update - Update multiple students entries
UPDATE `semester_fee`
   SET paid_year = 2019
	  ,paid_status = 'Paid'
 WHERE (SELECT roll_number
		  FROM student
		 WHERE student.`id` = semester_fee.`stud_id`) IN ('19IT013','19CS017','19EE215');
 
-- c) PAID_STATUS value as ‘Paid’
-- d) PAID_YEAR value as ‘year format’
UPDATE `semester_fee`
   SET paid_status = 'Paid'
      ,paid_year = 2018
 WHERE (SELECT roll_number
          FROM student
		 WHERE student.`id` = semester_fee.`stud_id`) = '17ME525';
       