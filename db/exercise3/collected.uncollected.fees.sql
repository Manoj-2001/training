 -- 9. Show consolidated result for the following scenarios
/*
a) collected and uncollected semester fees amount per semester 
for each college under an university. Result should be filtered 
based on given year.
*/
USE `universitydb`;
SELECT university.`university_name`
      ,college.`name`
      ,SUM(collected_semester_fee.`amount`) AS 'collected fees'
      ,SUM(uncollected_semester_fee.`amount`) AS 'uncollected fees'
  FROM university
	   INNER JOIN college 
       ON (university.`univ_code` = college.`univ_code`)
       INNER JOIN college_department 
       ON (college.`id` = college_department.`college_id`)
       INNER JOIN student 
       ON (student.`college_id` = college.`id` AND student.`cdept_id` = college_department.`cdept_id`)
	   LEFT JOIN semester_fee AS `collected_semester_fee` 
       ON (student.`id` = collected_semester_fee.`stud_id` AND collected_semester_fee.`stud_id` IN (SELECT stud_id 
                                                                                                      FROM semester_fee 
                                                                                                     WHERE paid_status = 'Paid'))
       LEFT JOIN semester_fee AS `uncollected_semester_fee` 
       ON (student.`id` = uncollected_semester_fee.`stud_id` AND uncollected_semester_fee.`stud_id` IN (SELECT stud_id 
                                                                                                          FROM semester_fee 
                                                                                                         WHERE paid_status = 'Unpaid'))
-- WHERE collected_semester_fee.`paid_year` = 2019
 GROUP BY college.`name`;

/*
b) Collected semester fees amount for each university for the given 
year
*/
SELECT university.`univ_code`
      ,university.`university_name`
      ,SUM(semester_fee.`amount`) AS 'overall collected fees'
      ,COUNT(semester_fee.`amount`) AS 'students paid'
      ,semester_fee.`paid_year`
  FROM university
       INNER JOIN college 
       ON university.`univ_code` = college.`univ_code`
       INNER JOIN college_department 
       ON college.`id` = college_department.`college_id`
       INNER JOIN semester_fee 
       ON college_department.`cdept_id` = semester_fee.`cdept_id`
 WHERE semester_fee.`paid_year` = 2020
 GROUP BY university.`university_name`;