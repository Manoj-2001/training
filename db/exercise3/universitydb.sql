CREATE SCHEMA `universitydb`;
USE `universitydb`;

CREATE TABLE university (`univ_code` CHAR(4) PRIMARY KEY
                        ,`university_name` VARCHAR(100) NOT NULL);
CREATE INDEX `idx_univ_code`
    ON university(univ_code);

CREATE TABLE college (`id` INT PRIMARY KEY
                     ,`code` CHAR(4) NOT NULL
                     ,`name` VARCHAR(100) NOT NULL
                     ,`univ_code` CHAR(4) 
                     ,`city` VARCHAR(50) NOT NULL
                     ,`state` VARCHAR(50) NOT NULL
                     ,`year_opened` YEAR(4) NOT NULL
                     ,FOREIGN KEY(`univ_code`)
                     REFERENCES university(`univ_code`));
CREATE INDEX `idx_college_id`
    ON college(id);

CREATE TABLE department (`dept_code` CHAR(4) PRIMARY KEY
                        ,`dept_name` VARCHAR(50) NOT NULL
                        ,`univ_code` CHAR(4)
                        ,FOREIGN KEY(`univ_code`)
                        REFERENCES university(`univ_code`));

CREATE TABLE designation (`id` INT PRIMARY KEY
                         ,`name` VARCHAR(30) NOT NULL
                         ,`rank` CHAR(1) NOT NULL);
CREATE INDEX `idx_designation_id`
    ON designation(id);

CREATE TABLE employee (`id` INT PRIMARY KEY
                      ,`name` VARCHAR(100) NOT NULL
                      ,`dob` DATE NOT NULL
                      ,`email` VARCHAR(50) NOT NULL
                      ,`phone` BIGINT NOT NULL
                      ,`college_id` INT
                      ,`cdept_id` INT
					  ,`desig_id` INT 
                      ,FOREIGN KEY(`college_id`)
                      REFERENCES college(`id`)
                      ,FOREIGN KEY(`cdept_id`)
                      REFERENCES college_department(`cdept_id`)
					  ,FOREIGN KEY(`desig_id`)
                      REFERENCES designation(`id`));
CREATE INDEX `idx_employee_id`
    ON employee(id);

CREATE TABLE college_department (`cdept_id` INT PRIMARY KEY
                                ,`udept_code` CHAR(4)
                                ,`college_id` INT
                                ,FOREIGN KEY(`udept_code`)
                                REFERENCES department(`dept_code`)
                                ,FOREIGN KEY(`college_id`)
                                REFERENCES college(`id`));
CREATE INDEX `idx_clg_dept_id`
    ON college_department(cdept_id);

CREATE TABLE professor_syllabus (`emp_id` INT
                                ,`syllabus_id` INT
                                ,`semester` TINYINT NOT NULL
                                ,FOREIGN KEY(`emp_id`)
                                REFERENCES employee(`id`));

CREATE TABLE syllabus (`id` INT PRIMARY KEY
                      ,`cdept_id` INT
                      ,`syllabus_code` CHAR(4) NOT NULL
                      ,`syllabus_name` VARCHAR(100) NOT NULL
                      ,FOREIGN KEY(`cdept_id`)
                      REFERENCES college_department(`cdept_id`));
CREATE INDEX `idx_syllabus_id`
    ON syllabus(id);

CREATE TABLE student (`id` INT PRIMARY KEY
                     ,`roll_number` CHAR(8) NOT NULL
                     ,`name` VARCHAR(100) NOT NULL
                     ,`dob` DATE NOT NULL
					 ,`gender` CHAR(1) NOT NULL
                     ,`email` VARCHAR(50) NOT NULL
                     ,`phone` BIGINT NOT NULL
                     ,`address` VARCHAR(200) NOT NULL
					 ,`academic_year` YEAR(4) NOT NULL
                     ,`cdept_id` INT
					 ,`college_id` INT 
                     ,FOREIGN KEY(`cdept_id`)
                     REFERENCES college_department(`cdept_id`)
					 ,FOREIGN KEY(`college_id`)
                     REFERENCES college(`id`));
CREATE INDEX `idx_stud_id`
    ON student(id);

CREATE TABLE semester_fee (`cdept_id` INT
                          ,`stud_id` INT
                          ,`semester` TINYINT NOT NULL
						  ,`amount` DOUBLE(18,2) NULL
                          ,`paid_year` YEAR(4) NULL
                          ,`paid_status` VARCHAR(10) NOT NULL
                          ,FOREIGN KEY(`cdept_id`)
                          REFERENCES college_department(`cdept_id`)
                          ,FOREIGN KEY(`stud_id`)
                          REFERENCES student(`id`));

CREATE TABLE semester_result (`stud_id` INT
							 ,`syllabus_id` INT
                             ,`semester` TINYINT NOT NULL
                             ,`grade` VARCHAR(2) NOT NULL
                             ,`credits` FLOAT NOT NULL
                             ,`result_date` DATE NOT NULL
                             ,FOREIGN KEY(`stud_id`)
                             REFERENCES student(`id`)
                             ,FOREIGN KEY(`syllabus_id`)
							 REFERENCES syllabus(`id`));