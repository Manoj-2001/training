-- 9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

SELECT * 
  FROM `schoolmanagement`.`student`
 WHERE `student_name`='Andrew' AND `standard`=12;
 
SELECT * 
  FROM `schoolmanagement`.`student`
 WHERE `student_name`='Andrew' OR `standard`=12;
 
SELECT * 
  FROM `schoolmanagement`.`student`
 WHERE NOT(`standard`=11 OR `standard`=12);
 
SELECT * 
  FROM `schoolmanagement`.`student`
 WHERE student_name LIKE '%n';

SELECT student_name
  FROM `schoolmanagement`.`student`
 WHERE `standard` IN (10,11);
 
SELECT student_name 
  FROM `schoolmanagement`.`student`
 WHERE student_id = ANY(SELECT student_id FROM `schoolmanagement`.`teacher` WHERE `subjects_handled`>2);
 