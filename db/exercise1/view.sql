-- 14.Create VIEW for your Join Query and select values from VIEW

CREATE VIEW `[Students With Good Marks]` AS
SELECT student_id,student_name,avg_mark
  FROM student_mark
 WHERE avg_mark > 75; 
 
SELECT * FROM `[Students With Good Marks]`;
SELECT student_id FROM `[Students With Good Marks]`;

CREATE VIEW `[Student's marks with teacher details]` AS
    SELECT teacher.teacher_id
		  ,teacher.teacher_name
          ,teacher.address
          ,teacher.subjects_handled
          ,teacher.subject_code
          ,student.student_id
          ,student.student_name
          ,student_mark.avg_mark
      FROM teacher 
      JOIN student ON teacher.student_id = student.student_id
      JOIN student_mark ON teacher.student_id = student_mark.student_id;
	
SELECT * 
  FROM `[Student's marks with teacher details]`;
SELECT teacher_name
      ,subject_code 
  FROM `[Student's marks with teacher details]`;