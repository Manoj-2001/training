-- 11.Read Records by Sub-queries by using two tables FOREIGN KEY relationships

SELECT *
  FROM `schoolmanagement`.`student`
 WHERE student_id = (
     SELECT student_id 
       FROM `schoolmanagement`.`teacher` 
	  WHERE subjects_handled>2
 );