-- 1.Create Database\table and Rename\Drop Table

CREATE SCHEMA `schoolmanagement`;

CREATE TABLE `schoolmanagement`.staff(
    `Teacher ID` INT NOT NULL UNIQUE
   ,`Name` VARCHAR(15) NOT NULL
);

ALTER TABLE `schoolmanagement`.`staff`
RENAME TO `schoolmanagement`.`teacher`;

SELECT * 
  FROM `schoolmanagement`.`teacher`;

CREATE TABLE `schoolmanagement`.employee(
    `Employee Name` VARCHAR(25)
    ,`Designation` VARCHAR(20)
);
DROP TABLE `schoolmanagement`.`employee`;
SHOW TABLES IN `schoolmanagement`;
