-- 7.Delete Records from the table with/without WHERE

DELETE FROM `schoolmanagement`.`teacher`
      WHERE subjects_handled = 2;

SELECT * 
  FROM `schoolmanagement`.`teacher`;

DELETE FROM `schoolmanagement`.`subject`;

SELECT * 
  FROM `schoolmanagement`.`subject`;
