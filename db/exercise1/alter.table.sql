-- 2.Alter Table with Add new Column and Modify\Rename\Drop column

CREATE TABLE `schoolmanagement`.student(
    student_id INT NOT NULL
   ,student_name VARCHAR(20) NOT NULL
   ,standard INT NOT NULL
   ,dob DATE NOT NULL
   ,address VARCHAR(100) NOT NULL
   ,contact_no INT NOT NULL
   ,PRIMARY KEY(`student_id`)
);

ALTER TABLE `schoolmanagement`.`student`
ADD COLUMN `student_age` VARCHAR(2);

ALTER TABLE `schoolmanagement`.`student`
MODIFY COLUMN `student_age` INT;

ALTER TABLE `schoolmanagement`.`student`
MODIFY COLUMN `contact_no` BIGINT;

ALTER TABLE `schoolmanagement`.`student`
RENAME COLUMN `student_age` TO `age`;

ALTER TABLE `schoolmanagement`.`student`
DROP COLUMN `age`;
