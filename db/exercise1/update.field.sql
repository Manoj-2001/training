-- 6.Update Field value on the Fields with/without WHERE

UPDATE `schoolmanagement`.`teacher`
   SET address = 'Shop No 19, Sector 6, Vashi, Navi Mumbai, Mumbai'
 WHERE teacher_id = 23 AND teacher_name = 'Suresh';
 
 UPDATE `schoolmanagement`.`subject`
	SET subject_name = 'Communication English';