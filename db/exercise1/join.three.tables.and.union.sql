-- 13.At-least use 3 tables for Join and Union 2 Different table with same Column name (use Distinct,ALL)

USE `schoolmanagement`;

UPDATE `student_mark`    
SET avg_mark = (tamil + english + maths + science + social)/5   
WHERE student_id IN (1,2,3,4);

SELECT teacher.teacher_id,teacher.teacher_name,teacher.subject_code,student.student_id,student.student_name,student_mark.avg_mark
  FROM teacher 
  JOIN student ON teacher.student_id = student.student_id
  JOIN student_mark ON teacher.student_id = student_mark.student_id;



SELECT DISTINCT `student_name` FROM student
UNION
SELECT DISTINCT `student_name` FROM student_mark;


SELECT ALL `student_name` FROM student
UNION
SELECT ALL `student_name` FROM student_mark;

