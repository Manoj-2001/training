-- 12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

USE `schoolmanagement`;
SELECT teacher.teacher_name,student.student_name,teacher.subject_code
  FROM teacher
 CROSS JOIN student;

USE `schoolmanagement`;
SELECT teacher.teacher_name,student.student_name,teacher.subject_code
  FROM teacher
 INNER JOIN student
 WHERE teacher.student_id = student.student_id;
 
USE `schoolmanagement`;
SELECT teacher.teacher_name,student.student_name,teacher.subject_code
  FROM teacher
  LEFT JOIN student
    ON teacher.student_id = student.student_id;
    
USE `schoolmanagement`;
SELECT teacher.teacher_name,student.student_name,teacher.subject_code
  FROM teacher
RIGHT JOIN student
    ON teacher.student_id = student.student_id;