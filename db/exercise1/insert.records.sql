-- 5.Insert Records into Table

USE `schoolmanagement`;

INSERT INTO student(
    student_id
   ,student_name
   ,standard
   ,dob
   ,address
   ,contact_no
)
VALUES
    (
        1
	   ,'Yuvraj'
       ,11
       ,'2017-09-20'
       ,'Heritage Plaza, J P Rd., Opp. Indian Oil Nagar, Andheri, Mumbai'
       ,2226355656
    ),
    (
        2
	   ,'Andrew'
       ,12
       ,'2016-08-12'
       ,'90ft Road, Dharavi, Mumbai'
       ,02224072399
    ),
    (
        3
	   ,'Kevin Tim'
       ,12
       ,'2016-01-21'
       ,'82/f, 82/f Dr Sudhir Basu Road, Mumbai'
       ,24497787
    );

INSERT INTO teacher(
    teacher_id
   ,teacher_name
   ,age
   ,address
   ,subject_code
   ,subjects_handled
   ,student_id
) 
VALUES 
    (
        023
	   ,'Suresh'
       ,26
       ,'24b, Rajabahadur Compound, Hamam Street, Fort, Mumbai'
       ,'PY023'
       ,2
       ,3
	),
    (
	    018
	   ,'Ted'
       ,34
       ,'P 12, Green Park Extn, Delhi','MA021'
       ,4
       ,3
    ),
    (
        07
	   ,'Ken'
       ,28
       ,'M 24, Part 2 Commercial Complex, Delhi'
       ,'CH011'
       ,1
       ,2
    ),
    (
        034
	   ,'Mark Scott'
       ,26
       ,'Shop-26, Sec-18, Annapoorna, Near Mafco Market, Mumbai'
       ,'CS067'
       ,2
       ,1
    ),
    (
        019
	   ,'Henry'
       ,45
       ,'20, Heera Panna S C, Vishveshwar Nagar Rd, Nr Virvani, Goregaon, Mumbai'
       ,'EN016'
       ,2
       ,1
    );
    
INSERT INTO `subject` 
VALUES 
    (
	    'EN016'
	   ,'English'
	),
    (
        'MA021'
	   ,'Maths'
    ),
    (
        'PY023'
	   ,'Physics'
    ),
    (
        'CH011'
	   ,'Chemistry'
    ),
    (
	    'CS067'
	   ,'Computer Science'
    );