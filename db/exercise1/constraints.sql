-- 3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX

CREATE TABLE `schoolmanagement`.teacher(
    teacher_id INT NOT NULL
   ,teacher_name VARCHAR(25) NOT NULL
   ,age INT NOT NULL
   ,address VARCHAR(100) NOT NULL
   ,subject_code VARCHAR(5)
   ,subjects_handled INT
   ,student_id INT NOT NULL
   ,profession VARCHAR(25) DEFAULT 'Teaching'
   ,CHECK(subjects_handled >= 1)
   ,PRIMARY KEY (`teacher_id`)
   ,FOREIGN KEY (`student_id`)
    REFERENCES student(`student_id`)
);

CREATE INDEX name_index
ON `schoolmanagement`.teacher(`teacher_name`)
