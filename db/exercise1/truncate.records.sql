-- 4.Truncate Records From table

USE `schoolmanagement`;
CREATE TABLE `subject`(
    subject_code VARCHAR(5) NOT NULL
   ,subject_name VARCHAR(25) NOT NULL
   ,PRIMARY KEY ( `subject_code`)
);
TRUNCATE TABLE `schoolmanagement`.`subject`;