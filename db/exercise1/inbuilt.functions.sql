-- 10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM

CREATE TABLE `schoolmanagement`.`student_mark`(
    student_id INT NOT NULL
   ,student_name VARCHAR(20)
   ,tamil INT DEFAULT 0
   ,english INT DEFAULT 0
   ,maths INT DEFAULT 0
   ,science INT DEFAULT 0
   ,social INT DEFAULT 0
   ,FOREIGN KEY (`student_id`) 
   REFERENCES student(`student_id`)
);
INSERT INTO `schoolmanagement`.student_mark
VALUES
    (
        1
	   ,'Yuvraj'
       ,78
       ,81
       ,91
       ,90
       ,66
    ),
    (
        2
	   ,'Andrew'
       ,59
       ,90
       ,84
       ,62
       ,65
    ),
    (
        3
	   ,'Kevin Tim'
       ,70
       ,77
       ,72
       ,69
       ,90
    ),
    (
        4
	   ,'Andrew'
       ,78
       ,81
       ,91
       ,90
       ,66
    ),
    (
        5
	   ,'Puvan'
       ,56
       ,78
       ,90
       ,95
       ,89
    ),
    (
        5
	   ,'Kevin'
       ,89
       ,89
       ,97
       ,99
       ,70
    );
SELECT MIN(tamil)
  FROM `schoolmanagement`.`student_mark`;
SELECT MAX(science)
  FROM `schoolmanagement`.`student_mark`;
SELECT AVG(maths)
  FROM `schoolmanagement`.`student_mark`;
SELECT COUNT(student_id)
  FROM `schoolmanagement`.`student_mark`;
SELECT SUM(tamil),SUM(social)
  FROM `schoolmanagement`.`student_mark`;
SELECT FIRST(student_name)
  FROM `schoolmanagement`.`student_mark`;
SELECT LAST(student_name)
  FROM `schoolmanagement`.`student_mark`;
  