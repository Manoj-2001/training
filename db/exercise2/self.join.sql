-- 7. Prepare an example for self-join
 
USE `office`;
ALTER TABLE `employee`
ADD COLUMN `manager_id` INT AFTER `annual_salary`;
 
UPDATE `office`.`employee` SET `manager_id` = '4' WHERE (`emp_id` = '1');
UPDATE `office`.`employee` SET `manager_id` = '10' WHERE (`emp_id` = '2');
UPDATE `office`.`employee` SET `manager_id` = '2' WHERE (`emp_id` = '3');
UPDATE `office`.`employee` SET `manager_id` = '17' WHERE (`emp_id` = '4');
UPDATE `office`.`employee` SET `manager_id` = '21' WHERE (`emp_id` = '5');
UPDATE `office`.`employee` SET `manager_id` = '30' WHERE (`emp_id` = '6');
UPDATE `office`.`employee` SET `manager_id` = '1' WHERE (`emp_id` = '8');
UPDATE `office`.`employee` SET `manager_id` = '3' WHERE (`emp_id` = '11');
UPDATE `office`.`employee` SET `manager_id` = '5' WHERE (`emp_id` = '10');
UPDATE `office`.`employee` SET `manager_id` = '18' WHERE (`emp_id` = '12');
UPDATE `office`.`employee` SET `manager_id` = '19' WHERE (`emp_id` = '13');
UPDATE `office`.`employee` SET `manager_id` = '16' WHERE (`emp_id` = '15');
UPDATE `office`.`employee` SET `manager_id` = '11' WHERE (`emp_id` = '16');
UPDATE `office`.`employee` SET `manager_id` = '8' WHERE (`emp_id` = '17');
UPDATE `office`.`employee` SET `manager_id` = '20' WHERE (`emp_id` = '18');
UPDATE `office`.`employee` SET `manager_id` = '9' WHERE (`emp_id` = '19');
UPDATE `office`.`employee` SET `manager_id` = '12' WHERE (`emp_id` = '20');
UPDATE `office`.`employee` SET `manager_id` = '24' WHERE (`emp_id` = '21');
UPDATE `office`.`employee` SET `manager_id` = '29' WHERE (`emp_id` = '22');
UPDATE `office`.`employee` SET `manager_id` = '26' WHERE (`emp_id` = '23');
UPDATE `office`.`employee` SET `manager_id` = '13' WHERE (`emp_id` = '30');
UPDATE `office`.`employee` SET `manager_id` = '25' WHERE (`emp_id` = '28');
UPDATE `office`.`employee` SET `manager_id` = '6' WHERE (`emp_id` = '27');
UPDATE `office`.`employee` SET `manager_id` = '7' WHERE (`emp_id` = '24');
UPDATE `office`.`employee` SET `manager_id` = '15' WHERE (`emp_id` = '25');
UPDATE `office`.`employee` SET `manager_id` = '22' WHERE (`emp_id` = '26');

SELECT emp.emp_id AS 'Employee ID'
      ,emp.first_name AS 'Employee name'
      ,man.emp_id AS 'Manager ID'
      ,man.first_name AS 'Manager name'
FROM `employee` AS emp, `employee` AS man
WHERE emp.`manager_id` = man.`emp_id`;

/*                        SELF JOIN
Self join is differ from 'inner' and 'outer' joins,there we
connect two different tables via foreign key but in self join 
we join the id to the name of that same table.
from the above example,
'emp' and 'man' are the virtual tables of a table 'employee'.
The 'manager_id' is inserted in the table 'employee'. The
manager_id is FK in employee emp then the id's of two table is
joined virtually.
*/

