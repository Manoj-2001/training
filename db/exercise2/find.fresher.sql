/* 10. Prepare a query to find out fresher(no department allocated employee) in the employee,
   where no matching records in the department table. */
   
ALTER TABLE `office`.`employee` 
MODIFY COLUMN `annual_salary` INT NULL,
MODIFY COLUMN `department_id` INT NULL ;

INSERT INTO `office`.employee(`emp_id`, `first_name`, `surname`, `city`, `dob`, `date_of_joining`)
    VALUES
        (
            '31'
		   ,'Allen'
           ,'Karen'
           ,'Delhi'
           ,'1990-08-05'
           ,'2008-09-15'
		),
        (
            '32'
		   ,'Perez'
           ,'Lois'
           ,'Coimbatore'
           ,'1989-12-02'
           ,'2009-10-12'
		),
        (
            '33'
		   ,'Smith'
           ,'Diana'
           ,'Mumbai'
           ,'1979-10-21'
           ,'2004-08-08'
		),
        (
            '34'
		   ,'Parker'
           ,'Alice'
           ,'Hydrabad'
           ,'1992-05-12'
           ,'2007-05-09'
		),
        (
            '35'
		   ,'Stewart'
           ,'Carol'
           ,'Mumbai'
           ,'1980-04-05'
           ,'2008-04-17'
		),
        (
            '36'
		   ,'White'
           ,'Frances'
           ,'Chennai'
           ,'1996-02-02'
           ,'2007-09-13'
		),
        (
            '37'
		   ,'Hill'
           ,'Anna'
           ,'Mumbai'
           ,'1995-01-06'
           ,'2009-09-28'
		),
        (
            '38'
		   ,'Nelson'
           ,'Pamela'
           ,'Bangaluru'
           ,'1994-03-13'
           ,'2010-01-31'
		),
        (
            '39'
		   ,'Ward'
           ,'Julia'
           ,'Bangaluru'
           ,'1997-07-12'
           ,'2005-02-11'
		),
        (    '40'
            ,'Murphy'
            ,'Jacqueline'
            ,'Hydrabad'
            ,'1998-09-20'
            ,'2003-06-06'
		);

SELECT emp_id, first_name, city, date_of_joining, department_id
  FROM `employee`
 WHERE department_id IS NULL;

