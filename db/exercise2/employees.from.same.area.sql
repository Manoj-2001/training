-- 8. Write a query to list out employees from the same area, and from the same department

USE `office`;
ALTER TABLE `employee`
ADD COLUMN `city` VARCHAR(25) NOT NULL AFTER surname;

UPDATE `employee`
   SET city = 'Mumbai'
 WHERE `emp_id` IN (1,5,6,15,21,23);
UPDATE `employee`
   SET city = 'Chennai'
 WHERE `emp_id` IN (2,4,14,22,29,8,11);
UPDATE `employee`
   SET city = 'Bengaluru'
 WHERE `emp_id` IN (3,7,12,13,17,20,24);
UPDATE `employee`
   SET city = 'Delhi'
 WHERE `emp_id` IN (9,10,16,18,28,30);
UPDATE `employee`
   SET city = 'Coimbatore'
 WHERE `emp_id` IN (19,25,26,27);
 
 SELECT city, first_name
   FROM `employee`
  ORDER BY city;

SELECT `department`.department_name, first_name
  FROM `employee`
  JOIN `department` ON (employee.department_id = department.department_id);

SELECT city, department.department_id,department.department_name, first_name
  FROM `employee`
  JOIN `department` ON (employee.department_id = department.department_id)
 ORDER BY department_name, city;
 
SELECT first_name, city, department_name
FROM employee 
	,department 
WHERE employee.department_id = department.department_id AND department_name='ITDesk';

SELECT first_name, city, department_name
FROM employee 
	,department 
WHERE employee.department_id = department.department_id AND department_name='Finance';

SELECT first_name, city, department_name
FROM employee 
	,department 
WHERE employee.department_id = department.department_id AND department_name='Engineering';

SELECT first_name, city, department_name
FROM employee 
	,department 
WHERE employee.department_id = department.department_id AND department_name='HR';

SELECT first_name, city, department_name
FROM employee 
	,department 
WHERE employee.department_id = department.department_id AND department_name='Recruitment';

