-- 11. Write a query to get employee names and their respective department name

USE `office`;
SELECT `employee`.first_name, `employee`.surname, `department`.department_name
  FROM employee
  JOIN department ON (employee.department_id = department.department_id);
