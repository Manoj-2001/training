-- 4. Write a query to increment 10% of every employee salary

SELECT first_name,annual_salary 
  FROM `office`.`employee`;

UPDATE `office`.`employee`
   SET annual_salary = annual_salary + (annual_salary*0.1)
 WHERE emp_id IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30);

SELECT first_name,annual_salary
  FROM `office`.`employee`;
