-- 6. Find out the average salary

USE `office`;
SELECT department_id, AVG(annual_salary) AS 'Average Salary'
  FROM `employee`
GROUP BY(department_id);