/* 1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility
    employee number must be the primary key in employee table
    department number must be the primary key in department table
    department number is the foreign key in the employee table */

CREATE SCHEMA `office`;
USE `office`;
CREATE TABLE employee
    (
        `emp_id` INT NOT NULL
	   ,`first_name` VARCHAR(20) NOT NULL
       ,`surname` VARCHAR(20)
       ,`dob` DATE NOT NULL
       ,`date_of_joining` DATE NOT NULL
       ,`annual_salary` INT NOT NULL
       ,`department_id` INT NOT NULL
       ,PRIMARY KEY(`emp_id`)
       ,FOREIGN KEY(`department_id`)
       REFERENCES department(`department_id`)
	);
    
CREATE TABLE department
    (
        department_id INT NOT NULL
	   ,department_name VARCHAR(20) NOT NULL
       ,PRIMARY KEY(`department_id`)
	);
INSERT INTO `department`
VALUES
    (
        1
	   ,'ITDesk'
    ),
    (
        2
	   ,'Finance'
    ),
    (
        3
	   ,'Engineering'
    ),
    (
        4
	   ,'HR'
    ),
    (
        5
	   ,'Recruitment'
    ),
    (
        6
	   ,'Facility'
    );
    
SELECT * 
  FROM `department`;
  
ALTER TABLE `employee`
DROP PRIMARY KEY;