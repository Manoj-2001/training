-- 5. Find out the highest and least paid in all the department

USE `office`;
SELECT department_id, MAX(annual_salary) AS 'Maximum Salary', MIN(annual_salary) AS 'Minimum Salary'
FROM `employee`
GROUP BY(department_id);
 